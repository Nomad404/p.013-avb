using System.Linq;
using GestureRecognizer;
using UnityEngine;
using UILineRenderer = UnityEngine.UI.Extensions.UILineRenderer;

namespace AVB
{
    [RequireComponent(typeof(UILineRenderer))]
    public class GestureDrawer : MonoBehaviour
    {
        public GesturePattern pattern;

        private void OnValidate()
        {
            var lineRenderer = GetComponent<UILineRenderer>();
            lineRenderer.RelativeSize = true;
            var points = pattern.gesture.lines
                .Select(line => line.points)
                .SelectMany(points => points)
                // .Select(point => point)
                .Distinct().ToList();

            if (pattern.gesture.lines.Last().closedLine)
            {
                points.Add(points[0]);
            }

            lineRenderer.Points = points.ToArray();
        }
    }
}