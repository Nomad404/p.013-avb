using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AVB
{
    [CreateAssetMenu(menuName = "AVB/GameConfig")]
    public class GameConfig : ScriptableObject
    {
        public PresetLevelEntry[] presetLevels;
        public bool skipPresetLevels;
        public bool continueAfterList;
        public bool allowToSkipPresetLevels;

        public int totalLevels = 50;
        public List<EndGameConfigEntry> endgameConfigs = new();

        private void OnValidate()
        {
            presetLevels.Select((entry, index) => (entry, index)).ToList()
                .ForEach(tuple => tuple.entry.Validate(tuple.index));

            if (totalLevels < presetLevels.Length) totalLevels = presetLevels.Length + 1;
            ValidateEndgameConfigs();
            endgameConfigs.ForEach(config => config.Validate());
        }

        private void ValidateEndgameConfigs()
        {
            if (endgameConfigs.Count > 0)
            {
                var config = endgameConfigs[0];
                var start = presetLevels.Length + 1;
                var end = Mathf.Clamp(config.levelRange.y, start < totalLevels ? start + 1 : start, totalLevels);
                config.levelRange = new Vector2Int(start, end);
            }

            if (endgameConfigs.Count > 1)
            {
                for (var i = 1; i < endgameConfigs.Count; i++)
                {
                    var previousConfig = endgameConfigs[i - 1];
                    var config = endgameConfigs[i];
                    var start = previousConfig.levelRange.y + 1;
                    var end = start + 1;
                    end = Mathf.Clamp(config.levelRange.y, end, totalLevels);
                    config.levelRange = new Vector2Int(start, end);
                }
            }
        }
    }

    [Serializable]
    public class PresetLevelEntry : IValidatable
    {
        [HideInInspector] public string name;
        [Scene] public string sceneName;
        public bool enabled = true;
        public LevelConfig overrideConfig;

        public void Validate(int index = -1)
        {
            if (index >= 0) name = $"{index}: ";
            name += $"{sceneName}";
            if (overrideConfig == null || overrideConfig is not DefaultLevelConfig config) return;
            name += $", {config.bridgeSegments} rows, mode: {config.spawnMode}";
            name += config.spawnMode switch
            {
                EnemySpawnMode.Random => $", size: {config.spawnSizeRange}",
                EnemySpawnMode.SpawnList => $", size: {config.spawnList.Count}",
                _ => throw new ArgumentOutOfRangeException()
            };
        }
    }

    [Serializable]
    public abstract class LevelConfig : ScriptableObject
    {
        [Scene] public string sceneName;
        [Space] [Range(1, 5)] public int bridgeSegments = 1;
        [Space] public List<Rune> allowedRunes;
    }

    [Serializable]
    public class EnemySpawnData : IValidatable
    {
        [HideInInspector] public string name;
        public LowAttackRune low = LowAttackRune.Default;
        public MidAttackRune mid = MidAttackRune.Default;
        public float speed = Enemy.DefaultEnemySpeed;

        public void Validate(int index = -1)
        {
            name = $"{low}.{mid} {speed}f";
        }
    }

    [Serializable]
    public class EndGameConfigEntry : IValidatable
    {
        [HideInInspector] public string name;
        public Vector2Int levelRange;
        public DefaultLevelConfig levelConfig;

        public void Validate(int index = -1)
        {
            name = $"{levelRange.x} - {levelRange.y}";
        }
    }

    public interface IValidatable
    {
        void Validate(int index = -1);
    }
}