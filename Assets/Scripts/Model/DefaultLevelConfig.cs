using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;

namespace AVB
{
    [Serializable]
    [CreateAssetMenu(menuName = "AVB/DefaultLevelConfig")]
    public class DefaultLevelConfig : LevelConfig
    {
        [Header("Intro texts")] public LocalizedString headerText;
        public LocalizedString subtitleText;

        [Space] [Header("Spawn settings")] public EnemySpawnMode spawnMode;

        [Header("Random spawn settings")] public Vector2Int spawnSizeRange;

        public Vector2 randomSpeedRange = new(Enemy.DefaultEnemySpeed, Enemy.DefaultEnemySpeed);

        [Header("Spawn list")] public EnemySpawnOrder spawnOrder;
        public List<EnemySpawnData> spawnList;

        [Header("Achievements")] public bool showAchievement;
        public AchievementNames achievementName;

        public void OnValidate()
        {
            spawnList.ForEach(entry => entry.Validate());
        }
    }
}