﻿using Spine;
using Spine.Unity;
using UnityEngine;
using Event = Spine.Event;

namespace AVB
{
    public class PlayerSpineController : MonoBehaviour
    {
        public SkeletonAnimation animObjBody;
        public SkeletonAnimation animObjDash;
        public BoneFollower socketBowString;
        public BoneFollower socketHandL;
        public BoneFollower socketHandR;

        [Space] [Header("Dash time scale")] public AnimationCurve dashTimeScaleCurve;

        [Header("FX Prefabs")] public SpriteRenderer dashStartDust;
        public SpriteRenderer dashEndDust;

        private readonly Vector3 _arrowAttachRotationString = new(0, 0, -90f);
        private readonly Vector3 _arrowAttachRotationHandR = new(0, 0, 0);

        private readonly Vector3 _dashScaleToLeft = new(0.45f, 0.45f, 1);
        private readonly Vector3 _dashScaleToRight = new(-0.45f, 0.45f, 1);

        private Player _selfController;

        private MeshRenderer _meshBody;
        private MeshRenderer _meshDash;

        private AudioSource _audioSource;

        public bool CanAttack =>
            animObjBody.AnimationName != AnimStages.DefaultShoot
            && !_meshDash.enabled;

        public bool ArrowVisible
        {
            get
            {
                var arrow = socketHandL.GetComponentInChildren<Arrow>()
                            ?? socketHandR.GetComponentInChildren<Arrow>()
                            ?? socketBowString.GetComponentInChildren<Arrow>();
                return arrow && arrow.GetComponentInChildren<SpriteRenderer>().enabled;
            }
            set
            {
                var arrow = socketHandL.GetComponentInChildren<Arrow>()
                            ?? socketHandR.GetComponentInChildren<Arrow>()
                            ?? socketBowString.GetComponentInChildren<Arrow>();
                if (arrow)
                {
                    arrow.GetComponentInChildren<SpriteRenderer>().enabled = value;
                }
            }
        }

        private void Awake()
        {
            _selfController = GetComponent<Player>();
            _meshBody = animObjBody.GetComponent<MeshRenderer>();
            _meshDash = animObjDash.GetComponent<MeshRenderer>();
            _audioSource = GetComponent<AudioSource>();
        }

        private void Start()
        {
            _meshBody.sortingOrder = SpriteLayers.Player;
            _meshDash.sortingOrder = SpriteLayers.Player;

            _meshDash.enabled = false;

            animObjBody.AnimationState.Event += Del_OnAnimEvent;
            animObjBody.AnimationState.Start += Del_OnAnimStart;
            animObjDash.AnimationState.Start += Del_OnAnimStart;
            animObjBody.AnimationState.Complete += Del_OnAnimComplete;
            animObjDash.AnimationState.Complete += Del_OnAnimComplete;
        }

        private void Del_OnAnimEvent(TrackEntry trackEntry, Event e)
        {
            var arrow = socketBowString.GetComponentInChildren<Arrow>();
            switch (e.Data.Name)
            {
                case AnimationTags.ArrowLetGO:
                    if (arrow != null) _selfController.ShootArrow(arrow);
                    break;
                case AnimationTags.ArrowTouchString:
                    arrow = _selfController.GetNewArrowInstance();
                    if (arrow != null)
                    {
                        var transform1 = arrow.transform;
                        transform1.parent = socketBowString.transform;
                        transform1.localPosition = Vector3.zero;
                        arrow.transform.localRotation = Quaternion.Euler(_arrowAttachRotationString);
                    }

                    break;
                case AnimationTags.BowStretch: break;
                case AnimationTags.ReloadArrow: break;
            }
        }

        private static void Del_OnAnimStart(TrackEntry trackEntry)
        {
        }

        private void Del_OnAnimComplete(TrackEntry trackEntry)
        {
            switch (trackEntry.Animation.Name)
            {
                case AnimStages.DefaultShoot:
                    SetAnimationForObject(animObjBody, 0, AnimStages.DefaultReload, false);
                    _audioSource.PlayOneShot(AudioResourceController.instance.RandomPlayerShootSound);
                    break;
                case AnimStages.DefaultReload:
                    SetAnimationForObject(animObjBody, 0, AnimStages.DefaultIdle, true);
                    // audioSource.PlayOneShot(AudioResourceController.INSTANCE.RandomPlayerReloadSound);
                    break;
                case AnimStages.DefaultDeath:
                    _selfController.OnDeathEnd();
                    break;
                case AnimStages.DashEnter:
                    if (animObjDash.AnimationName != AnimStages.DashExit)
                    {
                        SetAnimationForObject(animObjDash, 0, AnimStages.DashLoop, true);
                    }

                    break;
                case AnimStages.DashExit:
                    if (animObjDash.AnimationName != AnimStages.DashEnter)
                    {
                        _meshDash.enabled = false;
                        _meshBody.enabled = true;
                        ArrowVisible = true;
                    }

                    break;
                case AnimStages.DashLoop: break;
            }
        }

        private static void SetAnimationForObject(SkeletonAnimation animObj, int trackNumber, string animName,
            bool loop)
        {
            if (animName != animObj.AnimationState.ToString() ||
                animName != animObj.AnimationState.GetCurrent(trackNumber).Animation.Name)
            {
                animObj.AnimationState.SetAnimation(trackNumber, animName, loop);
            }
        }

        public void StartShot()
        {
            if (animObjBody.AnimationName == AnimStages.DefaultIdle)
                SetAnimationForObject(animObjBody, 0, AnimStages.DefaultShoot, false);
        }

        public void StartDash(bool toRight = false, float timeScale = 1f)
        {
            _meshBody.enabled = false;
            _meshDash.enabled = true;

            ArrowVisible = false;

            animObjDash.timeScale = timeScale;

            animObjDash.transform.localScale = toRight ? _dashScaleToRight : _dashScaleToLeft;

            SpawnDashDust(AnimStages.DashEnter);

            SetAnimationForObject(animObjDash, 0, AnimStages.DashEnter, false);
        }

        public void EndDash()
        {
            SetAnimationForObject(animObjDash, 0, AnimStages.DashExit, false);
            // SpawnDashDust(AnimStages.DASH_EXIT);
        }

        public void SpawnDashDust(string stage)
        {
            var position = gameObject.transform.position;
            var fx = stage switch
            {
                AnimStages.DashEnter => Instantiate(dashStartDust, position, Quaternion.identity),
                AnimStages.DashExit => Instantiate(dashEndDust, position, Quaternion.identity),
                _ => null
            };

            if (fx == null) return;
            fx.transform.localScale = new Vector3(animObjDash.transform.localScale.x >= 0 ? 1 : -1, 1, 1);
            fx.sortingOrder = _meshBody.sortingOrder + 1;
        }

        public float GetDashTimeScale(int steps)
        {
            return dashTimeScaleCurve.Evaluate(Mathf.Clamp(steps, 0, 4));
        }

        public void StartDeath()
        {
            _meshBody.enabled = true;
            _meshDash.enabled = false;
            ArrowVisible = false;
            SetAnimationForObject(animObjBody, 0, AnimStages.DefaultDeath, false);
        }

        public static class AnimStages
        {
            public const string DefaultIdle = "idle";
            public const string DefaultReload = "nachladen";
            public const string DefaultShoot = "schießen";
            public const string DefaultDeath = "sterben";

            public const string DashEnter = "dashEnter";
            public const string DashExit = "dashExit";
            public const string DashLoop = "dashLoop";
        }

        public static class AnimationTags
        {
            public const string BowStretch = "e_player_bow_stretch";
            public const string ArrowLetGO = "e_player_arrow_let_go";
            public const string ReloadArrow = "e_player_reload_arrow";
            public const string ArrowTouchString = "e_player_arrow_touch_string";
        }
    }
}