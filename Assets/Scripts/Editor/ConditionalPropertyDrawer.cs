using System;
using UnityEditor;
using UnityEngine;

namespace AVB
{
    [CustomPropertyDrawer(typeof(ConditionalAttribute))]
    public class ConditionalPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var attr = (ConditionalAttribute) attribute;
            var enabled = GetConditionalAttributeResult(attr, property);
            
            var wasEnabled = GUI.enabled;
            GUI.enabled = enabled;
            if (!attr.hideInInspector || enabled)
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
            
            GUI.enabled = wasEnabled;
        }
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var attr = (ConditionalAttribute) attribute;
            var enabled = GetConditionalAttributeResult(attr, property);
        
            if (!attr.hideInInspector || enabled)
            {
                return EditorGUI.GetPropertyHeight(property, label);
            }
        
            return -EditorGUIUtility.standardVerticalSpacing;
        }

        private bool GetConditionalAttributeResult(ConditionalAttribute condHAtt, SerializedProperty property)
        {
            var attr = (ConditionalAttribute) attribute;
            var enabled = true;
            var propertyPath =
                property.propertyPath; //returns the property path of the property we want to apply the attribute to
            var conditionPath = propertyPath.Replace(property.name,
                condHAtt.fieldName); //changes the path to the conditionalsource property path
            var sourcePropertyValue = property.serializedObject.FindProperty(conditionPath);

            if (sourcePropertyValue != null)
            {
                enabled = sourcePropertyValue.type switch
                {
                    "bool" => sourcePropertyValue.boolValue,
                    "int" => sourcePropertyValue.intValue == attr.defaultInt,
                    "float" => Math.Abs(sourcePropertyValue.floatValue - attr.defaultFloat) < 0.000001f,
                    "double" => Math.Abs(sourcePropertyValue.doubleValue - attr.defaultDouble) < 0.000001d,
                    "string" => sourcePropertyValue.stringValue == attr.defaultString,
                    "Enum" => sourcePropertyValue.intValue == attr.defaultInt,
                    _ => true
                };
            }
            else
            {
                Debug.LogWarning(
                    "Attempting to use a ConditionalAttribute but no matching SourcePropertyValue found in object: " +
                    condHAtt.fieldName);
            }

            return enabled;
        }
    }
}