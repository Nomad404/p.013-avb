﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AVB
{
    [Serializable]
    public struct Rune
    {
        public RuneType majorType;
        public int subType;
        public Params runeParams;

        public Rune(RuneType majorType = RuneType.Default, int subType = 0, Params runeParams = null)
        {
            this.majorType = majorType;
            this.subType = subType;
            this.runeParams = runeParams;
        }

        public Rune(int major, int sub) : this((RuneType)major, sub)
        {
        }

        public static Rune Empty => new(RuneType.Default, 0);

        public static Rune Attack => new(RuneType.General, (int)GeneralRune.Attack);

        [Obsolete("Rune was replaced by tap on bridge segment")]
        public static Rune Move => new(RuneType.General, (int)GeneralRune.Move);

        public static Rune Clear => new(RuneType.General, (int)GeneralRune.Clear);

        public static Rune Triangle => new(RuneType.Low, (int)LowAttackRune.Triangle);

        public static Rune Square => new(RuneType.Low, (int)LowAttackRune.Square);

        public static Rune Circle => new(RuneType.Low, (int)LowAttackRune.Circle);

        public static Rune NoColor => new(RuneType.Mid);

        public static Rune Red => new(RuneType.Mid, (int)MidAttackRune.Red);

        public static Rune Green => new(RuneType.Mid, (int)MidAttackRune.Green);

        public static Rune Blue => new(RuneType.Mid, (int)MidAttackRune.Blue);

        public static Rune Yellow => new(RuneType.Mid, (int)MidAttackRune.Yellow);

        public static List<Rune> LowRunes => new() { Triangle, Square, Circle };
        public static List<Rune> MidRunes => new() { Red, Green, Blue, Yellow };

        public static bool operator ==(Rune r1, Rune r2)
        {
            return r1.majorType == r2.majorType && r1.subType == r2.subType;
        }

        public static bool operator !=(Rune r1, Rune r2)
        {
            return !(r1 == r2);
        }

        [Serializable]
        public class Params
        {
        }

        public override bool Equals(object obj)
        {
            if (obj is not Rune rune) return false;

            return majorType == rune.majorType &&
                   subType == rune.subType &&
                   EqualityComparer<Params>.Default.Equals(runeParams, rune.runeParams);
        }

        public override string ToString()
        {
            var result = $"{majorType}.";
            switch (majorType)
            {
                case RuneType.General:
                    result += $"{(GeneralRune)subType}";
                    break;
                case RuneType.Default:
                    result += "invalid";
                    break;
                case RuneType.Low:
                    result += $"{(LowAttackRune)subType}";
                    break;
                case RuneType.Mid:
                    result += $"{(MidAttackRune)subType}";
                    break;
                case RuneType.High:
                    result += $"{(HighAttackRune)subType}";
                    break;
            }

            if (this == Empty) result = "Rune.Empty";
            return result;
        }

        public override int GetHashCode()
        {
            return majorType.GetHashCode() ^ subType.GetHashCode() ^ runeParams.GetHashCode();
        }
    }

    // Used for rune based player movement, not currently in use
    [Serializable]
    public abstract class MoveRuneParams : Rune.Params
    {
        public Vector3 startPoint;
        public Vector3 endPoint;

        protected MoveRuneParams(Vector3 start, Vector3 end)
        {
            startPoint = start;
            endPoint = end;
        }
    }

    [Serializable]
    public class RuneSet
    {
        public Rune[] runes;

        public RuneSet(int size)
        {
            runes = new Rune[size];
            for (var i = 0; i < runes.Length; i++)
            {
                runes[i] = Rune.Empty;
            }
        }

        public void UpdateRunes(Rune[] newRunes)
        {
            for (var i = 0; i < runes.Length; i++)
            {
                if (i < newRunes.Length)
                {
                    runes[i] = newRunes[i];
                }
            }
        }

        public override string ToString()
        {
            var result = "";
            for (var i = 0; i < runes.Length; i++)
            {
                result += $"{i}: {runes[i].ToString()}; ";
            }

            return result;
        }
    }

    // 
    [Serializable]
    public class AttackRuneSet : RuneSet
    {
        public Rune LowRune => runes[0];

        public LowAttackRune LowRuneType => (LowAttackRune)runes[0].subType;

        public Rune MidRune => runes[1];

        public MidAttackRune MidRuneType => (MidAttackRune)runes[1].subType;

        public Rune HighRune => runes[2];

        public HighAttackRune HighRuneType => (HighAttackRune)runes[2].subType;

        public static AttackRuneSet Empty => new();

        public AttackRuneSet(
            LowAttackRune low = LowAttackRune.Default,
            MidAttackRune mid = MidAttackRune.Default,
            HighAttackRune high = HighAttackRune.Default
        ) : base(3)
        {
            runes[0] = new Rune(RuneType.Low, (int)low);
            runes[1] = new Rune(RuneType.Mid, (int)mid);
            runes[2] = new Rune(RuneType.High, (int)high);
        }

        public static AttackRuneSet Random => new(
            (LowAttackRune)UnityEngine.Random.Range(1, 4),
            UnityEngine.Random.Range(0, 3) > 1
                ? (MidAttackRune)UnityEngine.Random.Range(1, 5)
                : MidAttackRune.Default);

        public void SetRune(Rune rune)
        {
            switch (rune.majorType)
            {
                case RuneType.Low:
                    runes[0] = rune;
                    break;
                case RuneType.Mid:
                    runes[1] = rune;
                    break;
                case RuneType.High:
                    runes[2] = rune;
                    break;
            }
        }

        public bool IsEmpty()
        {
            return this == Empty;
        }

        public override string ToString()
        {
            return $"attack set: low: {LowRune.subType}, mid: {MidRune.subType}, high: {HighRune.subType}";
        }

        public static bool operator ==(AttackRuneSet s1, AttackRuneSet s2)
        {
            return s1?.LowRune == s2?.LowRune
                   && s1?.MidRune == s2?.MidRune
                   && s1?.HighRune == s2?.HighRune;
        }

        public static bool operator !=(AttackRuneSet s1, AttackRuneSet s2)
        {
            return !(s1 == s2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var set = (AttackRuneSet)obj;
            return LowRune == set.LowRune && MidRune == set.MidRune && HighRune == set.HighRune;
        }

        public override int GetHashCode()
        {
            return LowRune.GetHashCode() ^ MidRune.GetHashCode() ^ HighRune.GetHashCode();
        }

        public MatchType CompareTo(RuneSet set)
        {
            var attackSet = FromRuneSet(set);
            return attackSet != null ? CompareTo(attackSet) : MatchType.NoMatch;
        }

        public MatchType CompareTo(AttackRuneSet targetSet)
        {
            if (LowRune != targetSet.LowRune) return MatchType.NoMatch;
            if (MidRune == Rune.NoColor && targetSet.MidRune != Rune.NoColor) return MatchType.NoMatch;
            if (MidRune == targetSet.MidRune && MidRune != Rune.NoColor)
                return MatchType.PartialMatch;
            return MatchType.FullMatch;
        }

        public static AttackRuneSet FromRuneSet(RuneSet set)
        {
            if (set.runes.Length is <= 0 or > 3) return null;
            var index = set.runes.Length - 1;
            var low = (int)LowAttackRune.Default;
            var mid = (int)MidAttackRune.Default;
            var high = (int)HighAttackRune.Default;
            while (index >= 0)
            {
                if (set.runes[index].majorType == RuneType.Low)
                {
                    low = set.runes[index].subType;
                    break;
                }

                index--;
            }

            if (low == (int)LowAttackRune.Default) return null;
            index++;
            if (index < set.runes.Length && set.runes[index].majorType == RuneType.Mid)
                mid = set.runes[index].subType;
            index++;
            if (index < set.runes.Length && set.runes[index].majorType == RuneType.High)
                mid = set.runes[index].subType;

            return new AttackRuneSet((LowAttackRune)low, (MidAttackRune)mid, (HighAttackRune)high);
        }

        public enum MatchType
        {
            NoMatch = 0,
            PartialMatch = 1,
            FullMatch = 2
        }
    }

    [Serializable]
    public enum RuneType
    {
        Default = 0,
        General = 1,
        SkyDirection = 2,
        Low = 3,
        Mid = 4,
        High = 5
    }

    [Serializable]
    public enum GeneralRune
    {
        Default = 0,
        Move = 1,
        Attack = 2,
        Clear = 3
    }

    [Serializable]
    public enum LowAttackRune
    {
        Default = 0,
        Triangle = 1,
        Square = 2,
        Circle = 3
    }

    [Serializable]
    public enum MidAttackRune
    {
        Default = 0,
        Red = 1,
        Green = 2,
        Blue = 3,
        Yellow = 4
    }

    [Serializable]
    public enum HighAttackRune
    {
        Default = 0
    }

    [Serializable]
    public enum ResponseType
    {
        Default = 0,
        Selected = 1,
        Accepted = 2,
        Error = 3
    }
}