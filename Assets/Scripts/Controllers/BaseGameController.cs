using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.UI;

namespace AVB
{
    public class BaseGameController : MonoBehaviour
    {
        public static BaseGameController instance;

        private SpriteRenderer background;
        private AudioSource _audioSource;

        private Player _player;
        private static GameController GameController => GameController.instance;
        private static EnemyController EnemyController => EnemyController.instance;
        private static BridgeController BridgeController => BridgeController.instance;
        private static UIController UIController => UIController.instance;
        private static UIMenu UIMenu => UIMenu.instance;
        private static QuickMessage QM => QuickMessage.instance;

        public LocalizedString goString;

        private void Awake()
        {
            if (!instance) instance = this;

            background = GameObject.FindGameObjectWithTag(Tags.Background).GetComponent<SpriteRenderer>();
            background.sortingOrder = SpriteLayers.Background;
            _player = FindObjectOfType<Player>();
            _audioSource = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<AudioSource>();
        }

        private void Start()
        {
            BridgeController.onBridgeDestroyed.AddListener(InitGameOver);
        }

        private void OnDestroy()
        {
            BridgeController.onBridgeDestroyed.RemoveListener(InitGameOver);
        }

        public void LoadNextLevel()
        {
            StartCoroutine(Seq_LoadNextLevel());
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus) UIMenu.instance.OpenPauseMenu();
        }

        private static IEnumerator Seq_LoadNextLevel()
        {
            yield return UIController.instance.Seq_FadeForegroundOverlay(true);
            GameController.LoadLevel(LevelNames.LoadingScreen);
        }

        public void InitGameOver(int startIndex)
        {
            StartCoroutine(Seq_GameOver(startIndex));
        }

        public void LoadMainMenu()
        {
            GameTime.IsPaused = false;
            GameController.LoadLevel(LevelNames.StartMenu);
        }

        public void ReloadLevel(bool reduceCurrentLevel = false)
        {
            // Necessary, cuz at the time level is already increased
            GameTime.IsPaused = false;
            if (reduceCurrentLevel) GameController.CurrentLevel--;
            GameController.LoadLevel(LevelNames.LoadingScreen);
        }

        #region Sequences

        public IEnumerator Seq_StartNewLevel(LocalizedString headerText, LocalizedString subtitleText)
        {
            UIController.FadeForegroundOverlay(false);

            BridgeController.bridgeEdgeLeft.baseSprite.sortingOrder = SpriteLayers.Player - 1;
            var segmentStartX = BridgeController.bridgeSegments[_player.CurrentBridgeIndex].transform.position.x;
            yield return _player.Seq_DashToLocation(new Vector3(segmentStartX, 0), 1f, true, 1.5f);
            BridgeController.bridgeEdgeLeft.baseSprite.sortingOrder = SpriteLayers.BridgeSeg;
            _player.CanReceiveMoveRequests = true;

            yield return new WaitForSeconds(1f);

            // Set header text
            if (!headerText.IsEmpty)
            {
                var size = UIController.headerText.rectTransform.sizeDelta;
                size = new Vector2(size.x, 100);
                UIController.SetUIText(UIController.headerText, headerText, size);
            }

            // Set subtitle text
            if (!subtitleText.IsEmpty)
            {
                var size = UIController.subtitleText.rectTransform.sizeDelta;
                size = new Vector2(size.x, 200);
                UIController.SetUIText(UIController.subtitleText, subtitleText, size);
            }

            yield return UIController.Seq_FadeTextElements(
                new List<TextMeshProUGUI> {UIController.headerText, UIController.subtitleText}, 0f, 1f,
                0.5f);

            yield return new WaitForSeconds(0.5f);

            // 3,2,1
            for (var i = 3; i > 0; i--)
            {
                yield return QM.Seq_ShowMessage($"{i}", new Vector3(1.5f, 1.5f, 1f), Vector3.one, 0.5f);
            }

            // Go
            var goText = goString.IsEmpty ? "Go" : goString.GetLocalizedString();
            yield return QM.Seq_ShowMessage(goText, new Vector3(1.5f, 1.5f, 1f), Vector3.one, 0.5f,
                true);
            _audioSource.PlayOneShot(AudioResourceController.instance.levelNewWave);
            yield return UIController.Seq_FadeTextElements(
                new List<TextMeshProUGUI> {UIController.headerText, UIController.subtitleText}, 1f, 0f,
                0.5f);
            UIController.headerText.text = "";
        }

        /// <summary>
        /// Moves and disables the player and shows menu to continue
        /// </summary>
        /// <returns></returns>
        public IEnumerator Seq_FinishLevel(int score = 3)
        {
            //DEBUG: Save score in prefs
            _player.CanReceiveMoveRequests = false;
            yield return new WaitForSeconds(1f);
            var exitPos = new Vector2(14, 0);
            BridgeController.bridgeEdgeRight.baseSprite.sortingOrder = SpriteLayers.Player - 1;
            yield return _player.Seq_DashToLocation(exitPos, 1f, shouldWait: true, dashTimeScale: 1.5f);

            // yield return UIController.INSTANCE.Seq_FadeForegroundOverlay(true);
            // yield return Observable.Timer(TimeSpan.FromSeconds(0.5f)).ToYieldInstruction();

            _audioSource.PlayOneShot(AudioResourceController.instance.levelWin, 1.1f);
            UIMenu.MenuMode = UIMenu.Mode.Score;
            UIMenu.OpenMenu();
            yield return new WaitForSeconds(0.5f);
            var scoreSlider = UIMenu.GetComponentInChildren<ScoreSlider>();
            GameController.currentSaveGame.levelScores[GameController.CurrentLevel] = score;
            GameController.CurrentLevel++;
            GameController.UpdateSaveGame();
            yield return scoreSlider.Seq_SetScoreAnimated(score, scoreIndex =>
            {
                var rankSounds = AudioResourceController.instance.levelStarRanks;
                if (scoreIndex < rankSounds.Length)
                {
                    _audioSource.PlayOneShot(rankSounds[scoreIndex], 0.5f);
                }
            });
            UIMenu.menus.First(r => r.mode == UIMenu.Mode.Score).menu.GetComponentsInChildren<Button>().ToList()
                .ForEach(button => button.interactable = true);
        }

        public IEnumerator Seq_GameOver(int startIndex)
        {
            _player.CanReceiveMoveRequests = false;
            EnemyController.canSpawnEnemies = false;
            EnemyController.DisableAllEnemies();
            var leftIndex = startIndex;
            var rightIndex = startIndex;
            while (leftIndex >= 0 || rightIndex < BridgeController.VisibleSegmentCount)
            {
                if (leftIndex >= 0)
                {
                    BridgeController.bridgeSegments[leftIndex].Kill();
                    if (leftIndex == _player.CurrentBridgeIndex)
                    {
                        _player.Kill();
                    }

                    leftIndex -= 1;
                }

                if (rightIndex < BridgeController.VisibleSegmentCount)
                {
                    BridgeController.bridgeSegments[rightIndex].Kill();
                    if (rightIndex == _player.CurrentBridgeIndex)
                    {
                        _player.Kill();
                    }

                    rightIndex += 1;
                }

                yield return new WaitForSeconds(0.3f);
            }

            _audioSource.PlayOneShot(AudioResourceController.instance.levelLose);
            UIMenu.MenuMode = UIMenu.Mode.GameOver;
            UIMenu.OpenMenu();
        }

        #endregion
    }
}