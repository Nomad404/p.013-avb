using UnityEngine;
using UnityEngine.UI;

namespace AVB
{
    [RequireComponent(typeof(Toggle))]
    public class VolumeToggleButton : MonoBehaviour
    {
        public Image swapIcon;
        public Sprite onSprite;
        public Sprite offSprite;

        private Toggle _toggle;

        private void Awake()
        {
            _toggle = GetComponent<Toggle>();
            _toggle.onValueChanged.AddListener(ToggleVolume);
            var isVolumeOn = PlayerPrefs.GetInt(PlayerPrefKeys.IsVolumeOn, 1);
            ToggleVolume(isVolumeOn != 0);
        }

        private void OnDestroy()
        {
            _toggle.onValueChanged.RemoveListener(ToggleVolume);
        }

        private void ToggleVolume(bool isOn)
        {
            swapIcon.sprite = isOn ? onSprite : offSprite;
            AudioListener.volume = isOn ? 1f : 0f;
            PlayerPrefs.SetInt(PlayerPrefKeys.IsVolumeOn, isOn ? 1 : 0);
            PlayerPrefs.Save();
        }
    }
}