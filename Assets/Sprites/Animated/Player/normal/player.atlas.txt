
player.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
bogen
  rotate: false
  xy: 0, 394
  size: 452, 116
  orig: 452, 116
  offset: 0, 0
  index: -1
cape
  rotate: false
  xy: 656, 341
  size: 140, 169
  orig: 140, 169
  offset: 0, 0
  index: -1
köcher
  rotate: false
  xy: 0, 122
  size: 283, 170
  orig: 283, 170
  offset: 0, 0
  index: -1
linke hand
  rotate: false
  xy: 887, 388
  size: 96, 122
  orig: 96, 122
  offset: 0, 0
  index: -1
linker arm
  rotate: false
  xy: 283, 172
  size: 29, 120
  orig: 29, 120
  offset: 0, 0
  index: -1
mütze
  rotate: false
  xy: 452, 321
  size: 204, 189
  orig: 204, 189
  offset: 0, 0
  index: -1
oberbeinL
  rotate: false
  xy: 213, 46
  size: 31, 76
  orig: 31, 76
  offset: 0, 0
  index: -1
oberbeinR
  rotate: false
  xy: 355, 320
  size: 36, 74
  orig: 36, 74
  offset: 0, 0
  index: -1
pfeil
  rotate: false
  xy: 796, 154
  size: 91, 356
  orig: 91, 356
  offset: 0, 0
  index: -1
rechte hand
  rotate: false
  xy: 107, 23
  size: 106, 99
  orig: 106, 99
  offset: 0, 0
  index: -1
rechter arm
  rotate: false
  xy: 887, 210
  size: 54, 53
  orig: 54, 53
  offset: 0, 0
  index: -1
sähne
  rotate: false
  xy: 0, 292
  size: 355, 102
  orig: 355, 102
  offset: 0, 0
  index: -1
torso
  rotate: false
  xy: 887, 263
  size: 73, 125
  orig: 73, 125
  offset: 0, 0
  index: -1
unterbein R
  rotate: false
  xy: 312, 227
  size: 37, 65
  orig: 37, 65
  offset: 0, 0
  index: -1
unterbeinL
  rotate: false
  xy: 983, 441
  size: 41, 69
  orig: 41, 69
  offset: 0, 0
  index: -1
zipfel
  rotate: false
  xy: 0, 0
  size: 107, 122
  orig: 107, 122
  offset: 0, 0
  index: -1
