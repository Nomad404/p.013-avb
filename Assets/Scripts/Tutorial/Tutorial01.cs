using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AVB.Tutorial
{
    public class Tutorial01 : TutorialScript
    {
        protected override int StepCount => 10;

        public override IEnumerator Seq_StartScript()
        {
            yield return base.Seq_StartScript();
            Player.CanAttack = false;
            yield return new WaitForSeconds(1f);

            // 1. Templates
            var enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Square))
            };
            enemies.ForEach(e => e.CanBeKilled = false);
            yield return FadeElement(steps[0], 1f, 0.5f);
            yield return new WaitForSeconds(5f);
            yield return FadeElement(steps[0], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 2. Draw square
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CharacterState = CharacterState.Idle;
            });
            yield return FadeElement(steps[1], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Square);
            yield return FadeElement(steps[1], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 3. Selection yellow
            yield return FadeElement(steps[2], 1f, 0.5f);
            yield return new WaitForSeconds(5f);
            yield return FadeElement(steps[2], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 4. Attack
            yield return FadeElement(steps[3], 1f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            yield return new WaitForRuneInput(Rune.Attack);
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            Player.CanAttack = false;
            yield return FadeElement(steps[3], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 5. Draw corner
            enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Square), -10)
            };
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CanBeKilled = false;
                e.CharacterState = CharacterState.Idle;
            });
            yield return FadeElement(steps[4], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Square);
            yield return FadeElement(steps[4], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 6. Attack again
            yield return FadeElement(steps[5], 1f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            yield return new WaitForRuneInput(Rune.Attack);
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            yield return FadeElement(steps[5], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 7. Draw 4 squares
            yield return FadeElement(steps[6], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Square);
            yield return new WaitForRuneInput(Rune.Square);
            yield return new WaitForRuneInput(Rune.Square);
            yield return FadeElement(steps[6], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 8. Multiple runes in queue
            yield return FadeElement(steps[7], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Square);
            yield return FadeElement(steps[7], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 09. Dismiss
            yield return FadeElement(steps[8], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Clear);
            yield return FadeElement(steps[8], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 10. Open pause menu
            yield return FadeElement(steps[9], 1f, 0.5f);
            yield return new WaitForMenuMode(UIMenu.Mode.Pause);
            GameTime.IsPaused = false;
            yield return FadeElement(steps[9], 0f, 0.5f);
            yield return new WaitForMenuState(UIMenu.State.Closed, 3f);
            UIMenu.instance.CloseMenu();

#if (UNITY_ANDROID || UNITY_IPHONE)
            if (GameServices.Instance.IsLoggedIn())
            {
                GameServices.Instance.SubmitAchievement(AchievementNames.Goodstart);
            }
#endif

            yield return BaseGame.Seq_FinishLevel();
        }
    }
}