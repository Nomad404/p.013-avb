﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace AVB
{
    public class Utils : MonoBehaviour
    {
        public static Vector3 GetScreenBorderPoint(ScreenBorder border)
        {
            var result = Vector3.zero;
            if (Camera.main is null) return result;
            result = border switch
            {
                ScreenBorder.Top => Camera.main.ScreenToWorldPoint(new Vector2(0, Camera.main.pixelHeight)),
                ScreenBorder.Bottom => Camera.main.ScreenToWorldPoint(new Vector2(0, 0)),
                ScreenBorder.Left => Camera.main.ScreenToWorldPoint(new Vector2(0, 0)),
                ScreenBorder.Right => Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, 0)),
                _ => result
            };

            return result;
        }

        public static Vector3 GetPointToScreenBorder(Vector3 origin, ScreenBorder border, float multiplier)
        {
            var screenPoint = GetScreenBorderPoint(border);
            var direction = (screenPoint - origin).normalized;
            var newDistance = (screenPoint - origin).magnitude * multiplier;
            return direction * newDistance;
        }

        public static float GetCameraSizeByRatio(float ratio)
        {
            return 40 * (1 - ratio);
        }

        public static float GetCameraYPosByRatio(float ratio)
        {
            return 25f * (1 - ratio);
        }

        public static Color GetColorByGestureID(string id)
        {
            return id switch
            {
                RunePatternIDs.LineVertical => Color.black,
                RunePatternIDs.LineDiagDownRight => GetColorByMidRune((int)MidAttackRune.Red),
                RunePatternIDs.LineDiagDownLeft => GetColorByMidRune((int)MidAttackRune.Green),
                RunePatternIDs.LineDiagTopLeft => GetColorByMidRune((int)MidAttackRune.Blue),
                RunePatternIDs.LineDiagTopRight => GetColorByMidRune((int)MidAttackRune.Yellow),
                // case GesturePatternIDs.RUNE_WAVE_VERTICAL: return GetColorByMidRune((int)MidAttackRune.Red);
                // case GesturePatternIDs.RUNE_WAVE_HORIZONTAL: return GetColorByMidRune((int)MidAttackRune.Blue);
                // case GesturePatternIDs.RUNE_CROSS: return GetColorByMidRune((int)MidAttackRune.Orange);
                // case GesturePatternIDs.RUNE_PARALLEL_HORIZONTAL: return GetColorByMidRune((int)MidAttackRune.Green); 
                RunePatternIDs.LineHorizontal => Color.black,
                _ => Color.white
            };
        }

        public static Color GetColorByMidRune(int type)
        {
            return (MidAttackRune)type switch
            {
                MidAttackRune.Blue => new Color(0.6156863f, 0.3803922f, 0.8352941f, 1f),
                MidAttackRune.Red => new Color(0.9058824f, 0.3137255f, 0.4705882f, 1f),
                MidAttackRune.Green => new Color(0.05882353f, 0.7058824f, 0.5450981f, 1f),
                MidAttackRune.Yellow => new Color(0.9921569f, 0.7647059f, 0.1803922f, 1f),
                _ => Color.white
            };
        }

        public static T LoadJsonDataFromFile<T>(string filename)
        {
            var filepath = Path.Combine(Application.streamingAssetsPath, filename);
            var dataAsJsonString = "";
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    filepath = $"jar:file://{Application.dataPath}!/assets/{filename}";
                    var request = UnityWebRequest.Get(filepath);
                    request.SendWebRequest();
                    while (!request.isDone)
                    {
                    }
                    dataAsJsonString = System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);
                    break;
                case RuntimePlatform.IPhonePlayer:
                    // filepath = $"{Application.dataPath}/Raw/{filename}";
                    if (File.Exists(filepath))
                    {
                        dataAsJsonString = File.ReadAllText(filepath);
                    }
                    else
                    {
                        Debug.LogError($"{filepath} does not exist");
                    }

                    break;
                default:
                    dataAsJsonString = File.ReadAllText(filepath);
                    break;
            }

            return JsonUtility.FromJson<T>(dataAsJsonString);
        }
    }

    public static class VectorExtension
    {
        public static Vector2 ToVector2(this Vector3 original)
        {
            return new Vector2(original.x, original.y);
        }

        public static Vector2 RotateAroundPoint(this Vector2 point, Vector2 axis, Vector3 angle)
        {
            var dir = point - axis;
            dir = Quaternion.Euler(angle) * dir;
            var result = dir + axis;
            return result;
        }
    }

    public static class ColorExtension
    {
        public static float GetMagnitude(this Color target)
        {
            return Mathf.Sqrt(
                target.r * target.r
                + target.g * target.g
                + target.b * target.b
                + target.a * target.a
            );
        }
    }

    public enum ScreenBorder
    {
        Top = 1,
        Bottom = 2,
        Left = 3,
        Right = 4
    }
}