﻿using UniRx;
using UnityEngine;

namespace AVB
{
    public class Enemy : Character
    {
        public int SegmentIndex { get; set; } = -1;

        private AttackRuneSet _runeSet = new();

        public AttackRuneSet RuneSet
        {
            get => _runeSet;
            set
            {
                _runeSet = value;
                RespawnEnemySprite();
                selectionCircle.sortingOrder = SelectionSortingOrder;
            }
        }

        public const float DefaultEnemySpeed = 1.5f;
        private const float BridgeDamageAmount = 10.0f;

        public bool IsOnScreen =>
            LevelBoundaryController.instance.screenBoundaries.IsWithinBoundary(transform.position);

        [Header("Enemy properties")] public EnemyRenderType renderType = EnemyRenderType.Basic;
        public GameObject socketSpineObject;

        [HideInInspector] public EnemySpriteController spriteController;
        public EnemySpriteController[] enemySpinePrefabs = new EnemySpriteController[3];
        [Header("FX")] public SpriteRenderer hitFXPrefab;

        [Header("Selection")] [SerializeField] private Color selectionColorFullMatch = new Color(1, 1, 0, 0.8f);
        [Header("Selection")] [SerializeField] private Color selectionColorPartialMatch = new Color(1, 0, 0, 0.8f);
        [SerializeField] private SpriteRenderer selectionCircle;

        private int BaseSortingOrder => spriteController.BaseSortingOrder;
        private int SelectionSortingOrder => BaseSortingOrder + (int)EnemyPartType.Selection;

        public int TorsoSortingOrder => BaseSortingOrder + 1;

        private IDamageable _attackTarget;

        private const float MAXHealth = 100.0f;

        private static RuneController RuneController => RuneController.instance;

        private AudioSource _audioSource;
        private CircleCollider2D _circleCollider2D;
        private static Player Player => Player.instance;

        public float DistanceToPlayer => Vector2.Distance(gameObject.transform.position, Player.transform.position);

        public float DistanceToBridge
        {
            get
            {
                var count = BridgeController.instance.VisibleSegmentCount;
                Vector2 end;
                if (count > 0 && SegmentIndex < count &&
                    BridgeController.instance.bridgeSegments[SegmentIndex] != null)
                {
                    end = BridgeController.instance.bridgeSegments[SegmentIndex].transform.position;
                }
                else if (moveTarget != null)
                {
                    end = moveTarget.transform.position;
                }
                else
                {
                    end = moveTargetPosition;
                }

                return Vector2.Distance(gameObject.transform.position, end);
            }
        }

        private new void Awake()
        {
            base.Awake();
            _circleCollider2D = GetComponent<CircleCollider2D>();
            _audioSource = GetComponent<AudioSource>();
            directionMethod = DirectionMethod.Position;
            CanMove = false;
            moveSpeed = DefaultEnemySpeed;
            moveLockedX = true;
            obstacleCheckDistance = _circleCollider2D.radius * transform.localScale.x * 1.01f;
            onNewCharacterState += CharacterStateChanged;
            Health = MAXHealth;
            UpdateSelectionCircle();
        }

        private void Start()
        {
            RuneController.rxLatestRune.Skip(1).Subscribe(OnNewRune).AddTo(this);
            Player.onNewBridgeIndex.AddListener(OnNewPlayerBridgeIndex);
        }

        private new void OnDestroy()
        {
            Player.onNewBridgeIndex.RemoveListener(OnNewPlayerBridgeIndex);
            base.OnDestroy();
        }

        private void OnNewRune(Rune rune)
        {
            UpdateSelectionCircle();
        }

        private void OnNewPlayerBridgeIndex(int index)
        {
            UpdateSelectionCircle();
        }

        private void UpdateSelectionCircle()
        {
            var runeSet = AttackRuneSet.FromRuneSet(RuneController.CurrentRuneSet);
            if (runeSet != null && SegmentIndex == Player.CurrentBridgeIndex)
            {
                var matchType = runeSet.CompareTo(RuneSet);
                selectionCircle.color = matchType switch
                {
                    AttackRuneSet.MatchType.PartialMatch => selectionColorPartialMatch,
                    AttackRuneSet.MatchType.FullMatch => selectionColorFullMatch,
                    _ => Color.clear
                };
            }
            else
            {
                selectionCircle.color = Color.clear;
            }
        }

        protected override void OnCantMove(RaycastHit2D hitResult)
        {
            _attackTarget = hitResult.transform.gameObject.GetComponent<IDamageable>();
            if (_attackTarget == null) hitResult.transform.gameObject.GetComponentInParent<IDamageable>();
            if (_attackTarget != null)
            {
                if (_attackTarget.GetType() != typeof(Enemy))
                {
                    CharacterState = CharacterState.Attacking;
                }
            }
            else CharacterState = CharacterState.Idle;
        }

        private void CharacterStateChanged(bool hasChanged)
        {
            if (!hasChanged) return;
            switch (CharacterState)
            {
                case CharacterState.Default: break;
                case CharacterState.Idle:
                    UpdateAnimationState();
                    break;
                case CharacterState.Moving:
                    UpdateAnimationState();
                    break;
                case CharacterState.Attacking:
                    if (CanAttack) UpdateAnimationState();
                    else CharacterState = CharacterState.Idle;
                    break;
                case CharacterState.Stunned: break;
                case CharacterState.Death:
                    UpdateAnimationState();
                    break;
            }
        }

        private void UpdateAnimationState()
        {
            // set proper animation for the object according to state
            switch (renderType)
            {
                case EnemyRenderType.Basic: break;
                case EnemyRenderType.Spine:
                    spriteController?.SetAnimState(CharacterState);
                    break;
            }
        }

        public void AttackTarget()
        {
            _attackTarget?.TakeDamage(BridgeDamageAmount);
        }

        private void RespawnEnemySprite()
        {
            if ((MidAttackRune)RuneSet.MidRune.subType != MidAttackRune.Default)
            {
                moveSpeed = DefaultEnemySpeed * 0.7f;
            }

            switch (renderType)
            {
                case EnemyRenderType.Basic: break;
                case EnemyRenderType.Spine:
                    if (spriteController)
                    {
                        Destroy(spriteController);
                    }

                    spriteController = Instantiate(enemySpinePrefabs[(int)RuneSet.LowRuneType - 1],
                        socketSpineObject.transform);
                    spriteController.UpdateResistanceColor(RuneSet.MidRune.subType);
                    break;
            }
        }

        protected override void OnObjectMove()
        {
            CharacterState = CharacterState.Moving;
        }

        protected override void OnObjectStop()
        {
        }

        public void BlockDamage(float knockBackForce = 1f)
        {
            _audioSource.PlayOneShot(AudioResourceController.instance.RandomEnemyBlockSound);
            var hitFX = Instantiate(hitFXPrefab, transform.position, Quaternion.identity);
            hitFX.sortingOrder = BaseSortingOrder + 5;
            if (CharacterState == CharacterState.Moving) knockBackForce *= 10f;
            rigid2D.AddForce(Vector2.up * knockBackForce, ForceMode2D.Impulse);
        }

        public void TakeDamage(float amount)
        {
            Health -= MAXHealth;
            var hitFX = Instantiate(hitFXPrefab, transform.position, Quaternion.identity);
            hitFX.sortingOrder = BaseSortingOrder + 5;
            // audioSource.PlayOneShot(damageSounds[Random.Range(0, deathSounds.Length)]);
        }

        public override void Kill()
        {
            Player.instance.KillCount++;
            CanMove = false;
            CanAttack = false;
            _circleCollider2D.isTrigger = true;
            selectionCircle.color = Color.clear;
            CharacterState = CharacterState.Death;
            _audioSource.PlayOneShot(AudioResourceController.instance.RandomEnemyDeathSound);
            EnemyController.instance.OnEnemyKilled(this);
        }

        protected override void OnHealthDepleted()
        {
            Kill();
        }

        public void Disable()
        {
            // currentMoveSpeed = 0.0f;
            CanMove = false;
            CharacterState = CharacterState.Idle;
            if (!spriteController) return;
            spriteController.SetAnimState(CharacterState);
            CanAttack = false;
        }

        public void UpdateSortingOrder(int newBaseOrder)
        {
            spriteController.UpdateSortingOrder(newBaseOrder);
            selectionCircle.sortingOrder = SelectionSortingOrder;
        }
    }
}

public enum EnemyRenderType
{
    Basic = 0,
    Spine = 1
}