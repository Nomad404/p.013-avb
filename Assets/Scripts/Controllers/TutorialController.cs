using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;

namespace AVB.Tutorial
{
    public class TutorialController : CustomLevelController<TutorialLevelConfig>
    {
        public TextMeshProUGUI headerText;
        private const string TutorialHeaderKey = "tutorial_header_suffix";

        private TutorialScript script;

        protected override void Awake()
        {
            base.Awake();
            script = GetComponent<TutorialScript>();
            EnemyController.instance.canSpawnEnemies = false;
        }

        protected void Start()
        {
            // Override config
            var config = GameController.CurrentLevelConfig;
            var scenes = SceneUtil.GetAllScenes().Select(scene => scene.name).ToList();
            if (config != null && scenes.Any(sceneName => sceneName == config.sceneName) &&
                config is TutorialLevelConfig tutorialLevelConfig)
            {
                levelConfig = tutorialLevelConfig;
            }

            if (headerText != null)
            {
                var loc = LocalizationManager.instance;
                headerText.text =
                    $"Level {GameController.instance.CurrentLevel + 1} ({loc.GetLocalizedText(TutorialHeaderKey)})";
            }

            RuneController.runeFilter = levelConfig.allowedRunes;
            BridgeController.VisibleSegmentCount = levelConfig.bridgeSegments;
            var index = levelConfig.bridgeSegments / 2;
            if (levelConfig.bridgeSegments % 2 == 0) index -= 1;
            BridgeController.HighlightedSegmentIndex = Player.CurrentBridgeIndex = Mathf.Clamp(index, 0, 4);

            StartCoroutine(Seq_StartTutorial());
        }

        private IEnumerator Seq_StartTutorial()
        {
            UIController.instance.FadeForegroundOverlay(false);

            BridgeController.bridgeEdgeLeft.baseSprite.sortingOrder = SpriteLayers.Player - 1;
            var segmentStartX = BridgeController.bridgeSegments[Player.CurrentBridgeIndex].transform.position.x;
            yield return Player.Seq_DashToLocation(new Vector3(segmentStartX, 0), 1f, true, 1.5f);
            BridgeController.bridgeEdgeLeft.baseSprite.sortingOrder = SpriteLayers.BridgeSeg;
            Player.CanReceiveMoveRequests = true;

            yield return script.Seq_StartScript();
        }
    }
}