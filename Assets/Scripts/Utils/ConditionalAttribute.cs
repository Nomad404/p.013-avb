using System;
using UnityEngine;

namespace AVB
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Enum |
                    AttributeTargets.Class | AttributeTargets.Struct)]
    public class ConditionalAttribute : PropertyAttribute
    {
        //The name of the bool field that will be in control
        public readonly string fieldName;
        public readonly bool defaultBool;
        public readonly int defaultInt;
        public readonly float defaultFloat;
        public readonly double defaultDouble;
        public readonly string defaultString;

        //TRUE = Hide in inspector / FALSE = Disable in inspector 
        public readonly bool hideInInspector;

        public ConditionalAttribute(string fieldName)
        {
            this.fieldName = fieldName;
            hideInInspector = false;
        }

        public ConditionalAttribute(string fieldName, bool hideInInspector)
        {
            this.fieldName = fieldName;
            this.hideInInspector = hideInInspector;
        }

        public ConditionalAttribute(string fieldName, bool hideInInspector, bool defaultBool)
        {
            this.fieldName = fieldName;
            this.hideInInspector = hideInInspector;
            this.defaultBool = defaultBool;
        }

        public ConditionalAttribute(string fieldName, bool hideInInspector, int defaultInt)
        {
            this.fieldName = fieldName;
            this.hideInInspector = hideInInspector;
            this.defaultInt = defaultInt;
        }

        public ConditionalAttribute(string fieldName, bool hideInInspector, float defaultFloat)
        {
            this.fieldName = fieldName;
            this.hideInInspector = hideInInspector;
            this.defaultFloat = defaultFloat;
        }

        public ConditionalAttribute(string fieldName, bool hideInInspector, double defaultDouble)
        {
            this.fieldName = fieldName;
            this.hideInInspector = hideInInspector;
            this.defaultDouble = defaultDouble;
        }

        public ConditionalAttribute(string fieldName, bool hideInInspector, string defaultString)
        {
            this.fieldName = fieldName;
            this.hideInInspector = hideInInspector;
            this.defaultString = defaultString;
        }
    }
}