using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class MultiSceneSetup : ScriptableObject
{
    public SceneSetup[] setups;
}

public static class MultiSceneSetupMenu
{
    [MenuItem("Assets/MultiSceneSetup/Create")]
    public static void CreateNewSceneSetup()
    {
        var folderPath = TryGetSelectedFolderPathInProjectsTab();
        var assetPath = ConvertFullAbsolutePathToAssetPath(
            Path.Combine(folderPath, "SceneSetup.asset"));
        SaveCurrentSceneSetup(assetPath);
    }

    [MenuItem("Assets/MultiSceneSetup/Create", true)]
    public static bool CreateNewSceneSetupValidate()
    {
        return TryGetSelectedFolderPathInProjectsTab() != null;
    }

    [MenuItem("Assets/MultiSceneSetup/Overwrite")]
    public static void SaveSceneSetup()
    {
        var assetPath = ConvertFullAbsolutePathToAssetPath(
            TryGetSelectedFilePathInProjectsTab());
        SaveCurrentSceneSetup(assetPath);
    }

    private static void SaveCurrentSceneSetup(string assetPath)
    {
        var loader = ScriptableObject.CreateInstance<MultiSceneSetup>();
        loader.setups = EditorSceneManager.GetSceneManagerSetup();

        AssetDatabase.CreateAsset(loader, assetPath);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        Debug.Log($"Scene setup '{Path.GetFileNameWithoutExtension(assetPath)}' saved");
    }

    [MenuItem("Assets/MultiSceneSetup/Load")]
    public static void RestoreSceneSetup()
    {
        var assetPath = ConvertFullAbsolutePathToAssetPath(
            TryGetSelectedFilePathInProjectsTab());
        var loader = AssetDatabase.LoadAssetAtPath<MultiSceneSetup>(assetPath);

        EditorSceneManager.RestoreSceneManagerSetup(loader.setups);

        Debug.Log($"Scene setup '{Path.GetFileNameWithoutExtension(assetPath)}' restored");
    }

    [MenuItem("Assets/Multi Scene Setup", true)]
    public static bool SceneSetupRootValidate()
    {
        return HasSceneSetupFileSelected();
    }

    [MenuItem("Assets/MultiSceneSetup/Overwrite", true)]
    public static bool SaveSceneSetupValidate()
    {
        return HasSceneSetupFileSelected();
    }

    [MenuItem("Assets/MultiSceneSetup/Load", true)]
    public static bool RestoreSceneSetupValidate()
    {
        return HasSceneSetupFileSelected();
    }

    private static bool HasSceneSetupFileSelected()
    {
        return TryGetSelectedFilePathInProjectsTab() != null;
    }

    private static List<string> GetSelectedFilePathsInProjectsTab()
    {
        return GetSelectedPathsInProjectsTab()
            .Where(x => File.Exists(x)).ToList();
    }

    private static string TryGetSelectedFilePathInProjectsTab()
    {
        var selectedPaths = GetSelectedFilePathsInProjectsTab();
        return selectedPaths.Count == 1 ? selectedPaths[0] : null;
    }

    // Returns the best guess directory in projects pane
    // Useful when adding to Assets -> Create context menu
    // Returns null if it can't find one
    // Note that the path is relative to the Assets folder for use in AssetDatabase.GenerateUniqueAssetPath etc.
    private static string TryGetSelectedFolderPathInProjectsTab()
    {
        var selectedPaths = GetSelectedFolderPathsInProjectsTab();
        return selectedPaths.Count == 1 ? selectedPaths[0] : null;
    }

    // Note that the path is relative to the Assets folder
    private static List<string> GetSelectedFolderPathsInProjectsTab()
    {
        return GetSelectedPathsInProjectsTab()
            .Where(x => Directory.Exists(x)).ToList();
    }

    private static IEnumerable<string> GetSelectedPathsInProjectsTab()
    {
        var selectedAssets = Selection.GetFiltered(
            typeof(Object), SelectionMode.Assets);

        return (from item in selectedAssets
            select AssetDatabase.GetAssetPath(item)
            into relativePath
            where !string.IsNullOrEmpty(relativePath)
            select Path.GetFullPath(Path.Combine(Application.dataPath, Path.Combine("..", relativePath)))).ToList();
    }

    private static string ConvertFullAbsolutePathToAssetPath(string fullPath)
    {
        return "Assets/" + Path.GetFullPath(fullPath)
            .Remove(0, Path.GetFullPath(Application.dataPath).Length + 1)
            .Replace("\\", "/");
    }
}