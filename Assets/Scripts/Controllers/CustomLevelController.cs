using UnityEngine;
using UnityEngine.SceneManagement;

namespace AVB
{
    public abstract class CustomLevelController<TLevelConfig> : MonoBehaviour where TLevelConfig : LevelConfig
    {
        protected static GameController GameController => GameController.instance;
        protected static BaseGameController BaseGameController => BaseGameController.instance;
        protected static BridgeController BridgeController => BridgeController.instance;
        protected static EnemyController EnemyController => EnemyController.instance;
        protected static RuneController RuneController => RuneController.instance;

        public TLevelConfig levelConfig;

        protected static Player Player => Player.instance;

        protected virtual void Awake()
        {
            var baseGameScene = SceneManager.GetSceneByName(LevelNames.BaseGame);
            if (!baseGameScene.IsValid())
            {
                SceneManager.LoadScene(LevelNames.BaseGame, LoadSceneMode.Additive);
            }
        }
    }
}