
circle_enemy.png
size: 2048,4096
format: RGBA8888
filter: Linear,Linear
repeat: none
armL
  rotate: false
  xy: 0, 140
  size: 619, 559
  orig: 619, 559
  offset: 0, 0
  index: -1
beard
  rotate: false
  xy: 1087, 794
  size: 817, 641
  orig: 817, 641
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 0, 699
  size: 1087, 979
  orig: 1087, 979
  offset: 0, 0
  index: -1
canvas
  rotate: false
  xy: 0, 2008
  size: 2048, 2048
  orig: 2048, 2048
  offset: 0, 0
  index: -1
eyes
  rotate: false
  xy: 0, 40
  size: 381, 100
  orig: 381, 100
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 619, 0
  size: 533, 350
  orig: 533, 350
  offset: 0, 0
  index: -1
face_water
  rotate: false
  xy: 1152, 0
  size: 533, 350
  orig: 533, 350
  offset: 0, 0
  index: -1
feetunderwater
  rotate: false
  xy: 1087, 350
  size: 690, 444
  orig: 690, 444
  offset: 0, 0
  index: -1
helmet
  rotate: false
  xy: 1540, 1435
  size: 433, 573
  orig: 433, 573
  offset: 0, 0
  index: -1
helmet_water
  rotate: false
  xy: 1107, 1435
  size: 433, 573
  orig: 433, 573
  offset: 0, 0
  index: -1
leg_left
  rotate: false
  xy: 1777, 540
  size: 177, 254
  orig: 177, 254
  offset: 0, 0
  index: -1
leg_right
  rotate: false
  xy: 1777, 288
  size: 164, 252
  orig: 164, 252
  offset: 0, 0
  index: -1
mustage
  rotate: false
  xy: 1685, 72
  size: 306, 216
  orig: 306, 216
  offset: 0, 0
  index: -1
water
  rotate: false
  xy: 0, 1678
  size: 1107, 330
  orig: 1107, 330
  offset: 0, 0
  index: -1

circle_enemy2.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
armR
  rotate: false
  xy: 1087, 1534
  size: 645, 514
  orig: 645, 514
  offset: 0, 0
  index: -1
arm_R
  rotate: false
  xy: 1087, 1020
  size: 645, 514
  orig: 645, 514
  offset: 0, 0
  index: -1
arm_left
  rotate: false
  xy: 0, 510
  size: 619, 559
  orig: 619, 559
  offset: 0, 0
  index: -1
horns
  rotate: false
  xy: 0, 0
  size: 631, 510
  orig: 631, 510
  offset: 0, 0
  index: -1
horns_water
  rotate: false
  xy: 619, 510
  size: 631, 510
  orig: 631, 510
  offset: 0, 0
  index: -1
torso_water
  rotate: false
  xy: 0, 1069
  size: 1087, 979
  orig: 1087, 979
  offset: 0, 0
  index: -1
