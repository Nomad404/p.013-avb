using System.Collections.Generic;
using System.Data;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace AVB
{
    public class LevelBoundaryController : MonoBehaviour
    {
        public static LevelBoundaryController instance;

        [SerializeField] private bool forceOnValidate;

        [HideInInspector] public ScreenBoundarySet screenBoundaries;
        public LevelBoundarySet levelBoundaries;

        private Camera mainCamera;

        [HideInInspector] public readonly List<Enemy> topEnemies = new();

        private void Awake()
        {
            if (!instance) instance = this;

            levelBoundaries.top.OnTriggerEnter2DAsObservable().Subscribe(col =>
            {
                var enemy = col.GetComponent<Enemy>();
                if (enemy != null) topEnemies.Add(enemy);
            }).AddTo(this);
            levelBoundaries.top.OnTriggerExit2DAsObservable().Subscribe(col =>
            {
                var enemy = col.GetComponent<Enemy>();
                if (topEnemies.Contains(enemy)) topEnemies.Remove(enemy);
            }).AddTo(this);

            SetupCamera();
            SetupScreenBoundaries();
            SetupLevelBoundaries();
        }

        private void OnValidate()
        {
            forceOnValidate = false;
            SetupCamera();
        }

        private void SetupCamera()
        {
            if (Camera.main == null) throw new NoNullAllowedException("Camera.main must not be null");
            mainCamera = Camera.main;
            // 1080p 0,5625
            // xs max 0,4620536
            // This is for setting the camera size based on the aspect ratio of the screen, so that slimmer screens can still see enough 
            mainCamera.orthographicSize = Utils.GetCameraSizeByRatio(mainCamera.aspect);
            // This is for moving the camera more up to compensate for a greater viewport size
            mainCamera.transform.position = new Vector3(0, Utils.GetCameraYPosByRatio(mainCamera.aspect), -10);
        }

        private void SetupScreenBoundaries()
        {
            screenBoundaries = new ScreenBoundarySet(
                mainCamera.ScreenToWorldPoint(new Vector2(0, Screen.height)),
                mainCamera.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height)),
                mainCamera.ScreenToWorldPoint(new Vector2(0, 0)),
                mainCamera.ScreenToWorldPoint(new Vector2(Screen.width, 0))
            );
        }

        private void SetupLevelBoundaries()
        {
            var cameraPos = mainCamera.transform.position;

            levelBoundaries.top.size =
                new Vector2((screenBoundaries.topLeft - screenBoundaries.topRight).magnitude * 1.1f, 1);
            levelBoundaries.top.transform.position = new Vector2(cameraPos.x,
                screenBoundaries.topLeft.y + levelBoundaries.top.size.y / 2);

            levelBoundaries.bottom.size =
                new Vector2((screenBoundaries.bottomLeft - screenBoundaries.bottomRight).magnitude * 1.1f, 1);
            levelBoundaries.bottom.transform.position = new Vector2(cameraPos.x,
                screenBoundaries.bottomLeft.y - levelBoundaries.bottom.size.y / 2);

            levelBoundaries.left.size = new Vector2(1,
                (screenBoundaries.topLeft - screenBoundaries.bottomLeft).magnitude * 1.1f);
            levelBoundaries.left.transform.position = new Vector2(
                screenBoundaries.topLeft.x - levelBoundaries.left.size.x / 2, cameraPos.y);

            levelBoundaries.right.size = new Vector2(1,
                (screenBoundaries.topRight - screenBoundaries.bottomRight).magnitude * 1.1f);
            levelBoundaries.right.transform.position = new Vector2(
                screenBoundaries.topRight.x + levelBoundaries.right.size.x / 2, cameraPos.y);
        }
    }
}