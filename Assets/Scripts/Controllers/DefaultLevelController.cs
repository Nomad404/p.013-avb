﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace AVB
{
    [RequireComponent(typeof(AudioSource))]
    public class DefaultLevelController : CustomLevelController<DefaultLevelConfig>
    {
        protected void Start()
        {
            // Override config
            var config = GameController.CurrentLevelConfig;
            var scenes = SceneUtil.GetAllScenes().Select(scene => scene.name).ToList();
            if (config != null && scenes.Any(sceneName => sceneName == config.sceneName) &&
                config is DefaultLevelConfig defaultLevelConfig)
            {
                levelConfig = defaultLevelConfig;
            }

            if (levelConfig != null)
            {
                RuneController.instance.runeFilter = levelConfig.allowedRunes;
                BridgeController.VisibleSegmentCount = levelConfig.bridgeSegments;
                var index = levelConfig.bridgeSegments / 2;
                if (levelConfig.bridgeSegments % 2 == 0) index -= 1;
                BridgeController.HighlightedSegmentIndex = Player.CurrentBridgeIndex = Mathf.Clamp(index, 0, 4);
            }
            else
            {
                BridgeController.VisibleSegmentCount = 3;
                BridgeController.HighlightedSegmentIndex = 1;
                Player.CurrentBridgeIndex = 1;
            }

            EnemyController.onAllEnemiesKilled.AddListener(OnAllEnemiesKilled);

            StartCoroutine(Seq_StartGame());
        }

        private void OnDestroy()
        {
            EnemyController.onAllEnemiesKilled.RemoveListener(OnAllEnemiesKilled);
        }

        private void OnAllEnemiesKilled()
        {
            var averageBridgeHealth =
                BridgeController.bridgeSegments.ToList().Where(seg => seg.Index >= 0).Select(seg => seg.Health).Sum() /
                100f / BridgeController.VisibleSegmentCount;
            StartCoroutine(BaseGameController.instance.Seq_FinishLevel((int) (averageBridgeHealth * 3)));
#if (UNITY_ANDROID || UNITY_IPHONE)
            var currentLevel = GameController.CurrentLevel + 1;
            var entry = GameController.gameConfig.endgameConfigs.FirstOrDefault(config =>
                currentLevel >= config.levelRange.x && currentLevel <= config.levelRange.y);
            var config = entry?.levelConfig;
            if (entry != null && currentLevel + 1 > entry.levelRange.y && config == levelConfig &&
                config.showAchievement)
            {
                if (GameServices.Instance.IsLoggedIn())
                {
                    GameServices.Instance.SubmitAchievement(levelConfig.achievementName);
                }
            }
#endif
        }

        private IEnumerator Seq_StartGame()
        {
            // audioSource.PlayOneShot(AudioResourceController.INSTANCE.levelNewWave);
            EnemyController.KillAllLivingEnemies();
            if (levelConfig != null)
            {
                switch (levelConfig.spawnMode)
                {
                    case EnemySpawnMode.Random:
                        EnemyController.SetupRandomEnemyPool(levelConfig);
                        break;
                    case EnemySpawnMode.SpawnList:
                        EnemyController.SetupEnemiesFromSpawnList(levelConfig);
                        break;
                }
            }
            else
            {
                EnemyController.SetupEnemyPoolByRandom();
            }

            yield return BaseGameController.Seq_StartNewLevel(levelConfig.headerText,
                levelConfig.subtitleText);

            EnemyController.SpawnEnemyFromQueue();
            EnemyController.spawnQueueTimer.Start();
        }
    }

    public static class DefaultLevelExtensions
    {
        public static void SetupRandomEnemyPool(this EnemyController controller, DefaultLevelConfig config)
        {
            var spawnSize = Random.Range(config.spawnSizeRange.x, config.spawnSizeRange.y);
            if (spawnSize == 0) Debug.LogWarning("Spawn size is 0, did you setup your config correctly?");
            controller.SetupRandomEnemyPool(spawnSize, config.allowedRunes, config.randomSpeedRange);
        }

        public static void SetupEnemiesFromSpawnList(this EnemyController controller,
            DefaultLevelConfig config)
        {
            controller.SetupEnemiesFromSpawnList(config.spawnOrder, config.spawnList);
        }
    }
}