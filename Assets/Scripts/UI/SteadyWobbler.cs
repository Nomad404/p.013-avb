using System;
using UnityEngine;

namespace AVB
{
    [RequireComponent(typeof(Transform))]
    public class SteadyWobbler : MonoBehaviour
    {
        [Header("Values X")] [SerializeField] private MoveMode moveModeX = MoveMode.Sin;
        [SerializeField] private float moveSpeedX = 1f;
        [SerializeField] private float moveAmountX = 1f;


        [Header("Values Y")] [SerializeField] private MoveMode moveModeY = MoveMode.Sin;
        [SerializeField] private float moveAmountY = 1f;
        [SerializeField] private float moveSpeedY = 1f;

        private Vector3 _startPosition;

        private void Awake()
        {
            _startPosition = transform.localPosition;
        }

        private void Update()
        {
            var moveX = GetMoveAmount(moveModeX, moveSpeedX, moveAmountX);
            var moveY = GetMoveAmount(moveModeY, moveSpeedY, moveAmountY);
            transform.localPosition = _startPosition + new Vector3(moveX, moveY);
        }

        private float GetMoveAmount(MoveMode mode, float speed, float amountMultiplier)
        {
            return mode switch
            {
                MoveMode.Sin => Mathf.Sin(Time.time * speed) * amountMultiplier,
                MoveMode.Cos => Mathf.Cos(Time.time * speed) * amountMultiplier,
                MoveMode.Tan => Mathf.Tan(Time.time * speed) * amountMultiplier,
                _ => throw new ArgumentOutOfRangeException(nameof(mode), mode, null)
            };
        }

        [Serializable]
        public enum MoveMode
        {
            Sin,
            Cos,
            Tan
        }
    }
}