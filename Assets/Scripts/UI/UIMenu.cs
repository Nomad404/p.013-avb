﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using UniRxExt = UniRx.ObservableExtensions;

namespace AVB
{
    public class UIMenu : MonoBehaviour
    {
        public static UIMenu instance;

        [SerializeField] private bool forceOnValidate;

        public Image background;

        [SerializeField] private Vector3 bannerButtonsPosition;

        [Header("Global buttons")] public GameObject btnContainer;
        public Button btnMenuInfo;
        public Button btnOpenMenu;
        public Button btnCloseMenu;

        private float _menuPosYHidden;
        [HideInInspector] public bool isMenuLocked;
        public readonly ReactiveProperty<State> rxMenuState = new(State.Closed);

        public State MenuState
        {
            get => rxMenuState.Value;
            private set => rxMenuState.SetValueAndForceNotify(value);
        }

        private RectTransform _rectTransform;
        private AudioSource _audioSource;

        private readonly CompositeDisposable _compDispMovement = new();

        public ReactiveProperty<Mode> rxMenuMode = new(Mode.Pause);

        public Mode MenuMode
        {
            get => rxMenuMode.Value;
            set => rxMenuMode.SetValueAndForceNotify(value);
        }

        public List<MenuStateObject> menus = new()
        {
            new MenuStateObject(Mode.Pause, null),
            new MenuStateObject(Mode.Info, null),
            new MenuStateObject(Mode.GameOver, null),
            new MenuStateObject(Mode.Score, null)
        };

        private void Awake()
        {
            if (!instance) instance = this;
            UniRxExt.Subscribe(rxMenuMode, OnNewMenuState).AddTo(this);
            if (menus.Select(r => r.mode).Distinct().Count() != Enum.GetValues(typeof(Mode)).Length - 1 ||
                menus.Any(r => r.menu == null))
            {
                throw new ArgumentException("menu objects must be set properly");
            }

            _rectTransform = GetComponent<RectTransform>();
            _audioSource = GetComponent<AudioSource>();
            _menuPosYHidden = _rectTransform.anchoredPosition.y;

            UpdateBannerButtonsPosition();

            var pauseMenu = menus.First(r => r.mode == Mode.Pause).menu;
            var skipButton = pauseMenu.GetComponentsInChildren<Button>().First(b => b.name == "SkipButton");
            skipButton.gameObject.SetActive(GameController.instance.CanSkipCurrentLevelLevel);
        }

        private void OnDestroy()
        {
            _compDispMovement.Dispose();
        }

        private void OnValidate()
        {
            forceOnValidate = false;
            OnNewMenuState(rxMenuMode.Value);
            UpdateBannerButtonsPosition();
        }

        private void OnNewMenuState(Mode newMode)
        {
            menus.ForEach(r =>
            {
                r.menu.SetActive(newMode == r.mode);
                if (newMode is Mode.Score or Mode.GameOver) isMenuLocked = true;
            });
        }

        private void UpdateBannerButtonsPosition()
        {
            var newPos = bannerButtonsPosition;
            if (Screen.safeArea.y > 0)
            {
                newPos.y -= Screen.safeArea.y;
            }

            btnContainer.transform.localPosition = newPos;
        }

        public void OpenPauseMenu()
        {
            if (btnCloseMenu.GetComponent<Image>().enabled) return;
            MenuMode = Mode.Pause;
            GameTime.IsPaused = true;
            OpenMenu();
        }

        public void OpenMenu()
        {
            if (MenuState == State.Open) return;
            MenuState = State.Open;
            SwitchMenuButton(false);
            _compDispMovement.Clear();
            var offsetY = -Screen.safeArea.y;
            if (Screen.height > 1920)
            {
                offsetY += ((float)Screen.width / Screen.height - 1080f / 1920f) * 1920f;
            }

            UniRxExt.Subscribe(
                Observable.WhenAll(
                    Observable.FromCoroutine(_ => Seq_MoveItemTo(_rectTransform, new Vector3(0, offsetY, 0))),
                    Observable.FromCoroutine(_ =>
                    {
                        var trans = btnContainer.GetComponent<RectTransform>();
                        var newPos = trans.localPosition;
                        newPos.y += Screen.safeArea.y;
                        return Seq_MoveItemTo(trans, newPos);
                    }))
            ).AddTo(_compDispMovement);
        }

        public void CloseMenu()
        {
            if (isMenuLocked) return;
            if (MenuState == State.Closed) return;
            MenuState = State.Closed;
            GameTime.IsPaused = false;
            SwitchMenuButton(true);
            _compDispMovement.Clear();
            UniRxExt.Subscribe(Observable.WhenAll(
                Observable.FromCoroutine(_ => Seq_MoveItemTo(_rectTransform, new Vector3(0, _menuPosYHidden, 0))),
                Observable.FromCoroutine(_ =>
                {
                    var trans = btnContainer.GetComponent<RectTransform>();
                    var newPos = trans.localPosition;
                    newPos.y -= Screen.safeArea.y;
                    return Seq_MoveItemTo(trans, newPos);
                }))).AddTo(_compDispMovement);
        }

        public void OnInfoButtonClicked()
        {
            if (rxMenuMode.Value is Mode.GameOver or Mode.Score) return;
            rxMenuMode.Value = Mode.Info;
            if (btnOpenMenu.GetComponent<Image>().enabled)
            {
                GameTime.IsPaused = true;
                OpenMenu();
            }
            else
            {
                CloseMenu();
            }
        }

        public void PlayButtonClickSound()
        {
            _audioSource.PlayOneShot(AudioResourceController.instance.menuButtonClick);
        }

        private void SwitchMenuButton(bool openVisible)
        {
            btnCloseMenu.GetComponentsInChildren<Image>().ToList().ForEach(image => image.enabled = !openVisible);
            btnOpenMenu.GetComponentsInChildren<Image>().ToList().ForEach(image => image.enabled = openVisible);
        }

        private static IEnumerator Seq_MoveItemTo(RectTransform trans, Vector3 destination)
        {
            while (Vector3.Distance(trans.anchoredPosition3D, destination) > 0.1f)
            {
                trans.anchoredPosition3D =
                    Vector3.Lerp(trans.anchoredPosition3D, destination, 6f * Time.unscaledDeltaTime);
                yield return new WaitForEndOfFrame();
            }

            trans.anchoredPosition3D = destination;
        }

        [Serializable]
        public enum Mode
        {
            Default = 0,
            Pause = 1,
            GameOver = 2,
            Info = 3,
            Score = 4
        }

        [Serializable]
        public enum State
        {
            Default = 0,
            Closed = 1,
            Open = 2
        }
    }

    [Serializable]
    public struct MenuStateObject
    {
        public UIMenu.Mode mode;
        public GameObject menu;

        public MenuStateObject(UIMenu.Mode mode, GameObject menu)
        {
            this.mode = mode;
            this.menu = menu;
        }
    }

    public class WaitForMenuMode : CustomYieldInstruction
    {
        private readonly UIMenu.Mode _targetMode;
        private UIMenu.Mode _currentMenuMode = UIMenu.Mode.Default;
        private readonly float _timeout;
        private readonly float _startTime;

        public override bool keepWaiting =>
            !(_targetMode == _currentMenuMode || Mathf.Abs(Time.time - _startTime) >= _timeout);

        public WaitForMenuMode(UIMenu.Mode mode, float timeout = 60f)
        {
            _targetMode = mode;
            _timeout = timeout;
            _startTime = Time.time;
            UniRxExt.Subscribe(UIMenu.instance.rxMenuMode.Skip(1), newMode => { _currentMenuMode = newMode; });
        }
    }

    public class WaitForMenuState : CustomYieldInstruction
    {
        private readonly UIMenu.State _targetState;
        private UIMenu.State _currentMenuState = UIMenu.State.Default;
        private readonly float _timeout;
        private readonly float _startTime;

        public override bool keepWaiting =>
            !(_currentMenuState == _targetState || Mathf.Abs(Time.time - _startTime) >= _timeout);

        public WaitForMenuState(UIMenu.State targetState, float timeout = 60f)
        {
            _targetState = targetState;
            _timeout = timeout;
            _startTime = Time.time;
            UniRxExt.Subscribe(UIMenu.instance.rxMenuState.Skip(1), newState => { _currentMenuState = newState; });
        }
    }
}