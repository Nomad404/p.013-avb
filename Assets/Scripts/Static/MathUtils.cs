﻿using UnityEngine;

namespace AVB
{
    public class MathUtils : MonoBehaviour
    {
        /// <summary>
        /// Gets the coordinates of the intersection point of two lines.
        /// </summary>
        /// <param name="a1">A point on the first line.</param>
        /// <param name="a2">Another point on the first line.</param>
        /// <param name="b1">A point on the second line.</param>
        /// <param name="b2">Another point on the second line.</param>
        /// <param name="found">Is set to false of there are no solution. true otherwise.</param>
        /// <param name="coords"></param>
        /// <returns>The intersection point coordinates. Returns Vector2.zero if there is no solution.</returns>
        public static bool GetIntersectionPointCoordinates(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2,
            out Vector2 coords)
        {
            var tmp = (b2.x - b1.x) * (a2.y - a1.y) - (b2.y - b1.y) * (a2.x - a1.x);

            if (tmp == 0)
            {
                // No solution!
                coords = Vector2.zero;
                return false;
            }

            var mu = ((a1.x - b1.x) * (a2.y - a1.y) - (a1.y - b1.y) * (a2.x - a1.x)) / tmp;

            coords = new Vector2(
                b1.x + (b2.x - b1.x) * mu,
                b1.y + (b2.y - b1.y) * mu
            );

            return true;
        }
    }
}