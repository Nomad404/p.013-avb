﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;
using UniRxExt = UniRx.ObservableExtensions;

namespace AVB
{
    public class EnemyController : MonoBehaviour
    {
        public static EnemyController instance;

        public Enemy enemyPrefab;
        public bool canSpawnEnemies = true;

        private const float EnemySpawnTickRate = 2f;
        private const float EnemySpawnAngleRange = 10.0f;
        private const float EnemySpawnOffsetRate = 0.3f;

        private const int EnemyCountInit = 8;

        private static BridgeController Bridge => BridgeController.instance;

        [FormerlySerializedAs("onEnemyWaveCleared")] [HideInInspector]
        public UnityEvent onAllEnemiesKilled;

        public static IEnumerable<Enemy> AllLivingEnemies =>
            FindObjectsOfType<Enemy>().Where(enemy => enemy.IsAlive).ToArray();

        public readonly Stopwatch spawnQueueTimer = new();
        public readonly Queue<EnemySpawnQueueParams> spawnQueue = new();
        private bool isSpawnQueueEnabled;

        public bool IsSpawnQueueEnabled
        {
            get => isSpawnQueueEnabled;
            set
            {
                isSpawnQueueEnabled = value;
                UpdateEnemyQueueTimer(isSpawnQueueEnabled);
            }
        }

        private List<Enemy>[] _allEnemies;
        private static Player Player => Player.instance;

        public List<Enemy> CurrentMatchingTargets =>
            GetAllEnemiesOnRowByRuneSet(Player.CurrentBridgeIndex, RuneController.instance.CurrentRuneSet)
                .Where(e => e.CanBeKilled).ToList();

        private void Awake()
        {
            if (!instance) instance = this;
        }

        private void Start()
        {
            UniRxExt.Subscribe(Bridge.rxBridgeSegmentCount, _ => SetupEnemyLists()).AddTo(this);
        }

        private void FixedUpdate()
        {
            if (GameTime.IsPaused) return;
            if (_allEnemies != null) UpdateEnemySortingOrder();
            if (spawnQueueTimer.IsRunning && spawnQueueTimer.Elapsed.Seconds >= EnemySpawnTickRate)
            {
                if (spawnQueue.Count > 0 && canSpawnEnemies)
                {
                    SpawnEnemyFromQueue();
                    spawnQueueTimer.Restart();
                }
            }
        }

        private void SetupEnemyLists()
        {
            _allEnemies = new List<Enemy>[Bridge.VisibleSegmentCount];
            for (var i = 0; i < _allEnemies.Length; i++)
            {
                _allEnemies[i] = new List<Enemy>();
            }
        }

        private void UpdateEnemyQueueTimer(bool isEnabled)
        {
            if (isEnabled)
            {
                if (spawnQueueTimer.IsRunning) return;
                spawnQueueTimer.Restart();
                return;
            }

            spawnQueueTimer.Reset();
        }

        public void SetupEnemyPoolByRandom()
        {
            for (var i = 0; i < EnemyCountInit; i++)
            {
                var runeSet = AttackRuneSet.Random;
                var row = Random.Range(0, Bridge.VisibleSegmentCount);
                var entry = new EnemySpawnQueueParams(runeSet, row);
                spawnQueue.Enqueue(entry);
            }
        }

        public void SetupRandomEnemyPool(int spawnSize, List<Rune> runeFilter, Vector2 speedRange)
        {
            if (runeFilter.Count == 0)
            {
                runeFilter = new List<Rune>
                {
                    Rune.Triangle, Rune.Square, Rune.Circle, Rune.NoColor, Rune.Red, Rune.Blue, Rune.Green, Rune.Yellow
                };
            }

            var lowRunes = runeFilter.Where(r => r.majorType == RuneType.Low).ToArray();
            var midRunes = runeFilter.Where(r => r.majorType == RuneType.Mid).ToArray();

            for (var i = 0; i < spawnSize; i++)
            {
                var runeSet = new AttackRuneSet();
                if (lowRunes.Length > 0)
                {
                    runeSet.SetRune(lowRunes[Random.Range(0, lowRunes.Length)]);
                }

                if (midRunes.Length > 0)
                {
                    runeSet.SetRune(midRunes[Random.Range(0, midRunes.Length)]);
                }

                var row = Random.Range(0, Bridge.VisibleSegmentCount);
                var entry = new EnemySpawnQueueParams(runeSet, row);

                if (speedRange.x > 0 && speedRange.y > 0 && speedRange.y > speedRange.x)
                {
                    entry.startSpeed = Random.Range(speedRange.x, speedRange.y);
                }

                spawnQueue.Enqueue(entry);
            }
        }

        public void SetupEnemiesFromSpawnList(EnemySpawnOrder order, IEnumerable<EnemySpawnData> spawnList)
        {
            var list = new List<EnemySpawnData>(spawnList);
            while (list.Count > 0)
            {
                var spawnData = order switch
                {
                    EnemySpawnOrder.Random => list[Random.Range(0, list.Count)],
                    EnemySpawnOrder.InOrder => list[0],
                    _ => throw new ArgumentOutOfRangeException(nameof(order), order, null)
                };
                var runeSet = new AttackRuneSet(spawnData.low, spawnData.mid);
                var row = Random.Range(0, Bridge.VisibleSegmentCount);
                var queueParams = new EnemySpawnQueueParams(runeSet, row, startSpeed: spawnData.speed);
                list.Remove(spawnData);
                spawnQueue.Enqueue(queueParams);
            }
        }

        public void SpawnEnemyFromQueue()
        {
            var entry = spawnQueue.ElementAt(0);
            var enemy = SpawnEnemyAtRow(entry.row, entry.runeSet, entry.yOffset);
            enemy.moveSpeed = entry.startSpeed;
            spawnQueue.Dequeue();
        }

        private void SpawnRandomEnemy()
        {
            if (canSpawnEnemies)
            {
                SpawnEnemyAtRandomRow();
            }
        }

        private Enemy SpawnEnemyAtRandomRow(AttackRuneSet runeSet = null, float yOffset = 0f)
        {
            var randomRowIndex = Random.Range(0, Bridge.VisibleSegmentCount);
            // int randomRowIndex = 0;
            return SpawnEnemyAtRow(randomRowIndex, runeSet, yOffset);
        }

        public Enemy SpawnEnemyAtRow(int index, AttackRuneSet runeSet = null, float yOffset = 0f)
        {
            var clampedIndex = Mathf.Clamp(index, 0, Bridge.VisibleSegmentCount);

            var bridgeSeg = Bridge.bridgeSegments[clampedIndex];
            var segmentWidth = bridgeSeg.SegmentWidth;
            var enemyShiftMax = segmentWidth * EnemySpawnOffsetRate;
            var position = bridgeSeg.transform.position;
            var xPos = position.x + Random.Range(-enemyShiftMax, enemyShiftMax);
            var enemy = SpawnEnemyAt(new Vector2(xPos,
                Utils.GetPointToScreenBorder(position, ScreenBorder.Top, 1.05f).y + yOffset), runeSet);
            _allEnemies[clampedIndex].Add(enemy);

            if (enemy == null) return null;
            enemy.SegmentIndex = clampedIndex;
            enemy.CanMove = true;
            enemy.moveTarget = bridgeSeg.gameObject;
            return enemy;
        }

        private Enemy SpawnEnemyAt(Vector2 position, AttackRuneSet runeSet = null)
        {
            var enemy = Instantiate(enemyPrefab, position, Quaternion.identity);
            enemy.RuneSet = runeSet ?? AttackRuneSet.Random;
            return enemy;
        }

        private void UpdateEnemySortingOrder()
        {
            for (var i = 0; i < _allEnemies.Length; i++)
            {
                if (_allEnemies[i].Count <= 0) continue;
                SortEnemiesByDistance(ref _allEnemies[i]);
                AdjustEnemiesSortingOrder(ref _allEnemies[i]);
            }
        }

        public Enemy GetClosestEnemyOnRow(int segmentIndex)
        {
            var enemiesInDirection = GetAllEnemiesOnRow(segmentIndex);

            if (enemiesInDirection.Count <= 0) return null;
            var result = enemiesInDirection[0];
            foreach (var t in enemiesInDirection.Where(t => t.DistanceToBridge <= result.DistanceToBridge))
            {
                result = t;
            }

            return result;
        }

        public List<Enemy> GetAllEnemiesOnRowByRuneSet(int rowIndex, RuneSet set)
        {
            var attackSet = AttackRuneSet.FromRuneSet(set);
            var allEnemiesOnRow = GetAllEnemiesOnRow(rowIndex);
            var matchingEnemies = allEnemiesOnRow
                .Where(enemy => (attackSet?.CompareTo(enemy.RuneSet) ?? AttackRuneSet.MatchType.NoMatch) !=
                                AttackRuneSet.MatchType.NoMatch).ToList();
            if (set == null || set.runes.Length != 3)
            {
                matchingEnemies = new List<Enemy>();
            }

            SortEnemiesByDistance(ref matchingEnemies);
            return matchingEnemies;
        }

        private static void SortEnemiesByDistance(ref List<Enemy> enemies)
        {
            for (var i = enemies.Count - 1; i > 0; i--)
            {
                for (var j = 0; j < i; j++)
                {
                    if (enemies[j] == null || enemies[j + 1] == null) continue;
                    if (enemies[j].DistanceToBridge > enemies[j + 1].DistanceToBridge)
                        ExchangeEnemies(ref enemies, j, j + 1);
                }
            }
        }

        private static void AdjustEnemiesSortingOrder(ref List<Enemy> enemies)
        {
            if (enemies.Count <= 1) return;
            for (var i = enemies.Count - 1; i >= 0; i--)
            {
                if (enemies[i] != null)
                {
                    var newBaseOrder = SpriteLayers.EnemyMAX - i * 4;
                    enemies[i].UpdateSortingOrder(newBaseOrder);
                    // enemies[i].baseSprite.sortingOrder = SpriteLayers.ENEMY_MAX - (i * 2);
                    // enemies[i].colorOverlaySprite.sortingOrder = enemies[i].baseSprite.sortingOrder + 1;
                }
            }
        }

        public List<Enemy> GetAllEnemiesOnRow(int rowIndex)
        {
            var allEnemies = FindObjectsOfType<Enemy>();
            return allEnemies.Where(e => e.SegmentIndex == rowIndex).ToList();
        }

        private Enemy GetClosestEnemy(IReadOnlyList<Enemy> list)
        {
            Enemy result = null;
            if (list.Count > 0) result = list[0].GetComponent<Enemy>();
            foreach (var e in list.Where(e => result is { } && e.DistanceToPlayer <= result.DistanceToPlayer))
            {
                result = e;
            }

            return result;
        }

        private static void ExchangeEnemies(ref List<Enemy> enemies, int i, int j)
        {
            (enemies[i], enemies[j]) = (enemies[j], enemies[i]);
        }

        public void OnEnemyKilled(Enemy enemy)
        {
            if (!AllLivingEnemies.Any() && spawnQueue.Count == 0)
            {
                onAllEnemiesKilled.Invoke();
            }
        }

        public static void DisableAllEnemies()
        {
            foreach (var e in FindObjectsOfType<Enemy>())
            {
                e.Disable();
            }
        }

        public void KillAllEnemies()
        {
            foreach (var e in FindObjectsOfType<Enemy>())
            {
                e.Kill();
            }
        }

        public static void KillAllLivingEnemies()
        {
            var enemies = FindObjectsOfType<Enemy>();
            if (enemies.Length <= 0) return;
            foreach (var e in enemies.Where(enemy => enemy.IsAlive))
            {
                e.Kill();
            }
        }
    }

    public enum EnemySpawnMode
    {
        Random = 0,
        SpawnList = 1
    }

    public enum EnemySpawnOrder
    {
        Random = 0,
        InOrder = 1
    }

    public struct EnemySpawnQueueParams
    {
        public AttackRuneSet runeSet;
        public int row;
        public float yOffset;
        public float startSpeed;

        public EnemySpawnQueueParams(AttackRuneSet runeSet, int row = 0, float yOffset = 0f,
            float startSpeed = Enemy.DefaultEnemySpeed)
        {
            this.row = row;
            this.runeSet = runeSet;
            this.yOffset = yOffset;
            this.startSpeed = startSpeed;
        }
    }
}