﻿using UnityEngine;

namespace AVB
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(Animator))]
    public class WaterWaveFX : MonoBehaviour
    {
        private const string ParamAnimOffset = "AnimOffset";

        private SpriteRenderer _spriteRenderer;
        private Animator _animator;
        private static readonly int AnimOffset = Animator.StringToHash(ParamAnimOffset);

        private void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _animator = GetComponent<Animator>();

            _spriteRenderer.sortingOrder = SpriteLayers.Waves;
            _animator.SetFloat(AnimOffset, Random.Range(0.0f, 1.0f));
        }
    }
}