﻿using System;
using System.Collections.Generic;
using System.Linq;
using Spine;
using Spine.Unity;
using UnityEngine;
using Event = Spine.Event;

namespace AVB
{
    public class EnemySpriteController : MonoBehaviour
    {
        public List<EnemySpritePart> parts = new(4);

        public int BaseSortingOrder => parts[0].skeletonAnimation.GetComponent<MeshRenderer>().sortingOrder;

        private Enemy _selfController;

        private void Awake()
        {
            _selfController = GetComponentInParent<Enemy>();
        }

        private void Start()
        {
            foreach (var entry in parts)
            {
                if (entry.partType != EnemyPartType.Body) continue;
                entry.skeletonAnimation.AnimationState.Event += OnAnimationEvent;
                entry.skeletonAnimation.AnimationState.Complete += OnAnimationEnd;
            }
        }

        public void UpdateResistanceColor(int subtype)
        {
            foreach (var entry in parts)
            {
                if (entry.partType != EnemyPartType.Body && entry.partType != EnemyPartType.Helmet) continue;
                var color = Utils.GetColorByMidRune(subtype);
                entry.skeletonAnimation.skeleton.SetColor(color);
            }
        }

        public void UpdateSortingOrder(int newBaseOrder)
        {
            for (var i = 0; i < parts.Count; i++)
            {
                var part = parts[i];
                part.skeletonAnimation.GetComponent<MeshRenderer>().sortingOrder = newBaseOrder + (int) part.partType;
                if (parts[i].partType == EnemyPartType.Body)
                {
                    parts[i].skeletonAnimation.GetComponentsInChildren<SpriteRenderer>().ToList().ForEach(sprite =>
                    {
                        sprite.sortingOrder = newBaseOrder + i - 1;
                    });
                }
            }
        }

        private EnemySpritePart GetBodyPart()
        {
            EnemySpritePart result = null;
            foreach (var entry in parts)
            {
                if (entry.partType == EnemyPartType.Body)
                {
                    result = entry;
                }
            }

            return result;
        }

        public void SetAnimState(CharacterState charState)
        {
            switch (charState)
            {
                case CharacterState.Idle:
                    SetAnimationForObject(0, AnimStages.Idle, true);
                    break;
                case CharacterState.Moving:
                    SetAnimationForObject(0, AnimStages.Move, true);
                    break;
                case CharacterState.Attacking:
                    SetAnimationForObject(0, AnimStages.Attack, true);
                    break;
                case CharacterState.Death:
                    SetAnimationForObject(0, AnimStages.Die, false);
                    break;
            }
        }

        private void SetAnimationForObject(int trackNumber, string animName, bool loop)
        {
            var entries = parts.Where(entry =>
                animName != entry.skeletonAnimation.AnimationState.GetCurrent(0).Animation.Name);
            foreach (var entry in entries)
            {
                entry.skeletonAnimation.AnimationState.SetAnimation(trackNumber, animName, loop);
            }
        }

        private void OnAnimationEvent(TrackEntry trackEntry, Event e)
        {
            switch (e.Data.Name)
            {
                case AnimationTags.EnemyEventAttackHit:
                    _selfController?.AttackTarget();
                    break;
            }
        }

        private void OnAnimationEnd(TrackEntry trackEntry)
        {
            switch (trackEntry.animation.name)
            {
                case AnimStages.Die:
                    Destroy(_selfController != null ? _selfController.gameObject : null);
                    break;
            }
        }

        public static class AnimStages
        {
            public const string Idle = "idle";
            public const string Move = "swim";
            public const string Attack = "attack";
            public const string Die = "die";
        }

        public static class AnimationTags
        {
            public const string EnemyEventAttackHit = "hit";
        }
    }
}

[Serializable]
public class EnemySpritePart
{
    public EnemyPartType partType;
    public SkeletonAnimation skeletonAnimation;
}

public enum EnemyPartType
{
    Water = 0,
    Selection = 1,
    Body = 2,
    Weapons = 3,
    Head = 4,
    Helmet = 5
}