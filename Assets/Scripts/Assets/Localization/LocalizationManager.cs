﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AVB
{
    public class LocalizationManager : MonoBehaviour
    {
        public static LocalizationManager instance;

        private const string LocaleEn = "config/translations/en.json";
        private const string LocaleDe = "config/translations/de.json";

        private Dictionary<string, string> localizedTexts;
        private const string MissingTextString = "Localized text not found";

        private void Awake()
        {
            if (!instance) instance = this;
            localizedTexts = new Dictionary<string, string>();
            var data = Application.systemLanguage switch
            {
                SystemLanguage.German => Utils.LoadJsonDataFromFile<LocalizationData>(LocaleDe),
                SystemLanguage.English => Utils.LoadJsonDataFromFile<LocalizationData>(LocaleEn),
                _ => Utils.LoadJsonDataFromFile<LocalizationData>(LocaleEn)
            };

            if (data == null) return;
            foreach (var item in data.items)
            {
                localizedTexts.Add(item.key, item.value);
            }
        }

        public string GetLocalizedText(string key)
        {
            var result = MissingTextString;
            if (localizedTexts.ContainsKey(key))
            {
                result = localizedTexts[key];
            }

            return result;
        }
    }

    [Serializable]
    public class LocalizationData
    {
        public LocalizationItem[] items;
    }

    [Serializable]
    public class LocalizationItem
    {
        public string key;
        public string value;
    }
}