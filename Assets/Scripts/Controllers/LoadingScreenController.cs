﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AVB
{
    public class LoadingScreenController : MonoBehaviour
    {
        public static LoadingScreenController instance;

        public DefaultMovingObject playerSprite;

        private void Awake()
        {
            if (!instance) instance = this;
        }

        public void LoadLevel(string levelName, bool includeBaseGame = true)
        {
            StartCoroutine(Seq_LoadLevel(levelName, includeBaseGame));
        }

        private IEnumerator Seq_LoadLevel(string levelName, bool includeBaseGame = true)
        {
            playerSprite.MoveToOverTime(Vector2.zero, 1f);
            yield return new WaitForSeconds(1.5f);

            var loadOperations = includeBaseGame
                ? new List<AsyncOperation>
                {
                    SceneManager.LoadSceneAsync(LevelNames.BaseGame, LoadSceneMode.Single),
                    SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive)
                }
                : new List<AsyncOperation>
                {
                    SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Single)
                };
            foreach (var loadOp in loadOperations)
            {
                loadOp.allowSceneActivation = false;
                while (loadOp.isDone)
                {
                    if (loadOp.progress >= 0.9f) break;
                    yield return null;
                }
            }

            playerSprite.MoveToOverTime(new Vector2(14, 0), 1f);
            yield return new WaitForSeconds(1.5f);

            loadOperations.ForEach(op => op.allowSceneActivation = true);
        }
    }
}