using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AVB.Tutorial
{
    public class Tutorial05 : TutorialScript
    {
        protected override int StepCount => 14;

        public override IEnumerator Seq_StartScript()
        {
            yield return base.Seq_StartScript();
            Player.CanAttack = false;
            yield return new WaitForSeconds(1f);

            // 1. Templates
            var enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Triangle, MidAttackRune.Red))
            };
            enemies.ForEach(e => e.CanBeKilled = false);
            yield return FadeElement(steps[0], 1f, 0.5f);
            yield return new WaitForSeconds(5f);
            yield return FadeElement(steps[0], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 2. Explanation
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CharacterState = CharacterState.Idle;
            });
            yield return FadeElement(steps[1], 1f, 0.5f);
            yield return new WaitForSeconds(5f);
            yield return FadeElement(steps[1], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 3. Shape first
            yield return FadeElement(steps[2], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Triangle);
            yield return FadeElement(steps[2], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 4. Now color
            yield return FadeElement(steps[3], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Red);
            yield return FadeElement(steps[3], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 5. Try to attack
            yield return FadeElement(steps[4], 1f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            yield return new WaitForRuneInput(Rune.Attack);
            yield return FadeElement(steps[4], 0f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = false);
            Player.CanAttack = false;

            yield return new WaitForSeconds(0.5f);

            // 6. Red doesn't work
            yield return FadeElement(steps[5], 1f, 0.5f);
            yield return new WaitForSeconds(5f);
            yield return FadeElement(steps[5], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 7. Triangle again
            yield return FadeElement(steps[6], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Triangle);
            yield return FadeElement(steps[6], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 8. Any other color
            yield return FadeElement(steps[7], 1f, 0.5f);
            yield return new WaitForRuneInput(new[] {Rune.Yellow, Rune.Blue, Rune.Green});
            yield return FadeElement(steps[7], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 9. Now attack
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            yield return FadeElement(steps[8], 1f, 0.5f);
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            Player.CanAttack = false;
            yield return FadeElement(steps[8], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 10. Notice combo order
            yield return FadeElement(steps[9], 1f, 0.5f);
            yield return new WaitForSeconds(5f);
            yield return FadeElement(steps[9], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 11. Try again shape
            enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Triangle, MidAttackRune.Yellow),
                    -10f)
            };
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CanBeKilled = false;
                e.CharacterState = CharacterState.Idle;
            });
            yield return FadeElement(steps[10], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Triangle);
            yield return FadeElement(steps[10], 0f, 0.5f);

            // 12. Any color again
            yield return FadeElement(steps[11], 1f, 0.5f);
            yield return new WaitForRuneInput(new[] {Rune.Red, Rune.Blue, Rune.Green});
            yield return FadeElement(steps[11], 0f, 0.5f);

            // 13. Attack again
            yield return FadeElement(steps[12], 1f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            yield return FadeElement(steps[12], 0f, 0.5f);

            // 14. Attack combo
            enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Circle, MidAttackRune.Green), -4f),
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Circle, MidAttackRune.Yellow), -6f),
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Circle, MidAttackRune.Red), -8f)
            };
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CanBeKilled = false;
                e.CharacterState = CharacterState.Idle;
            });
            yield return FadeElement(steps[13], 1f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = false;
            yield return new WaitForAttackSet(new AttackRuneSet(LowAttackRune.Circle, MidAttackRune.Blue));
            Player.CanAttack = true;
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            yield return FadeElement(steps[13], 0f, 0.5f);

#if (UNITY_ANDROID || UNITY_IPHONE)
            if (GameServices.Instance.IsLoggedIn())
            {
                GameServices.Instance.SubmitAchievement(AchievementNames.Readytogo);
            }
#endif

            yield return BaseGameController.instance.Seq_FinishLevel();
        }
    }
}