using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace AVB
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(FilePopupAttribute))]
    public class FilePopupDrawer : PropertyDrawer
    {
        private string[] _displayNames;
        private int _selectedIndex = -1;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.String)
            {
                Debug.LogError($"type: {property.propertyType} is not supported.");
                EditorGUI.LabelField(position, label.text, "Use FilePopup with string");
                return;
            }

            if (_displayNames == null)
            {
                var regex = (attribute as FilePopupAttribute)?.regex;
                InitDisplayNames(regex);
            }

            if (_selectedIndex < 0)
            {
                _selectedIndex = FindSelectedIndex(property.stringValue);
            }

            EditorGUI.BeginProperty(position, label, property);

            _selectedIndex = EditorGUI.Popup(position, label.text, _selectedIndex, _displayNames);
            property.stringValue = _displayNames[_selectedIndex];

            EditorGUI.EndProperty();
        }

        private void InitDisplayNames(string regex)
        {
            var fullPaths =
                Directory.GetFiles(Application.streamingAssetsPath, regex, SearchOption.AllDirectories);
            _displayNames = fullPaths.Select(fullPath =>
            {
                var path = fullPath.Replace(Application.streamingAssetsPath, "").Replace('\\', '/');
                if (path.StartsWith("/"))
                {
                    path = path.Substring(1);
                }

                return path;
            }).ToArray();
        }

        private int FindSelectedIndex(string value)
        {
            for (var i = 0; i < _displayNames.Length; i++)
            {
                if (_displayNames[i] == value)
                {
                    return i;
                }
            }

            return 0;
        }
    }
#endif

    public class FilePopupAttribute : PropertyAttribute
    {
        public readonly string regex;

        public FilePopupAttribute(string regex)
        {
            this.regex = regex;
        }
    }

    public class FileUtil : MonoBehaviour
    {
        public static byte[] LoadFile(string path)
        {
            if (!IsPathRooted(path))
            {
                path = Path.Combine(Application.streamingAssetsPath, path);
            }

            if (Application.platform != RuntimePlatform.Android)
            {
                path = $"file://{path}";
            }

            var request = UnityWebRequest.Get(path);
            request.SendWebRequest();
            while (!request.isDone)
            {
            }

            return request.downloadHandler.data;
        }

        private static bool IsPathRooted(string path)
        {
            return path.StartsWith("jar:file") || Path.IsPathRooted(path);
        }
    }
}