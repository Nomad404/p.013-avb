﻿namespace AVB
{
    public interface IKillable
    {
        public bool CanBeKilled { get; set; }
        void Kill();
    }

    public interface IDamageable : IKillable
    {
        void TakeDamage(float amount);
    }
}