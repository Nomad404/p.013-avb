using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AVB.Tutorial
{
    public class Tutorial04 : TutorialScript
    {
        protected override int StepCount => 5;

        public override IEnumerator Seq_StartScript()
        {
            yield return base.Seq_StartScript();
            Player.CanAttack = false;
            Player.CanReceiveMoveRequests = false;
            yield return new WaitForSeconds(1f);

            // 1. Intro
            var enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(1, new AttackRuneSet(LowAttackRune.Triangle)),
                EnemyController.SpawnEnemyAtRow(1, new AttackRuneSet(LowAttackRune.Circle), 2f)
            };
            enemies.ForEach(e => e.CanBeKilled = false);
            yield return FadeElement(steps[0], 1f, 0.5f);
            yield return new WaitForSeconds(5f);
            yield return FadeElement(steps[0], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 2. Change row
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CharacterState = CharacterState.Idle;
            });
            yield return FadeElement(steps[1], 1f, 0.5f);
            Bridge.bridgeSegments[1].IsGlowing = Player.CanReceiveMoveRequests = true;
            yield return new WaitForBridgeRowSwitch(1);
            Bridge.bridgeSegments[1].IsGlowing = Player.CanReceiveMoveRequests = false;
            yield return FadeElement(steps[1], 0f, 0.5f);

            // 3. Attack
            yield return FadeElement(steps[2], 1f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            Player.CanAttack = false;
            yield return FadeElement(steps[2], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 4. Change row back
            enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Circle), -8f),
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Square), -10f),
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Triangle), -12f)
            };
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CharacterState = CharacterState.Idle;
            });
            yield return FadeElement(steps[3], 1f, 0.5f);
            Bridge.bridgeSegments[0].IsGlowing = Player.CanReceiveMoveRequests = true;
            yield return new WaitForBridgeRowSwitch(0);
            Bridge.bridgeSegments[0].IsGlowing = Player.CanReceiveMoveRequests = false;
            yield return FadeElement(steps[3], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 5. Attack again
            yield return FadeElement(steps[4], 1f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            yield return FadeElement(steps[4], 0f, 0.5f);

            yield return BaseGame.Seq_FinishLevel();
        }
    }
}