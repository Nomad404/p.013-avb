using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AVB
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TextHyperlinkHandler : MonoBehaviour, IPointerClickHandler
    {
        private TextMeshProUGUI _textTMP;
        private Vector2 _currentMousePos;

        private void Awake()
        {
            _textTMP = GetComponent<TextMeshProUGUI>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            var linkIndex = TMP_TextUtilities.FindIntersectingLink(_textTMP, Input.mousePosition, null);
            if (linkIndex == -1) return;
            var linkInfo = _textTMP.textInfo.linkInfo[linkIndex];
            Application.OpenURL(linkInfo.GetLinkID());
        }
    }
}