﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UniRxExt = UniRx.ObservableExtensions;

namespace AVB
{
    public class BridgeController : MonoBehaviour
    {
        public static BridgeController instance;

        [InspectorName("BridgeSegmentCount")] [RangeReactiveProperty(1, 5)]
        public IntReactiveProperty rxBridgeSegmentCount = new(1);

        public int VisibleSegmentCount
        {
            get => rxBridgeSegmentCount.Value;
            set => rxBridgeSegmentCount.Value = Mathf.Clamp(value, 1, 5);
        }

        public BridgeEdge bridgeEdgeLeft;
        public BridgeEdge bridgeEdgeRight;
        public List<BridgeSegment> bridgeSegments = new();

        public int HighlightedSegmentIndex
        {
            // get { return BridgeSegments.ToList().FindIndex(seg => seg.IsHighlighted); }
            set
            {
                for (var i = 0; i < VisibleSegmentCount; i++)
                {
                    bridgeSegments[i].SwitchRowHighlight(i == value, true);
                }
            }
        }

        [HideInInspector] public UnityEvent onBridgeCreated;
        [HideInInspector] public UnityEvent<int> onBridgeDestroyed;

        private void Awake()
        {
            if (!instance) instance = this;
            UniRxExt.Subscribe(rxBridgeSegmentCount, _ => SetupBridge()).AddTo(this);
        }

        private void OnValidate()
        {
            SetupBridge();
        }

        private void SetupBridge()
        {
            if (bridgeSegments.Any(seg => seg == null) || bridgeEdgeLeft == null || bridgeEdgeRight == null)
                throw new ArgumentException("all bridge parts must not be null");
            var segmentWidth = bridgeSegments.First().GetComponent<BridgeSegment>().SegmentWidth;
            var outerSegmentWidth = bridgeEdgeLeft.GetComponent<BridgeEdge>().SegmentWidth;

            var segmentStartX = segmentWidth / 2 * (VisibleSegmentCount - 1);
            var borderStartX = segmentStartX + segmentWidth / 2 + outerSegmentWidth / 2;
            segmentStartX *= -1;

            for (var i = 0; i < bridgeSegments.Count; i++)
            {
                if (i < VisibleSegmentCount)
                {
                    bridgeSegments[i].transform.position = new Vector3(segmentStartX, 0f);
                    bridgeSegments[i].Index = i;
                    segmentStartX += segmentWidth;
                }
                else
                {
                    bridgeSegments[i].transform.position = new Vector3(-20, 0f);
                    bridgeSegments[i].Index = -1;
                }
            }

            bridgeEdgeRight.transform.position = new Vector2(borderStartX, 0);
            bridgeEdgeLeft.transform.position = new Vector2(-borderStartX, 0);

            bridgeSegments.ForEach(seg => seg.SwitchRowHighlight(false, true));

            onBridgeCreated.Invoke();
        }
    }
}