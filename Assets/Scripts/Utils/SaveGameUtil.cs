﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using JetBrains.Annotations;
using UnityEngine;

namespace AVB
{
    public static class SaveGameUtil
    {
        public const string DefaultSaveName = "player.save";

        public static string GetSaveGameFullPath(string saveName)
        {
            return $"{Application.persistentDataPath}/{saveName}";
        }

        public static bool HasSave()
        {
            return File.Exists(GetSaveGameFullPath(DefaultSaveName));
        }

        public static void WriteSaveGame(SaveGame saveGame, string saveName)
        {
            try
            {
                var fs = File.Open(GetSaveGameFullPath(saveName), FileMode.Create, FileAccess.Write);
                var bf = new BinaryFormatter();
                bf.Serialize(fs, saveGame);
                fs.Close();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        [CanBeNull]
        public static SaveGame ReadSaveGame(string saveName)
        {
            SaveGame save = null;
            if (File.Exists(GetSaveGameFullPath(saveName)))
            {
                try
                {
                    var fs = File.Open(GetSaveGameFullPath(saveName), FileMode.Open, FileAccess.Read);
                    var bf = new BinaryFormatter();
                    save = (SaveGame)bf.Deserialize(fs);
                    fs.Close();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
            else
            {
                Debug.LogWarning($"{saveName} not found");
            }

            return save;
        }
    }
}