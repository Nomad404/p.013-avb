﻿using System.Collections;
using UniRx;
using UnityEngine;

namespace AVB
{
    [RequireComponent(typeof(Rigidbody2D))]
    public abstract class MovingObject : MonoBehaviour
    {
        public LayerMask obstacleLayers;
        public float moveSpeed = 1f;
        [SerializeField] protected DirectionMethod directionMethod = DirectionMethod.Position;
        [SerializeField] protected MovementMethod movementMethod = MovementMethod.Force;
        protected bool ignoreObstacleTracing = false;
        protected float obstacleCheckDistance = 2f;
        private RaycastHit2D _currentObstacleTarget;

        private bool _isObstacleAhead = false;

        public bool IsObstacleAhead
        {
            get => _isObstacleAhead;
            set
            {
                var isFirstObstacleHit = value && _isObstacleAhead == false;
                _isObstacleAhead = value;
                if (isFirstObstacleHit) OnCantMove(_currentObstacleTarget);
            }
        }

        private bool _canMove = true;

        public bool CanMove
        {
            get => _canMove;
            set
            {
                _canMove = value;
                if (_canMove) return;
                rigid2D.velocity = Vector2.zero;
                _dispMove?.Dispose();
            }
        }

        private const float InstantMoveDistanceThreshold = 0.01f;

        protected Rigidbody2D rigid2D;

        public Vector2 CurrentPosition => new Vector2(transform.position.x, transform.position.y);

        [HideInInspector] public GameObject moveTarget;
        [HideInInspector] public Vector2 moveTargetPosition;
        [HideInInspector] public Vector2 movementDirectionNorm;

        [Header("Movement stuff")] public BoxCollider2D movementDeadZone;
        public LayerMask deadZoneLayer;
        [Header("Movement axis locks")] public bool moveLockedX = false;
        public bool moveLockedY = false;

        private float _angleToTarget;

        private readonly CompositeDisposable _dispMove = new();

        protected virtual void Awake()
        {
            rigid2D = GetComponent<Rigidbody2D>();
        }

        protected virtual void FixedUpdate()
        {
            if (GameTime.IsPaused) return;
            if (CheckIfObstacleAhead(movementDirectionNorm, out var hit))
            {
                _currentObstacleTarget = hit;
                IsObstacleAhead = true;
            }
            else
            {
                IsObstacleAhead = false;
            }

            var transform1 = transform;
            if (directionMethod == DirectionMethod.Reactive) return;
            if (moveTarget) moveTargetPosition = moveTarget.transform.position;
            else moveTargetPosition = transform.position + transform1.up;

            if (moveLockedX) moveTargetPosition = new Vector2(transform.position.x, moveTargetPosition.y);
            if (moveLockedY) moveTargetPosition = new Vector2(moveTargetPosition.x, transform.position.y);

            if (!CanMove || IsObstacleAhead) return;
            _angleToTarget =
                Vector2.Angle(directionMethod == DirectionMethod.Rotation ? -transform.up : transform1.up,
                    CurrentPosition - moveTargetPosition);
            _angleToTarget *=
                Vector2.Angle(directionMethod == DirectionMethod.Rotation ? -transform.right : transform1.right,
                    CurrentPosition - moveTargetPosition) > 90.0f
                    ? -1.0f
                    : 1.0f;
            switch (directionMethod)
            {
                case DirectionMethod.Position:
                    movementDirectionNorm = (moveTargetPosition - CurrentPosition).normalized;
                    break;
                case DirectionMethod.Rotation:
                    movementDirectionNorm = transform.up;
                    transform.Rotate(new Vector3(0, 0, -_angleToTarget));
                    break;
            }

            switch (movementMethod)
            {
                case MovementMethod.Force:
                    rigid2D.AddForce(movementDirectionNorm * moveSpeed);
                    break;
                case MovementMethod.Velocity:
                    rigid2D.velocity = movementDirectionNorm * moveSpeed;
                    break;
            }

            rigid2D.velocity = Vector2.ClampMagnitude(rigid2D.velocity, moveSpeed);

            OnObjectMove();
        }

        protected virtual void OnDestroy()
        {
            _dispMove.Dispose();
        }

        protected abstract void OnCantMove(RaycastHit2D hitResult);
        protected abstract void OnObjectMove();
        protected abstract void OnObjectStop();

        protected virtual bool CheckIfObstacleAhead(Vector2 normalizedDirection, out RaycastHit2D hit)
        {
            // First check long distance targets like bridges
            var start = CurrentPosition;
            var end = start + (normalizedDirection * obstacleCheckDistance);
            hit = Physics2D.Linecast(start, end, obstacleLayers);
            return hit.transform != null;

            // If no targets present, check immediate blocking clones in front
            // if(movementDeadZone != null){
            //     start += movementDeadZone.transform.position.ToVector2();
            //     end = start + (normalizedDirection * (movementDeadZone.size.y / 2) * 2f);
            //     hit = Physics2D.Linecast(start, end, deadZoneLayer);
            //     if(hit.transform != null){
            //         Debug.DrawLine(start, end, Color.red, 5.0f);
            //         return true;
            //     }else{
            //         Debug.DrawLine(start, end, Color.green, 3.0f);
            //     }
            // }

            // Otherwise, nothing is in front of target
        }

        public void MoveToOverTime(Vector3 destination, float timeFrame)
        {
            _dispMove?.Clear();
            Observable.FromCoroutine(_ => Seq_MoveToPositionOverTime(destination, timeFrame))
                .Subscribe(_ => { OnObjectStop(); }).AddTo(_dispMove);
        }

        private IEnumerator Seq_MoveToPositionOverTime(Vector3 destination, float duration)
        {
            var startPos = transform.position;
            var t = 0f;
            while (Vector3.Distance(transform.position, destination) > InstantMoveDistanceThreshold)
            {
                t += Time.fixedDeltaTime / duration;
                transform.position = Vector3.Lerp(startPos, destination, t);
                yield return new WaitForFixedUpdate();
            }
        }

        protected IEnumerator Seq_Shake(float duration, float magnitude)
        {
            var originalPosition = transform.position;
            var elapsed = 0f;

            while (elapsed < duration)
            {
                var newX = originalPosition.x + Random.Range(-1f, 1f) * magnitude;
                var newY = originalPosition.y + Random.Range(-1f, 1f) * magnitude;
                transform.position = new Vector3(newX, newY, 0);
                elapsed += Time.deltaTime;
                yield return null;
            }

            transform.position = originalPosition;
        }
    }

    public enum DirectionMethod
    {
        Position = 0,
        Rotation = 1,
        Reactive = 2
    }

    public enum MovementMethod
    {
        Force = 0,
        Velocity = 1
    }
}