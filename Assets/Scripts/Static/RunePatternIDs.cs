﻿namespace AVB
{
    public static class RunePatternIDs
    {
        public const string LineHorizontal = "rune_line_horizontal";
        public const string LineVertical = "rune_line_vertical";

        public const string LineDiagTopLeft = "rune_line_diag_top_left";
        public const string LineDiagTopRight = "rune_line_diag_top_right";
        public const string LineDiagDownLeft = "rune_line_diag_down_left";
        public const string LineDiagDownRight = "rune_line_diag_down_right";

        public const string Circle = "rune_circle";
        public const string Triangle = "rune_triangle";
        public const string Square = "rune_square";

        public const string CircleHalfDown = "rune_circle_half_down";
        public const string CircleHalfUp = "rune_circle_half_up";
        public const string CircleHalfRight = "rune_circle_half_right";
        public const string CircleHalfLeft = "rune_circle_half_left";

        public const string CircleQuartLeft = "rune_circle_quart_left";
        public const string CircleQuartRight = "rune_circle_quart_right";
        public const string CircleQuartUp = "rune_circle_quart_up";
        public const string CircleQuartDown = "rune_circle_quart_down";

        public const string CircleQuartDownLeft = "rune_circle_quart_down_left";
        public const string CircleQuartDownRight = "rune_circle_quart_down_right";
        public const string CircleQuartUpLeft = "rune_circle_quart_up_left";
        public const string CircleQuartUpRight = "rune_circle_quart_up_right";

        public const string SquareHalfDown = "rune_square_half_down";
        public const string SquareHalfUp = "rune_square_half_up";
        public const string SquareHalfLeft = "rune_square_half_left";
        public const string SquareHalfRight = "rune_square_half_right";

        public const string ArrowUp = "rune_arrow_up";
        public const string ArrowDown = "rune_arrow_down";
        public const string ArrowRight = "rune_arrow_right";
        public const string ArrowLeft = "rune_arrow_left";

        public const string ArrowDownRight = "rune_arrow_down_right";
        public const string ArrowDownLeft = "rune_arrow_down_left";
        public const string ArrowUpRight = "rune_arrow_up_right";
        public const string ArrowUpLeft = "rune_arrow_up_left";

        public const string AttackHighSword = "rune_attack_high_sword";
        public const string AttackHighAxe = "rune_attack_high_axe";

        public const string Cross = "rune_cross";
        public const string WaveHorizontal = "rune_wave_horizontal";
        public const string WaveVertical = "rune_wave_vertical";
        public const string Plus = "rune_plus";
        public const string ParallelHorizontal = "rune_parallel_horizontal";
        public const string ParallelVertical = "rune_parallel_vertical";

        public static string FromPatternIndex(int index)
        {
            return index switch
            {
                0 => Triangle,
                1 => Triangle,
                2 => Triangle,
                3 => Triangle,
                4 => Triangle,
                5 => Triangle,
                6 => Triangle,
                7 => Triangle,
                8 => Square,
                9 => SquareHalfLeft,
                10 => SquareHalfRight,
                11 => SquareHalfUp,
                12 => SquareHalfDown,
                13 => ArrowUpLeft,
                14 => ArrowUpRight,
                15 => ArrowDownLeft,
                16 => ArrowDownRight,
                17 => Circle,
                18 => CircleHalfLeft,
                19 => CircleHalfRight,
                20 => CircleHalfUp,
                21 => CircleHalfDown,
                22 => CircleQuartUpLeft,
                23 => CircleQuartUpRight,
                24 => CircleQuartDownLeft,
                25 => CircleQuartDownRight,
                26 => ArrowUp,
                27 => ArrowDown,
                28 => ArrowLeft,
                29 => ArrowRight,
                30 => LineVertical,
                31 => LineHorizontal,
                32 => LineDiagDownRight,
                33 => LineDiagDownLeft,
                34 => LineDiagTopRight,
                35 => LineDiagTopLeft,
                _ => ""
            };
        }
    }
}