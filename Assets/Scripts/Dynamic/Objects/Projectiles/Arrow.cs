﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace AVB
{
    public class Arrow : Projectile
    {
        private const float ArrowSpeed = 50.0f;

        private readonly ReactiveProperty<AttackRuneSet> _rxRunes =
            new(new AttackRuneSet());

        public AttackRuneSet RuneSet
        {
            get => _rxRunes.Value;
            set => _rxRunes.Value = value;
        }

        [HideInInspector] public ReactiveCollection<Enemy> rxAttackTargets = new(new List<Enemy>());

        private SpriteRenderer _baseSprite;
        [HideInInspector] public TrailRenderer trail;

        private static Player Player => Player.instance;
        private int comboCounter = 0;

        private new void Awake()
        {
            base.Awake();
            _baseSprite = GetComponentInChildren<SpriteRenderer>();
            trail = GetComponentInChildren<TrailRenderer>();

            _baseSprite.sortingOrder = SpriteLayers.ArrowDefault;
            trail.sortingOrder = SpriteLayers.ArrowTrail;
            trail.enabled = false;
        }

        private new void Start()
        {
            // TODO: Reenable this for death timer on arrow
            // base.Start();
            directionMethod = DirectionMethod.Rotation;
            moveSpeed = ArrowSpeed;
            if (moveTarget == null) CanMove = false;
            ignoreObstacleTracing = true;

            _rxRunes.Skip(1).Subscribe(set =>
            {
                if (set != null)
                {
                    SetArrowColor(Utils.GetColorByMidRune(set.MidRune.subType));
                }
            }).AddTo(this);

            rxAttackTargets.ObserveRemove()
                .Select(_ => rxAttackTargets.Where(e => e.IsAlive).ToArray())
                .Subscribe(targets =>
                {
                    if (targets.Length > 0)
                    {
                        SetArrowSortOrder(targets[0].TorsoSortingOrder);
                        moveTarget = targets[0].gameObject;
                    }
                    else
                    {
                        Destroy(gameObject);
                    }
                }).AddTo(this);

            this.OnTriggerEnter2DAsObservable()
                .Subscribe(col =>
                {
                    var enemy = col.gameObject.GetComponent<Enemy>();
                    if (!enemy || enemy.gameObject != moveTarget) return;

                    if (RuneSet.CompareTo(enemy.RuneSet) == AttackRuneSet.MatchType.PartialMatch)
                    {
                        enemy.BlockDamage();
                        Player.CurrentScore--;
                        Destroy(gameObject);
                    }
                    else
                    {
                        enemy.TakeDamage(100.0f);
                        Player.CurrentScore += 1 + comboCounter;
                        comboCounter++;
                    }

                    rxAttackTargets.Remove(enemy);
                }).AddTo(this);
        }

        private void UpdateArrowColor(int runeType)
        {
            switch (runeType)
            {
                case (int) MidAttackRune.Blue:
                    SetArrowColor(Color.cyan);
                    break;
                case (int) MidAttackRune.Green:
                    SetArrowColor(Color.green);
                    break;
                case (int) MidAttackRune.Red:
                    SetArrowColor(Color.red);
                    break;
                case (int) MidAttackRune.Yellow:
                    SetArrowColor(Color.yellow);
                    break;
            }
        }

        public void SetArrowSortOrder(int order)
        {
            _baseSprite.sortingOrder = order;
            trail.sortingOrder = order;
        }

        private void SetArrowColor(Color color)
        {
            // baseSprite.material.SetColor("_Color", color);
            trail.startColor = color;
            trail.endColor = new Color(color.r, color.g, color.b, 0.0f);
        }

        protected override bool CheckIfObstacleAhead(Vector2 normalizedDirection, out RaycastHit2D hit)
        {
            // First check long distance targets like bridges

            var start = CurrentPosition;
            var end = start + (normalizedDirection * obstacleCheckDistance);
            hit = Physics2D.Linecast(start, end, obstacleLayers);
            return hit.transform != null && moveTarget != null && hit.transform.gameObject == moveTarget;

            // Otherwise, nothing is in front of target
        }
    }
}