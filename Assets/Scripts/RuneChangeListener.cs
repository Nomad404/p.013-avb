﻿using UniRx;
using UnityEngine;

namespace AVB
{
    public abstract class RuneChangeListener : MonoBehaviour
    {
        protected virtual void Start()
        {
            RuneController.instance.rxLatestRune
                .Skip(1)
                .Subscribe(OnRuneChanged);
        }

        protected abstract void OnRuneChanged(Rune rune);
    }
}