
enemy_triangle_spineready.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_left
  rotate: false
  xy: 1024, 1368
  size: 437, 348
  orig: 437, 348
  offset: 0, 0
  index: -1
arm_left Kopie
  rotate: false
  xy: 1024, 1020
  size: 437, 348
  orig: 437, 348
  offset: 0, 0
  index: -1
arm_left_water
  rotate: false
  xy: 1568, 1700
  size: 437, 348
  orig: 437, 348
  offset: 0, 0
  index: -1
arm_right_water
  rotate: false
  xy: 544, 676
  size: 437, 348
  orig: 437, 348
  offset: 0, 0
  index: -1
beard
  rotate: true
  xy: 508, 160
  size: 516, 256
  orig: 516, 256
  offset: 0, 0
  index: -1
beard_water
  rotate: true
  xy: 764, 160
  size: 516, 256
  orig: 516, 256
  offset: 0, 0
  index: -1
canvas
  rotate: false
  xy: 0, 1024
  size: 1024, 1024
  orig: 1024, 1024
  offset: 0, 0
  index: -1
eyes
  rotate: true
  xy: 2005, 1858
  size: 190, 43
  orig: 190, 43
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 508, 9
  size: 351, 151
  orig: 351, 151
  offset: 0, 0
  index: -1
face_water
  rotate: true
  xy: 1207, 407
  size: 351, 151
  orig: 351, 151
  offset: 0, 0
  index: -1
feetunderwater
  rotate: true
  xy: 981, 676
  size: 344, 221
  orig: 344, 221
  offset: 0, 0
  index: -1
helmet
  rotate: false
  xy: 1202, 758
  size: 220, 262
  orig: 220, 262
  offset: 0, 0
  index: -1
helmet_water
  rotate: false
  xy: 1461, 1438
  size: 220, 262
  orig: 220, 262
  offset: 0, 0
  index: -1
horns
  rotate: false
  xy: 0, 0
  size: 508, 346
  orig: 508, 346
  offset: 0, 0
  index: -1
horns_water
  rotate: false
  xy: 0, 346
  size: 508, 346
  orig: 508, 346
  offset: 0, 0
  index: -1
leg_left
  rotate: true
  xy: 1020, 184
  size: 88, 126
  orig: 88, 126
  offset: 0, 0
  index: -1
leg_right
  rotate: false
  xy: 859, 35
  size: 81, 125
  orig: 81, 125
  offset: 0, 0
  index: -1
mustage
  rotate: false
  xy: 1461, 1316
  size: 207, 122
  orig: 207, 122
  offset: 0, 0
  index: -1
torso
  rotate: false
  xy: 0, 692
  size: 544, 332
  orig: 544, 332
  offset: 0, 0
  index: -1
torso_water
  rotate: false
  xy: 1024, 1716
  size: 544, 332
  orig: 544, 332
  offset: 0, 0
  index: -1
water
  rotate: true
  xy: 1020, 272
  size: 404, 187
  orig: 404, 187
  offset: 0, 0
  index: -1
