﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using AVB;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GestureRecognizer
{
    /// <summary>
    /// Captures player drawing and call the Recognizer to discover which gesture player id.
    /// Calls 'OnRecognize' event when something is recognized.
    /// </summary>
    public class DrawDetector : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
    {
        public Recognizer recognizer;

        public UILineRenderer lineTemplateObject;
        private List<UILineRenderer> linesRendered;

        [Range(0f, 1f)] public float scoreToAccept = 0.8f;

        [Range(1, 10)] public int minLines = 1;

        public int MinLines
        {
            set => minLines = Mathf.Clamp(value, 1, 10);
        }

        [Range(1, 10)] public int maxLines = 2;

        public int MaxLines
        {
            set => maxLines = Mathf.Clamp(value, 1, 10);
        }

        public enum RemoveStrategy
        {
            RemoveOld,
            ClearAll
        }

        public RemoveStrategy removeStrategy;

        public bool clearNotRecognizedLines;

        public bool fixedArea;

        private GestureData drawData = new();

        [Serializable]
        public class ResultEvent : UnityEvent<RecognitionResult>
        {
        }

        public ResultEvent OnRecognize;

        private RectTransform rectTransform;

        private const float LineFadeSpeed = 0.1f;

        private void Start()
        {
            lineTemplateObject.relativeSize = true;
            lineTemplateObject.LineList = false;
            linesRendered = new List<UILineRenderer> {lineTemplateObject};
            rectTransform = transform as RectTransform;
            UpdateLines();
        }

        private void OnValidate()
        {
            maxLines = Mathf.Max(minLines, maxLines);
        }

        public void UpdateLines()
        {
            while (linesRendered.Count < drawData.lines.Count)
            {
                var newLine = Instantiate(lineTemplateObject, lineTemplateObject.transform.parent);
                linesRendered.Add(newLine);
            }

            foreach (var t in linesRendered)
            {
                t.Points = new Vector2[] { };
                t.SetAllDirty();
            }

            var n = Mathf.Min(linesRendered.Count, drawData.lines.Count);
            for (var i = 0; i < n; i++)
            {
                linesRendered[i].Points = drawData.lines[i].points.Select(p => RealToLine(p)).ToArray();
                linesRendered[i].SetAllDirty();
            }
        }

        private Vector2 RealToLine(Vector2 position)
        {
            var local = rectTransform.InverseTransformPoint(position);
            var normalized = Rect.PointToNormalized(rectTransform.rect, local);
            return normalized;
        }

        private static Vector2 FixedPosition(Vector2 position)
        {
            return position;
            //var local = rectTransform.InverseTransformPoint(position);
            //var normalized = Rect.PointToNormalized(rectTransform.rect, local);
            //return normalized;
        }

        public void ClearAllLines()
        {
            drawData.lines.Clear();
            UpdateLines();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (drawData.lines.Count >= maxLines)
            {
                switch (removeStrategy)
                {
                    case RemoveStrategy.RemoveOld:
                        drawData.lines.RemoveAt(0);
                        break;
                    case RemoveStrategy.ClearAll:
                        drawData.lines.Clear();
                        break;
                }
            }

            drawData.lines.Add(new GestureLine());

            var fixedPos = FixedPosition(eventData.position);
            if (drawData.LastLine.points.Count != 0 && drawData.LastLine.points.Last() == fixedPos) return;
            drawData.LastLine.points.Add(fixedPos);
            drawData.LastLine.originalPoints.Add(fixedPos);
            UpdateLines();
        }

        public void OnDrag(PointerEventData eventData)
        {
            var fixedPos = FixedPosition(eventData.position);
            if (drawData.LastLine == null) return;
            if (drawData.LastLine.points.Count != 0 && drawData.LastLine.points.Last() == fixedPos) return;
            drawData.LastLine.points.Add(fixedPos);
            drawData.LastLine.originalPoints.Add(fixedPos);
            UpdateLines();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            StartCoroutine(OnEndDragCoroutine(eventData));
        }

        private IEnumerator OnEndDragCoroutine(PointerEventData eventData)
        {
            if (drawData.hasLines)
            {
                var fixedPos = FixedPosition(eventData.position);
                drawData.LastLine.points.Add(fixedPos);
                drawData.LastLine.originalPoints.Add(fixedPos);
            }

            UpdateLines();

            for (var size = drawData.lines.Count; size >= 1 && size >= minLines; size--)
            {
                //last[size] lines
                var sizedData = new GestureData
                {
                    lines = drawData.lines.GetRange(drawData.lines.Count - size, size)
                };

                var sizedNormalizedData = sizedData;

                if (fixedArea)
                {
                    var rect = this.rectTransform.rect;
                    sizedNormalizedData = new GestureData()
                    {
                        lines = sizedData.lines.Select(line => new GestureLine()
                        {
                            closedLine = line.closedLine,
                            points = line.points.Select(p =>
                                Rect.PointToNormalized(rect, this.rectTransform.InverseTransformPoint(p))).ToList()
                        }).ToList()
                    };
                }

                RecognitionResult result = null;

                //run in another thread 

                var thread = new Thread(() =>
                {
                    result = recognizer.Recognize(sizedNormalizedData, normalizeScale: !fixedArea);
                });
                thread.Start();
                while (thread.IsAlive)
                {
                    yield return null;
                }

                var fadeData = new GestureData
                {
                    renderedLines = linesRendered.Select(l => Instantiate(l, lineTemplateObject.transform.parent))
                        .ToList()
                };

                if (result.gesture != null && result.score.score >= scoreToAccept)
                {
                    OnRecognize.Invoke(result);
                    if (clearNotRecognizedLines)
                    {
                        drawData = sizedData;
                        StartLineFade(result.gesture.id, fadeData);
                        UpdateLines();
                        ClearAllLines();
                    }

                    break;
                }

                OnRecognize.Invoke(RecognitionResult.Empty);
                StartLineFade("", fadeData);
                ClearAllLines();
            }

            yield return null;
        }

        private void StartLineFade(string id, GestureData data)
        {
            foreach (var l in data.renderedLines)
            {
                var color = Utils.GetColorByGestureID(id);
                color.a = lineTemplateObject.color.a;
                l.color = color;
                StartCoroutine(OnRecognizePatternFade(l));
            }
        }

        private IEnumerator OnRecognizePatternFade(Graphic line)
        {
            yield return new WaitForSeconds(0.3f);
            line.color = lineTemplateObject.color;
            while (line.color.a > 0)
            {
                var target = line.color;
                target.a = 0;
                line.color = Color.Lerp(line.color, target, LineFadeSpeed);
                if (line.color.a < 0.01f) break;
                yield return null;
            }

            Destroy(line.gameObject);
        }
    }
}