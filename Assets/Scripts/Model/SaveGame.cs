﻿using System;
using System.Linq;

namespace AVB
{
    [Serializable]
    public class SaveGame
    {
        public int[] levelScores;
        public int currentLevel;
        public bool tutorialsFinished;

        public SaveGame(int[] levelScores, int currentLevel = 0, bool tutorialsFinished = false)
        {
            this.levelScores = levelScores;
            this.currentLevel = currentLevel;
            this.tutorialsFinished = tutorialsFinished;
        }

        public static SaveGame FromOther(SaveGame other)
        {
            return new SaveGame(other.levelScores, other.currentLevel, other.tutorialsFinished);
        }

        public override bool Equals(object obj)
        {
            if (obj is not SaveGame save) return false;

            return levelScores == save.levelScores && currentLevel == save.currentLevel &&
                   tutorialsFinished == save.tutorialsFinished;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(levelScores, currentLevel, tutorialsFinished);
        }

        public override string ToString()
        {
            return
                $"avg. score: {levelScores.Average()}, level: {currentLevel}, tutorials finished: {tutorialsFinished}";
        }
    }
}