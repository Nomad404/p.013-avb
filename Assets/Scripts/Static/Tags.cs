﻿namespace AVB
{
    public static class Tags
    {
        public const string Player = "Player";
        public const string GameController = "GameController";
        public const string EventSystem = "EventSystem";
        public const string BridgeSegment = "BridgeSegment";
        public const string Controllers = "Controllers";
        public const string Background = "Background";
    }
}