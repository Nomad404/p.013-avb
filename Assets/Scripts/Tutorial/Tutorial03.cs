using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AVB.Tutorial
{
    public class Tutorial03 : TutorialScript
    {
        protected override int StepCount => 7;

        public override IEnumerator Seq_StartScript()
        {
            yield return base.Seq_StartScript();
            Player.CanAttack = false;
            yield return new WaitForSeconds(1f);

            // 1. Templates
            var enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Circle)),
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Circle), 2f)
            };
            enemies.ForEach(e => e.CanBeKilled = false);
            yield return FadeElement(steps[0], 1f, 0.5f);
            yield return new WaitForSeconds(5f);
            yield return FadeElement(steps[0], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 2. Draw circle
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CharacterState = CharacterState.Idle;
            });
            yield return FadeElement(steps[1], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Circle);
            yield return FadeElement(steps[1], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 3. Attack
            yield return FadeElement(steps[2], 1f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            yield return new WaitForRuneInput(Rune.Attack);
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            Player.CanAttack = false;
            yield return FadeElement(steps[2], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 4. Draw arc
            enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Circle), -6f),
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Triangle), -8f),
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Circle), -10f),
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Square), -12f),
            };
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CanBeKilled = false;
                e.CharacterState = CharacterState.Idle;
            });
            yield return FadeElement(steps[3], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Circle);
            yield return FadeElement(steps[3], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 5. Attack again
            var circleEnemies = enemies.Where(e => e.RuneSet.LowRune == Rune.Circle).ToList();
            yield return FadeElement(steps[4], 1f, 0.5f);
            circleEnemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            yield return new WaitForRuneInput(Rune.Attack);
            while (circleEnemies.AnyAlive())
            {
                yield return null;
            }

            Player.CanAttack = false;
            yield return FadeElement(steps[4], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 6. Attack again
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            yield return FadeElement(steps[5], 1f, 0.5f);
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            yield return FadeElement(steps[5], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 7. Info menu
            yield return FadeElement(steps[6], 1f, 0.5f);
            yield return new WaitForMenuMode(UIMenu.Mode.Info);
            GameTime.IsPaused = false;
            yield return FadeElement(steps[6], 0f, 0.5f);
            yield return new WaitForMenuState(UIMenu.State.Closed, 3f);
            UIMenu.instance.CloseMenu();

#if (UNITY_ANDROID || UNITY_IPHONE)
            if (GameServices.Instance.IsLoggedIn())
            {
                GameServices.Instance.SubmitAchievement(AchievementNames.Thebasics);
            }
#endif

            yield return BaseGameController.instance.Seq_FinishLevel();
        }
    }
}