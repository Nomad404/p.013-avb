using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using UniRxExt = UniRx.ObservableExtensions;

namespace AVB
{
    public class ScoreSlider : MonoBehaviour
    {
        public List<Image> scoreImages;
        [RangeReactiveProperty(0, 3)] public IntReactiveProperty score = new(0);

        private void Awake()
        {
            UniRxExt.Subscribe(score, UpdateImages).AddTo(this);
        }

        private void OnValidate()
        {
            if (scoreImages.Any(image => image == null))
                throw new ArgumentException("No null score images allowed");
            UpdateImages(score.Value);
        }

        private void UpdateImages(int value)
        {
            scoreImages.Select((image, index) => new { image, index }).ToList()
                .ForEach(entry => entry.image.enabled = entry.index < value);
        }

        public IEnumerator Seq_SetScoreAnimated(int newScore, Action<int> onScoreSet = null)
        {
            StopAllCoroutines();
            if (newScore == 0)
            {
                UpdateImages(newScore);
                yield break;
            }

            onScoreSet?.Invoke(0);
            yield return Seq_PopupStar(scoreImages[0]);
            if (newScore <= 1) yield break;
            var multiplier = 1.1f;
            for (var i = 1; i < newScore; i++)
            {
                onScoreSet?.Invoke(i);
                yield return Seq_PopupStar(scoreImages[i], multiplier);
                multiplier += 0.1f;
            }
        }

        private static IEnumerator Seq_PopupStar(Behaviour star, float scaleMultiplier = 1f)
        {
            star.enabled = true;
            var starTrans = star.transform;
            var startScale = starTrans.localScale;
            var targetScale = startScale * 1.5f * scaleMultiplier;
            while (Vector3.Distance(starTrans.localScale, targetScale) > 0.01f)
            {
                starTrans.localScale = Vector3.Lerp(starTrans.localScale, targetScale, 0.5f);
                yield return new WaitForFixedUpdate();
            }

            starTrans.localScale = targetScale;
            while (Vector3.Distance(starTrans.localScale, startScale) > 0.01f)
            {
                starTrans.localScale = Vector3.Lerp(starTrans.localScale, startScale, 0.2f);
                yield return new WaitForFixedUpdate();
            }
        }
    }
}