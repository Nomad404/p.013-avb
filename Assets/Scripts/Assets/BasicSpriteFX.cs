﻿using UnityEngine;

public class BasicSpriteFX : MonoBehaviour
{
    public float delay = 0f;

    private void Start()
    {
        Destroy(gameObject, GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + delay);
    }
}