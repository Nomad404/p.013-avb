using System;
using UnityEngine;

namespace AVB.Tutorial
{
    [Serializable]
    [CreateAssetMenu(menuName = "AVB/TutorialLevelConfig")]
    public class TutorialLevelConfig : LevelConfig
    {
    }
}