using UnityEngine;
using UnityEngine.UI;

namespace AVB
{
    public class SketchInterpreter : MonoBehaviour
    {
        private const int SketchInputSize = 28;
        private const int SketchTypeNum = 34;

        [SerializeField, FilePopup("*.tflite")]
        private string fileName = "sketches.tflite";

        [SerializeField] private ComputeShader computeShader;

        // private PaintCanvas _paintCanvas;
        private bool _isProcessing;
        private readonly float[,] _inputData = new float[SketchInputSize, SketchInputSize];
        private readonly float[] _outputData = new float[SketchTypeNum];

        private int _computeKernelIndex;
        private ComputeBuffer _inputBuffer;
        private RenderTexture _inputRenderTex;

        // private Interpreter _interpreter;

        [SerializeField] private RawImage inputImage;

        private void Awake()
        {
            // _paintCanvas = FindObjectOfType<PaintCanvas>();
            // _paintCanvas.OnDrawTexture.AddListener(OnDrawTexture);
            // _paintCanvas.OnDrawEvent.AddListener(OnDrawEvent);
        }

        private void Start()
        {
            _inputRenderTex = new RenderTexture(28, 28, 0, RenderTextureFormat.ARGB32);
            inputImage.texture = _inputRenderTex;

            // var options = new InterpreterOptions()
            // {
            //     threads = 2,
            //     useNNAPI = true,
            // };
            // _interpreter = new Interpreter(FileUtil.LoadFile(fileName), options);
            // _interpreter.AllocateTensors();

            _computeKernelIndex = computeShader.FindKernel("TextureToBuffer");
            _inputBuffer = new ComputeBuffer(SketchInputSize * SketchInputSize, sizeof(float));
        }

        private void OnDestroy()
        {
            // _paintCanvas.OnDrawTexture.RemoveListener(OnDrawTexture);
            // _paintCanvas.OnDrawEvent.RemoveListener(OnDrawEvent);
            // _interpreter?.Dispose();
            _inputBuffer?.Dispose();
        }

        private void OnDrawTexture(RenderTexture texture)
        {
            ResizeAndCropInputTexture(texture);
            if (!_isProcessing)
            {
                Invoke(_inputRenderTex);
            }
        }

        // private void OnDrawEvent(PaintCanvas.DrawEventType drawEventType)
        // {
        //     if (drawEventType != PaintCanvas.DrawEventType.Stop) return;
        //     StartCoroutine(ClassifyRune());
        // }

        // private IEnumerator ClassifyRune()
        // {
        //     var (runeProb, runeIndex) = _outputData.Select((n, i) => (n, i)).Max();
        //     var canvasCopy = Instantiate(_paintCanvas.gameObject, _paintCanvas.transform.parent);
        //     canvasCopy.transform.SetSiblingIndex(_paintCanvas.transform.GetSiblingIndex());
        //     Destroy(canvasCopy.GetComponent<PaintCanvas>());
        //     var copyImage = canvasCopy.GetComponent<RawImage>();
        //     copyImage.texture =
        //         ToTexture2D((RenderTexture)_paintCanvas.GetComponent<RawImage>().texture);
        //     copyImage.color = Color.black;
        //     _paintCanvas.ResetCanvas();
        //     if (runeProb > 0.9f)
        //     {
        //         if (_paintCanvas.CurrentScaledCanvasPos.y > _paintCanvas.LastScaledCanvasPos.y)
        //         {
        //             runeIndex = runeIndex switch
        //             {
        //                 32 => 35,
        //                 33 => 34,
        //                 _ => runeIndex
        //             };
        //         }
        //
        //         InputHandler.OnRuneIndex(runeIndex);
        //     }
        //
        //     yield return new WaitForSeconds(1.5f);
        //     Destroy(canvasCopy.gameObject);
        //     yield return null;
        // }

        private void ResizeAndCropInputTexture(RenderTexture original)
        {
            // var tex = ToTexture2D(original);
            // var bounds = _paintCanvas.CurrentBounds;
            // var cropWidth = Mathf.Clamp(bounds.xDiff, 0, tex.width);
            // var cropHeight = Mathf.Clamp(bounds.yDiff, 0, tex.height);
            // var pixels = tex.GetPixels(
            //     (int)Mathf.Clamp(bounds.xMin, 0, tex.width),
            //     (int)Mathf.Clamp(tex.height - bounds.yMax, 0, tex.height),
            //     (int)cropWidth, (int)cropHeight
            // );
            // var croppedTex = new Texture2D((int)bounds.xDiff, (int)bounds.yDiff, TextureFormat.ARGB32, false);
            // croppedTex.SetPixels(pixels);
            // croppedTex.Apply();
            // RenderTexture.active = _inputRenderTex;
            // Graphics.Blit(croppedTex, _inputRenderTex);
            // RenderTexture.active = null;
        }

        private static Texture2D ToTexture2D(RenderTexture renderTex)
        {
            var tex = new Texture2D(renderTex.width, renderTex.height, TextureFormat.ARGB32, false);
            RenderTexture.active = renderTex;
            tex.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
            tex.Apply();
            RenderTexture.active = null;
            return tex;
        }

        private void Invoke(RenderTexture inputTexture)
        {
            _isProcessing = true;

            computeShader.SetTexture(_computeKernelIndex, "InputTexture", inputTexture);
            computeShader.SetBuffer(_computeKernelIndex, "OutputBuffer", _inputBuffer);
            computeShader.SetInt("ImageSize", SketchInputSize);
            computeShader.Dispatch(_computeKernelIndex, SketchInputSize / 4, SketchInputSize / 4, 1);
            _inputBuffer.GetData(_inputData);

            // _interpreter.SetInputTensorData(0, _inputData);
            // _interpreter.Invoke();
            // _interpreter.GetOutputTensorData(0, _outputData);

            _isProcessing = false;
        }
    }
}