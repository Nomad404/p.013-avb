﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace AVB
{
    public class LocalizedText : MonoBehaviour
    {
        public string key;

        private void Start()
        {
            var text = GetComponent<Text>();
            if (text != null)
            {
                text.text = GetLocalizedText(key);
            }

            var textTMP = GetComponent<TextMeshProUGUI>();
            if (textTMP != null)
            {
                textTMP.text = GetLocalizedText(key);
            }
        }

        private static string GetLocalizedText(string key)
        {
            return LocalizationManager.instance
                ? LocalizationManager.instance.GetLocalizedText(key)
                : "Loc-Manager not found";
        }
    }
}