﻿using System.Linq;
using AVB.Tutorial;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Localization.Settings;
using UnityEngine.Localization.SmartFormat.Extensions;
using UnityEngine.Localization.SmartFormat.PersistentVariables;
using UnityEngine.SceneManagement;

namespace AVB
{
    public class GameController : MonoBehaviour
    {
        public static GameController instance;

        public GameConfig gameConfig;

        private int _currentLevel;

        [HideInInspector] public SaveGame currentSaveGame;

        public int CurrentLevel
        {
            get => _currentLevel;
            set
            {
                _currentLevel = value;
                var source = LocalizationSettings.StringDatabase.SmartFormatter
                    .GetSourceExtension<PersistentVariablesSource>();
                if (source["global"]["currentLevel"] is IntVariable level) level.Value = _currentLevel + 1;
                currentSaveGame.currentLevel = _currentLevel;
                if (_currentLevel >= gameConfig.presetLevels.Length) currentSaveGame.tutorialsFinished = true;
            }
        }

        [CanBeNull]
        public LevelConfig CurrentLevelConfig
        {
            get
            {
                return CurrentLevel switch
                {
                    _ when CurrentLevel < gameConfig.presetLevels.Length => gameConfig.presetLevels[CurrentLevel]
                        .overrideConfig,
                    _ when CurrentLevel >= gameConfig.presetLevels.Length && gameConfig.endgameConfigs.Count > 0 =>
                        gameConfig.endgameConfigs.First(config =>
                                CurrentLevel + 1 >= config.levelRange.x && CurrentLevel + 1 <= config.levelRange.y)
                            .levelConfig,
                    _ => null
                };
            }
        }

        public bool CanSkipCurrentLevelLevel =>
            gameConfig.allowToSkipPresetLevels ||
            CurrentLevelConfig is TutorialLevelConfig && currentSaveGame.tutorialsFinished;

        private void Awake()
        {
            if (!instance)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
                SceneManager.sceneLoaded += OnSceneLoaded;
                currentSaveGame = SaveGameUtil.ReadSaveGame(SaveGameUtil.DefaultSaveName) ??
                                  new SaveGame(new int[gameConfig.totalLevels]);
                CurrentLevel = currentSaveGame.currentLevel;
                UpdateSaveGame();
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            if (instance == this)
            {
                SceneManager.sceneLoaded -= OnSceneLoaded;
                UpdateSaveGame();
            }
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus) UpdateSaveGame();
        }

        private void OnApplicationQuit()
        {
            GameTime.IsPaused = false;
            UpdateSaveGame();
        }

        public void UpdateSaveGame()
        {
            if (currentSaveGame == null) return;
            var oldSave = SaveGameUtil.ReadSaveGame(SaveGameUtil.DefaultSaveName);
            if (oldSave != null && currentSaveGame.Equals(oldSave)) return;
            SaveGameUtil.WriteSaveGame(currentSaveGame, SaveGameUtil.DefaultSaveName);
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            switch (scene.name)
            {
                case LevelNames.StartMenu:
                    if (CurrentLevel >= gameConfig.totalLevels && StartMenuController.instance != null)
                    {
                        StartMenuController.instance.UpdateMenuMode(StartMenuController.MenuMode.ThankYou);
                    }

                    break;
                case LevelNames.BaseGame: break;
                case LevelNames.LoadingScreen:
                    if (gameConfig.presetLevels.Length == 0 && gameConfig.endgameConfigs.Count == 0)
                    {
                        LoadingScreenController.instance.LoadLevel(LevelNames.DefaultLevel);
                        return;
                    }

                    switch (CurrentLevel)
                    {
                        case var _ when CurrentLevel >= gameConfig.totalLevels:
                            // Load thank you screen
                            LoadingScreenController.instance.LoadLevel(LevelNames.StartMenu, false);
                            break;
                        case var _ when CurrentLevel < gameConfig.presetLevels.Length:
                            var entry = gameConfig.presetLevels[CurrentLevel];
                            if (entry.enabled && !gameConfig.skipPresetLevels)
                            {
                                // Load next level
                                LoadingScreenController.instance.LoadLevel(entry.sceneName);
                            }
                            else
                            {
                                // Skip level and load the next one
                                CurrentLevel++;
                                LoadLevel(LevelNames.LoadingScreen);
                            }

                            break;
                        case var _ when CurrentLevel >= gameConfig.presetLevels.Length:
                            // Return to main menu if no more levels in list
                            if (gameConfig.continueAfterList)
                            {
                                LoadingScreenController.instance.LoadLevel(LevelNames.DefaultLevel);
                            }
                            else
                            {
                                LoadLevel(LevelNames.StartMenu);
                            }

                            break;
                    }

                    break;
            }
        }

        public static void LoadLevel(string levelName)
        {
            SceneManager.LoadScene(levelName);
        }

        public static void QuitGame()
        {
            Application.Quit();
        }
    }

    public static class LevelNames
    {
        public const string StartMenu = "StartMenu";
        public const string LoadingScreen = "LoadingScreen";
        public const string BaseGame = "BaseGame";
        public const string DefaultLevel = "DefaultLevel";
    }

    public static class GameTime
    {
        private static bool _isPaused;

        public static bool IsPaused
        {
            get => _isPaused;
            set
            {
                _isPaused = value;
                Time.timeScale = _isPaused ? 0f : 1f;
            }
        }
    }
}