using System.Collections;
using GestureRecognizer;
using Painter2D;
using UniRx;
using UnityEngine;

namespace AVB
{
    [RequireComponent(typeof(PaintCanvas))]
    public class PainterCanvasFader : MonoBehaviour
    {
        public static PainterCanvasFader instance;
        private const float CanvasFadeSpeed = 0.1f;

        private PaintCanvas _paintCanvas;

        private void Awake()
        {
            if (!instance) instance = this;
            _paintCanvas = GetComponent<PaintCanvas>();
        }

        private void Start()
        {
            RuneController.instance.rxLatestRune.Skip(1).Subscribe(OnNewRune).AddTo(this);
        }

        public void OnGesturePattern(RecognitionResult result)
        {
            if (result.gesture == null) FadeoutPainting(Color.white);
        }

        private void OnNewRune(Rune rune)
        {
            switch (rune.majorType)
            {
                case RuneType.General:
                case RuneType.Low:
                    FadeoutPainting(Color.black);
                    break;
                case RuneType.Mid:
                    FadeoutPainting(Utils.GetColorByMidRune(rune.subType));
                    break;
                default:
                    FadeoutPainting(Color.white);
                    break;
            }
        }

        public void FadeoutPainting(Color targetColor)
        {
            StartCoroutine(Seq_FadeoutCanvas(targetColor));
        }

        private IEnumerator Seq_FadeoutCanvas(Color targetColor)
        {
            var duplicate = _paintCanvas.DuplicateCanvas();
            _paintCanvas.ResetCanvas();
            duplicate.color = targetColor;
            yield return new WaitForSeconds(0.3f);
            while (duplicate.color.a > 0.01f)
            {
                var color = targetColor;
                color.a = 0;
                duplicate.color = Color.Lerp(duplicate.color, color, CanvasFadeSpeed);
                yield return new WaitForFixedUpdate();
            }

            duplicate.color = Color.clear;
            Destroy(duplicate.gameObject);
        }
    }
}