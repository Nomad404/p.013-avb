﻿using GestureRecognizer;
using UnityEngine;

namespace AVB
{
    public class InputHandler : MonoBehaviour
    {
        public static InputHandler instance;

        public PlayerBridgeMoveType playerBridgeMoveType = PlayerBridgeMoveType.Tap;

        private void Awake()
        {
            if (!instance) instance = this;
        }

        public static void OnRuneIndex(int index)
        {
            var runeID = RunePatternIDs.FromPatternIndex(index);
            EnqueueRuneByID(runeID);
        }

        public void OnGesturePattern(RecognitionResult result)
        {
            if (result.gesture == null) return;
            EnqueueRuneByID(result.gesture.id);
        }

        private static void EnqueueRuneByID(string id)
        {
            switch (id)
            {
                case RunePatternIDs.LineHorizontal:
                    RuneController.instance.EnqueueRune(Rune.Clear);
                    // if(playerBridgeMoveType == PlayerBridgeMoveType.Gesture){
                    //     GestureLine line = result.gesture.gesture.lines[0];
                    //     RuneController.INSTANCE.EnqueueRune(new Rune(RuneType.General, (int)GeneralRune.Move, new MoveRuneParams(line.originalPoints[0], line.originalPoints[line.originalPoints.Count - 1])));
                    // }
                    break;
                case RunePatternIDs.LineVertical:
                    RuneController.instance.EnqueueRune(Rune.Attack);
                    break;
                case RunePatternIDs.CircleHalfDown:
                case RunePatternIDs.CircleHalfUp:
                case RunePatternIDs.CircleHalfLeft:
                case RunePatternIDs.CircleHalfRight:
                case RunePatternIDs.CircleQuartLeft:
                case RunePatternIDs.CircleQuartRight:
                case RunePatternIDs.CircleQuartUp:
                case RunePatternIDs.CircleQuartDown:
                case RunePatternIDs.CircleQuartDownLeft:
                case RunePatternIDs.CircleQuartDownRight:
                case RunePatternIDs.CircleQuartUpLeft:
                case RunePatternIDs.CircleQuartUpRight:
                case RunePatternIDs.Circle:
                    RuneController.instance.EnqueueRune(Rune.Circle);
                    break;
                case RunePatternIDs.ArrowUp:
                case RunePatternIDs.ArrowDown:
                case RunePatternIDs.ArrowLeft:
                case RunePatternIDs.ArrowRight:
                case RunePatternIDs.Triangle:
                    RuneController.instance.EnqueueRune(Rune.Triangle);
                    break;
                case RunePatternIDs.SquareHalfDown:
                case RunePatternIDs.SquareHalfUp:
                case RunePatternIDs.SquareHalfLeft:
                case RunePatternIDs.SquareHalfRight:
                case RunePatternIDs.ArrowDownLeft:
                case RunePatternIDs.ArrowDownRight:
                case RunePatternIDs.ArrowUpLeft:
                case RunePatternIDs.ArrowUpRight:
                case RunePatternIDs.Square:
                    RuneController.instance.EnqueueRune(Rune.Square);
                    break;
                case RunePatternIDs.LineDiagDownRight:
                    RuneController.instance.EnqueueRune(Rune.Red);
                    break;
                case RunePatternIDs.LineDiagDownLeft:
                    RuneController.instance.EnqueueRune(Rune.Green);
                    break;
                case RunePatternIDs.LineDiagTopLeft:
                    RuneController.instance.EnqueueRune(Rune.Blue);
                    break;
                case RunePatternIDs.LineDiagTopRight:
                    RuneController.instance.EnqueueRune(Rune.Yellow);
                    break;

                // case GesturePatternIDs.RUNE_PARALLEL_HORIZONTAL:
                //     RuneController.INSTANCE.EnqueueRune(new Rune(RuneType.Mid, (int)MidAttackRune.Green));
                //     break;
                // case GesturePatternIDs.RUNE_WAVE_VERTICAL:
                //     RuneController.INSTANCE.EnqueueRune(new Rune(RuneType.Mid, (int)MidAttackRune.Red));
                //     break;
                // case GesturePatternIDs.RUNE_WAVE_HORIZONTAL:
                //     RuneController.INSTANCE.EnqueueRune(new Rune(RuneType.Mid, (int)MidAttackRune.Blue));
                //     break;
                // case GesturePatternIDs.RUNE_CROSS:
                //     RuneController.INSTANCE.EnqueueRune(new Rune(RuneType.Mid, (int)MidAttackRune.Orange));
                //     break;
            }
        }
    }

    public enum PlayerBridgeMoveType
    {
        Tap = 0,
        Gesture = 1
    }
}