﻿using System.Collections;
using TMPro;
using UniRx;
using UnityEngine;

namespace AVB
{
    public class QuickMessage : MonoBehaviour
    {
        public static QuickMessage instance;

        private RectTransform rect;
        private TextMeshProUGUI textTMP;

        private const float ScaleUpSpeed = 1f;
        private const float FadeOutSpeed = 3f;

        private readonly CompositeDisposable compDisp = new();

        private void Awake()
        {
            rect = GetComponent<RectTransform>();
            textTMP = GetComponent<TextMeshProUGUI>();
        }

        private void Start()
        {
            textTMP.text = "";
        }

        private void OnDestroy()
        {
            compDisp.Dispose();
        }

        public void ShowMessage(string text, Vector3 scaleFrom, Vector3 scaleTo, float duration, bool fadeOut = false,
            ColorParams colorParams = null)
        {
            compDisp.Clear();
            Observable.FromCoroutine(_ => Seq_ShowMessage(text, scaleFrom, scaleTo, duration, fadeOut, colorParams))
                .Subscribe().AddTo(compDisp);
        }

        public IEnumerator Seq_ShowMessage(string text, Vector3 scaleFrom, Vector3 scaleTo, float duration,
            bool fadeOut = false, ColorParams colorParams = null)
        {
            textTMP.alpha = 1f;
            textTMP.text = text;
            textTMP.color = colorParams?.textColor ?? Color.white;
            textTMP.outlineColor = colorParams?.textOutlineColor ?? Color.clear;

            rect.localScale = scaleFrom;
            var t = 0f;
            while (Vector3.Distance(rect.localScale, scaleTo) > 0.01f)
            {
                t += Time.fixedDeltaTime / duration;
                rect.localScale = Vector3.Lerp(scaleFrom, scaleTo, t);
                yield return new WaitForFixedUpdate();
            }

            if (fadeOut)
            {
                t = 0f;
                while (Mathf.Abs(textTMP.alpha - 0f) > 0.01f)
                {
                    t += Time.fixedDeltaTime / duration;
                    textTMP.alpha = Mathf.Lerp(1f, 0f, t);
                    yield return new WaitForFixedUpdate();
                }

                textTMP.alpha = 0f;
            }

            textTMP.text = "";
            rect.localScale = Vector3.one;
        }

        public class ColorParams
        {
            public Color textColor;
            public Color textOutlineColor;

            public ColorParams(Color textColor, Color textOutlineColor)
            {
                this.textColor = textColor;
                this.textOutlineColor = textOutlineColor;
            }
        }
    }
}