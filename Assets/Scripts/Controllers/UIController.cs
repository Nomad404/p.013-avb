﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.UI;

namespace AVB
{
    public class UIController : MonoBehaviour
    {
        public static UIController instance;
        [Header("Components")] public Image foregroundOverlay;
        public TextMeshProUGUI headerText;
        public TextMeshProUGUI subtitleText;

        private Canvas canvas;

        private void Awake()
        {
            if (!instance) instance = this;
            QuickMessage.instance = GetComponentInChildren<QuickMessage>();
            foregroundOverlay.color = Color.black;

            canvas = GetComponent<Canvas>();
            canvas.sortingOrder = UILayers.Default;
        }

        public void FadeForegroundOverlay(bool toBlack)
        {
            StartCoroutine(Seq_FadeForegroundOverlay(toBlack));
        }

        public IEnumerator Seq_FadeForegroundOverlay(bool toBlack)
        {
            var target = toBlack ? new Color(0, 0, 0, 1) : new Color(0, 0, 0, 0);
            if (toBlack) foregroundOverlay.enabled = true;
            while (Mathf.Abs(foregroundOverlay.color.a - target.a) > 0.01)
            {
                foregroundOverlay.color = Color.Lerp(foregroundOverlay.color, target, 2f * Time.deltaTime);

                if (toBlack)
                    foregroundOverlay.raycastTarget = foregroundOverlay.color.a switch
                    {
                        > 0.5f => true,
                        < 0.5f => false,
                        _ => foregroundOverlay.raycastTarget
                    };

                yield return new WaitForEndOfFrame();
            }

            foregroundOverlay.color = target;
            if (!toBlack) foregroundOverlay.enabled = false;
        }

        public IEnumerator Seq_FadeTextElement(TextMeshProUGUI text, float start, float target, float duration)
        {
            yield return Seq_FadeTextElements(new List<TextMeshProUGUI> {text}, start, target, duration);
        }

        public IEnumerator Seq_FadeTextElements(List<TextMeshProUGUI> texts, float start, float target, float duration)
        {
            var t = 0f;
            texts.ForEach(text => text.alpha = start);
            while (texts.Any(text => Mathf.Abs(text.alpha - target) > 0.01f))
            {
                t += Time.fixedDeltaTime / duration;
                texts.ForEach(text => text.alpha = Mathf.Lerp(start, target, t));
                yield return new WaitForFixedUpdate();
            }

            texts.ForEach(text => text.alpha = target);
        }

        public void SetUIText(TextMeshProUGUI uiText, LocalizedString text, Vector2 size)
        {
            uiText.text = text.GetLocalizedString();
            uiText.rectTransform.sizeDelta = size;

            var parentTransform = (RectTransform) uiText.transform.parent.transform;
            var pos = parentTransform.position;
            parentTransform.position = new Vector3(pos.x, pos.y + size.y);
        }
    }
}