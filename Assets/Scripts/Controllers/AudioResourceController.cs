﻿using UnityEngine;

namespace AVB
{
    public class AudioResourceController : MonoBehaviour
    {
        public static AudioResourceController instance;

        private void Awake()
        {
            if (!instance) instance = this;
        }

        [Header("Rune selection sounds")] public AudioClip[] runeSelectionLow;

        public AudioClip RandomRuneSelectionLowSound => runeSelectionLow[Random.Range(0, runeSelectionLow.Length)];

        public AudioClip[] runeSelectionMidFire;

        public AudioClip RandomRuneSelectionMidFireSound =>
            runeSelectionMidFire[Random.Range(0, runeSelectionMidFire.Length)];

        public AudioClip[] runeSelectionMidWater;

        public AudioClip RandomRuneSelectionMidWaterSound =>
            runeSelectionMidWater[Random.Range(0, runeSelectionMidWater.Length)];

        public AudioClip[] runeSelectionMidFlash;

        public AudioClip RandomRuneSelectionMidFlashSound =>
            runeSelectionMidFlash[Random.Range(0, runeSelectionMidFlash.Length)];

        public AudioClip[] runeSelectionMidLeaf;

        public AudioClip RandomRuneSelectionMidLeafSound =>
            runeSelectionMidLeaf[Random.Range(0, runeSelectionMidLeaf.Length)];

        [Header("Bridge sounds")] public AudioClip[] bridgeDamageSounds;

        public AudioClip RandomBridgeDamageSound => bridgeDamageSounds[Random.Range(0, bridgeDamageSounds.Length)];

        public AudioClip[] bridgeDeathSounds;

        public AudioClip RandomBridgeDeathSound => bridgeDeathSounds[Random.Range(0, bridgeDeathSounds.Length)];

        [Header("Enemy sounds")] public AudioClip[] enemyDamageSounds;

        public AudioClip RandomEnemyDamageSound => enemyDamageSounds[Random.Range(0, enemyDamageSounds.Length)];

        public AudioClip[] enemyDeathSounds;

        public AudioClip RandomEnemyDeathSound => enemyDeathSounds[Random.Range(0, enemyDeathSounds.Length)];

        public AudioClip[] enemyBlockSounds;
        public AudioClip RandomEnemyBlockSound => enemyBlockSounds[Random.Range(0, enemyBlockSounds.Length)];

        [Header("Player sounds")] public AudioClip[] playerReloadSounds;

        public AudioClip RandomPlayerReloadSound => playerReloadSounds[Random.Range(0, playerReloadSounds.Length)];

        public AudioClip[] playerShootSounds;

        public AudioClip RandomPlayerShootSound => playerShootSounds[Random.Range(0, playerShootSounds.Length)];

        [Header("Menu sounds")] public AudioClip menuButtonClick;
        public AudioClip menuButtonStartLevel;

        [Header("Level sounds")] public AudioClip levelWin;
        public AudioClip levelLose;
        public AudioClip levelNewWave;
        public AudioClip[] levelStarRanks;

        [Header("Music")] public AudioClip mainMenuMusic;
    }
}