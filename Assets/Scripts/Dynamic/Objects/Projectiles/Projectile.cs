﻿using UnityEngine;

namespace AVB
{
    public abstract class Projectile : MovingObject
    {
        private const float DeathTimer = 5.0f;
        
        protected virtual void Start()
        {
            Destroy(gameObject, DeathTimer);
        }

        protected override void OnObjectMove()
        {
        }

        protected override void OnCantMove(RaycastHit2D hitResult)
        {
        }

        protected override void OnObjectStop()
        {
        }
    }
}