using System.Collections.Generic;
using UnityEngine;

namespace AVB
{
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(AudioSource))]
    public abstract class BridgePart : MonoBehaviour
    {
        public SpriteRenderer baseSprite;
        public List<SpriteRenderer> bottomOverlaySprites;

        protected static Player Player => Player.instance;
        protected static BridgeController BridgeController => BridgeController.instance;

        protected BoxCollider2D boxCol;
        protected AudioSource audioSource;

        protected Vector3 baseOriginalStartPosition;

        public float SegmentWidth => baseSprite.bounds.size.x;

        protected virtual void Awake()
        {
            boxCol = GetComponent<BoxCollider2D>();
            audioSource = GetComponent<AudioSource>();

            if (bottomOverlaySprites.TrueForAll(sprite => sprite != null))
            {
                bottomOverlaySprites.ForEach(sprite => sprite.sortingOrder = SpriteLayers.BridgeSegDeathOverlay - 1);
            }
        }
    }
}