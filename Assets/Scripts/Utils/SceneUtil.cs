﻿using UnityEngine.SceneManagement;

namespace AVB
{
    public static class SceneUtil
    {
        public static Scene[] GetAllScenes()
        {
            var result = new Scene[SceneManager.sceneCount];
            for (var i = 0; i < result.Length; i++)
            {
                result[i] = SceneManager.GetSceneAt(i);
            }

            return result;
        }
    }
}