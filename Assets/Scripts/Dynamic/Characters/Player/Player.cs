﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace AVB
{
    public class Player : Character
    {
        public static Player instance;
        public SpriteRenderer baseSprite;
        public GameObject arrowPrefab;

        private const float ArrowSpawnPosShift = 2f;
        private const float ArrowSpawnDelay = 0.2f;

        private int _currentBridgeIndex = -1;

        public int CurrentBridgeIndex
        {
            get => _currentBridgeIndex;
            set
            {
                _currentBridgeIndex = value;
                Bridge.HighlightedSegmentIndex = _currentBridgeIndex;
                onNewBridgeIndex.Invoke(_currentBridgeIndex);
            }
        }

        [HideInInspector] public UnityEvent<int> onNewBridgeIndex = new();

        private readonly ReactiveProperty<AttackRuneSet> _latestAttackRunes = new();

        public bool CanReceiveMoveRequests { get; set; }

        private PlayerSpineController _spineController;

        private static BridgeController Bridge => BridgeController.instance;

        public int CurrentScore { get; set; }
        public int KillCount { get; set; }

        private new void Awake()
        {
            base.Awake();
            if (!instance) instance = this;
            directionMethod = DirectionMethod.Reactive;
            baseSprite.sortingOrder = SpriteLayers.Player;

            _spineController = GetComponentInChildren<PlayerSpineController>();
        }

        //DEBUG: remove this for release
        private void Start()
        {
            RuneController.instance.rxLatestRune
                .Skip(1)
                .Subscribe(OnNewRune).AddTo(this);
        }

        protected override void OnObjectMove()
        {
        }

        protected override void OnCantMove(RaycastHit2D hitResult)
        {
        }

        protected override void OnObjectStop()
        {
            _spineController.EndDash();
        }

        private void OnNewRune(Rune rune)
        {
            if (rune != Rune.Empty)
            {
                if (rune.majorType == RuneType.General)
                {
                    switch (rune.subType)
                    {
                        case (int) GeneralRune.Move:
                        {
                            if (rune.runeParams is MoveRuneParams p) OnMoveRune(p.startPoint, p.endPoint);
                            return;
                        }
                        case (int) GeneralRune.Attack:
                            OnAttackRune();
                            return;
                    }
                }
            }
        }

        private void OnMoveRune(Vector3 posStartScreen, Vector3 posEndScreen)
        {
            var segmentWidth = Bridge.bridgeSegments.First().GetComponent<SpriteRenderer>().bounds.size
                .x;
            if (Camera.main is null) return;
            var posStartWorld = Camera.main.ScreenToWorldPoint(posStartScreen);
            var posEndWorld = Camera.main.ScreenToWorldPoint(posEndScreen);
            var distance = Vector2.Distance(posStartWorld, posEndWorld);

            var moveDistanceIndex = (int) (Mathf.Clamp(distance, 0, segmentWidth * 5) / segmentWidth) + 1;
            var newIndex = CurrentBridgeIndex +
                           (posStartWorld.x < posEndWorld.x ? moveDistanceIndex : -moveDistanceIndex);
            var toRight = newIndex > CurrentBridgeIndex;

            if (newIndex == CurrentBridgeIndex) return;
            Bridge.bridgeSegments[CurrentBridgeIndex].SwitchRowHighlight(false, false);

            CurrentBridgeIndex = Mathf.Clamp(newIndex, 0, Bridge.VisibleSegmentCount - 1);

            Bridge.bridgeSegments[CurrentBridgeIndex].SwitchRowHighlight(true, false);

            var segment = Bridge.bridgeSegments[CurrentBridgeIndex];
            var segTransform = segment.transform;
            var segPos = segTransform.position;
            MoveToOverTime(new Vector3(segPos.x, segPos.y, transform.position.z), 1.0f);
        }

        private void OnAttackRune()
        {
            if (CanAttack && _spineController.CanAttack && EnemyController.instance.CurrentMatchingTargets.Count > 0)
            {
                _latestAttackRunes.Value = AttackRuneSet.FromRuneSet(RuneController.instance.CurrentRuneSet);
                _spineController.StartShot();
                RuneController.instance.ConfirmAttackRune();
            }
            else
            {
                RuneController.instance.runeEventSubject.OnNext(new RuneEventArgs(ResponseType.Error));
            }
        }

        public void OnBridgeSegmentTapped(int newIndex)
        {
            if (CurrentBridgeIndex == newIndex || !CanReceiveMoveRequests || !CanMove) return;
            var steps = Mathf.Abs(newIndex - CurrentBridgeIndex);
            CurrentBridgeIndex = newIndex;
            var destination = Bridge.bridgeSegments[CurrentBridgeIndex].transform.position;
            _spineController.StartDash(destination.x > transform.position.x,
                _spineController.GetDashTimeScale(steps));
            MoveToOverTime(destination, steps * 0.2f);
        }

        public IEnumerator Seq_DashToLocation(Vector3 destination, float timeFrame, bool shouldWait = false,
            float dashTimeScale = 1f)
        {
            _spineController.StartDash(destination.x > transform.position.x, dashTimeScale);
            MoveToOverTime(destination, timeFrame);
            if (shouldWait) yield return new WaitForSeconds(timeFrame);
            else yield return null;
        }

        public Arrow GetNewArrowInstance()
        {
            return Instantiate(arrowPrefab, new Vector3(-10, -10, 0), Quaternion.identity).GetComponent<Arrow>();
        }

        private IEnumerator Seq_AttackAllMatchingEnemies(List<Enemy> enemies)
        {
            SpawnArrow(enemies[0].RuneSet, enemies[0].gameObject);
            if (enemies.Count <= 1) yield break;
            for (var i = 1; i < enemies.Count; i++)
            {
                yield return new WaitForSeconds(ArrowSpawnDelay);
                SpawnArrow(enemies[i].RuneSet, enemies[i].gameObject);
            }
        }

        private void SpawnArrow(AttackRuneSet runeSet, GameObject target)
        {
            GetArrowSpawnCoords(out var outSpawnPosition, out var outSpawnRotation);
            var arrow = Instantiate(arrowPrefab, outSpawnPosition, outSpawnRotation).GetComponent<Arrow>();
            arrow.RuneSet = runeSet;
            arrow.moveTarget = target;

            var enemy = target.GetComponent<Enemy>();
            if (enemy)
            {
                arrow.GetComponent<SpriteRenderer>().sortingOrder = enemy.GetComponent<SpriteRenderer>().sortingOrder;
            }
        }

        public void ShootArrow(Arrow arrow)
        {
            arrow.transform.parent = null;
            Observable.FromCoroutine(_ =>
                    Seq_ShootAllEnemiesWithOneArrow(EnemyController.instance.CurrentMatchingTargets, arrow))
                .Subscribe(_ => RuneController.instance.ClearQueue())
                .AddTo(this);
        }

        private IEnumerator Seq_ShootAllEnemiesWithOneArrow(List<Enemy> targets, Arrow arrowInstance)
        {
            arrowInstance.RuneSet = _latestAttackRunes.Value;
            foreach (var e in targets) arrowInstance.rxAttackTargets.Add(e);
            arrowInstance.moveTarget = arrowInstance.rxAttackTargets[0].gameObject;
            arrowInstance.trail.enabled = true;
            arrowInstance.SetArrowSortOrder(arrowInstance.rxAttackTargets[0].TorsoSortingOrder);
            arrowInstance.CanMove = true;
            yield return null;
        }

        private IEnumerator Seq_ShootAllEnemiesSeparate(List<Enemy> targets, Arrow arrowInstance)
        {
            foreach (var e in targets)
            {
                var arrowClone = Instantiate(arrowInstance).GetComponent<Arrow>();
                arrowClone.RuneSet = e.RuneSet;
                arrowClone.moveTarget = e.gameObject;
                arrowClone.GetComponentInChildren<SpriteRenderer>().sortingOrder =
                    e.GetComponentInChildren<EnemySpriteController>().BaseSortingOrder + 4;
                arrowClone.CanMove = true;
                yield return new WaitForSeconds(0.1f);
            }

            Destroy(arrowInstance.gameObject);
            yield return null;
        }

        private void GetArrowSpawnCoords(out Vector2 pos, out Quaternion rot)
        {
            pos = (Vector2) transform.position + (Vector2.up * ArrowSpawnPosShift);
            rot = Quaternion.Euler(new Vector3(0, 0, 0));
        }

        public override void Kill()
        {
            _spineController.StartDeath();
        }

        public void OnDeathEnd()
        {
            var pos = Bridge.bridgeSegments[CurrentBridgeIndex].deadLoopOverlayObj.transform
                .position;
            MoveToOverTime(pos, 1f);
        }

        protected override void OnHealthDepleted()
        {
        }
    }
}