﻿using UnityEngine;

namespace AVB
{
    public class DefaultMovingObject : MovingObject
    {
        public DirectionMethod moveMethod;

        private new void Awake()
        {
            base.Awake();

            directionMethod = moveMethod;
        }

        protected override void OnCantMove(RaycastHit2D hitResult)
        {
        }

        protected override void OnObjectMove()
        {
        }

        protected override void OnObjectStop()
        {
        }
    }
}