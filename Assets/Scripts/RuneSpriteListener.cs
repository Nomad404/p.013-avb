﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AVB
{
    public class RuneSpriteListener : RuneChangeListener
    {
        public RuneType runeType;
        public Sprite[] spritesLow;
        public Sprite[] spritesMid;
        public Sprite[] spritesHigh;

        public Image[] imageComps;

        protected override void OnRuneChanged(Rune rune)
        {
            var currentRunes = RuneController.instance.CurrentRuneQueue;
            var temp = new List<Rune>(new Rune[] { Rune.Empty, Rune.Empty, Rune.Empty });
            for (var i = 0; i < currentRunes.Count; i++)
            {
                temp[i] = currentRunes[i];
            }

            for (var i = 0; i < temp.Count; i++)
            {
                imageComps[i].sprite = GetRuneSprite(temp[i].majorType, temp[i].subType);
                imageComps[i].color = imageComps[i].sprite != null ? Color.white : new Color(1, 1, 1, 0);
            }
        }

        private void ClearAllImages()
        {
            foreach (var i in imageComps)
            {
                i.sprite = null;
            }
        }

        private Sprite GetRuneSprite(RuneType majorType, int subtype)
        {
            return majorType switch
            {
                RuneType.Low => GetLowRuneSprite(subtype),
                RuneType.Mid => GetMidRuneSprite(subtype),
                _ => null
            };
        }

        private Sprite GetLowRuneSprite(int type)
        {
            return spritesLow[Mathf.Clamp(type - 1, 0, spritesLow.Length - 1)];
        }

        private Sprite GetMidRuneSprite(int type)
        {
            return spritesMid[Mathf.Clamp(type - 1, 0, spritesMid.Length - 1)];
        }
    }
}