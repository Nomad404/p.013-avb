using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;
using UnityEngine.UI;
using UniRxExt = UniRx.ObservableExtensions;

namespace AVB
{
    [RequireComponent(typeof(AudioSource))]
    public class StartMenuController : MonoBehaviour
    {
        public static StartMenuController instance;

        [Header("Buttons")] public Button btnStart;
        public Button btnTutorial;
        public Button btnQuit;

        [Header("Localization")] public LocalizedString buttonTextStarGame;
        public LocalizedString buttonTextContinue;

        [Header("Containers")] public CanvasGroup buttonsContainer;
        public CanvasGroup creditsContainer;
        public CanvasGroup thankYouContainer;
        private readonly CompositeDisposable _compDisp = new();
        private const float ContainerFadeSpeed = 0.3f;

        private static GameController GameController => GameController.instance;

        private AudioSource _audioSource;

        private void Awake()
        {
            if (!instance) instance = this;
            _audioSource = GetComponent<AudioSource>();
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                btnQuit.gameObject.SetActive(false);
            }
        }

        private void Start()
        {
            var text = btnStart.GetComponentInChildren<LocalizeStringEvent>();
            text.StringReference = GameController.CurrentLevel > 0 ? buttonTextContinue : buttonTextStarGame;

            btnTutorial.gameObject.SetActive(GameController.currentSaveGame.tutorialsFinished);

#if (UNITY_ANDROID || UNITY_IPHONE)
            if (!GameServices.Instance.IsLoggedIn())
            {
                GameServices.Instance.LogIn();
            }
#endif
        }

        public void PlayButtonClickSound()
        {
            _audioSource.PlayOneShot(AudioResourceController.instance.menuButtonClick);
        }

        public void StartGame(GameConfig config)
        {
            GameController.gameConfig = config;
            GameController.LoadLevel(LevelNames.LoadingScreen);
        }

        public void QuitGame()
        {
            GameController.QuitGame();
        }

        public void ResetProgress()
        {
            GameController.instance.CurrentLevel = 0;
            btnStart.GetComponentInChildren<LocalizeStringEvent>().StringReference = buttonTextStarGame;
        }

        public void UpdateMenuMode(int targetMode)
        {
            UpdateMenuMode((MenuMode)targetMode, false);
        }

        public void UpdateMenuMode(MenuMode targetMode, bool instant = true)
        {
            _compDisp.Clear();
            if (instant)
            {
                buttonsContainer.alpha = targetMode switch
                {
                    MenuMode.Default => 1f,
                    _ => 0f,
                };
                buttonsContainer.gameObject.SetActive(targetMode == MenuMode.Default);

                creditsContainer.alpha = targetMode switch
                {
                    MenuMode.Credits => 1f,
                    _ => 0f
                };
                creditsContainer.gameObject.SetActive(targetMode == MenuMode.Credits);

                thankYouContainer.alpha = targetMode switch
                {
                    MenuMode.ThankYou => 1f,
                    _ => 0f
                };
                thankYouContainer.gameObject.SetActive(targetMode == MenuMode.ThankYou);
            }
            else
            {
                var fromContainers = targetMode switch
                {
                    MenuMode.Default => new List<CanvasGroup> { creditsContainer, thankYouContainer },
                    MenuMode.Credits => new List<CanvasGroup> { buttonsContainer, thankYouContainer },
                    MenuMode.ThankYou => new List<CanvasGroup> { buttonsContainer, creditsContainer },
                    _ => throw new ArgumentOutOfRangeException(nameof(targetMode), targetMode, null)
                };
                var toContainer = targetMode switch
                {
                    MenuMode.Default => buttonsContainer,
                    MenuMode.Credits => creditsContainer,
                    MenuMode.ThankYou => thankYouContainer,
                    _ => throw new ArgumentOutOfRangeException(nameof(targetMode), targetMode, null)
                };
                UniRxExt.Subscribe(Observable.FromCoroutine(_ => Seq_FadeContainer(fromContainers, toContainer)))
                    .AddTo(_compDisp);
            }
        }

        private static IEnumerator Seq_FadeContainer(List<CanvasGroup> fromContainers, CanvasGroup toContainer)
        {
            while (fromContainers.Any(c => c.alpha > 0.01f))
            {
                fromContainers.ForEach(c => c.alpha = Mathf.Lerp(c.alpha, 0f, ContainerFadeSpeed));
                yield return new WaitForFixedUpdate();
            }

            fromContainers.ForEach(c =>
            {
                c.alpha = 0f;
                c.gameObject.SetActive(false);
            });

            toContainer.gameObject.SetActive(true);

            while (toContainer.alpha < 0.99f)
            {
                toContainer.alpha = Mathf.Lerp(toContainer.alpha, 1f, ContainerFadeSpeed);
                yield return new WaitForFixedUpdate();
            }

            toContainer.alpha = 1f;
        }

        public enum MenuMode
        {
            Default = 0,
            Credits = 1,
            ThankYou = 2
        }
    }
}