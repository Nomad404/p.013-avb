﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace AVB
{
    public class RuneInfoContainer : MonoBehaviour
    {
        public static RuneInfoContainer instance;

        public RuneInfoImage runeImagePrefab;

        [HideInInspector] public RuneInfoSlot[] slots = new RuneInfoSlot[5];
        public int SlotCount => slots.Length;
        private ReactiveCollection<RuneInfoImage> imageQueue;

        [Header("Sounds")] private AudioSource audioSource;

        private void Awake()
        {
            instance = this;

            slots = GetComponentsInChildren<RuneInfoSlot>();
            audioSource = GetComponent<AudioSource>();
            imageQueue = new Queue<RuneInfoImage>(RuneController.MAXQueueSize).ToReactiveCollection();
            imageQueue.ObserveAdd().Subscribe(e =>
            {
                if (imageQueue.Count > RuneController.MAXQueueSize)
                {
                    imageQueue[0].CurrentSlotIndex = 0;
                    imageQueue.RemoveAt(0);
                }

                foreach (var image in imageQueue) image.CurrentSlotIndex = imageQueue.IndexOf(image) + 1;
            }).AddTo(this);

            audioSource = GetComponent<AudioSource>();

            var bottomPadding = Screen.height - Screen.safeArea.y - Screen.safeArea.height;
            if (bottomPadding > 0)
            {
                var newPos = transform.localPosition;
                newPos.y += bottomPadding / 2;
                transform.localPosition = newPos;
            }
        }

        private void Start()
        {
            RuneController.instance.rxLatestRune
                .Subscribe(OnNewRune)
                .AddTo(this);
            RuneController.instance.OnRuneEvent
                .Subscribe(args =>
                {
                    if (args != null)
                    {
                        SetSlotsGlow(args.ResponseType, args.RuneSetStartIndex);
                    }
                    else
                    {
                        SetSlotsGlow(ResponseType.Default);
                    }
                }).AddTo(this);
        }

        public void SetSlotsGlow(ResponseType res, int startIndex = -1)
        {
            var runeQueueLength = RuneController.instance.CurrentRuneQueue.Count;
            var start = Mathf.Clamp(startIndex + 1, 1, RuneController.MAXQueueSize);
            var end = Mathf.Clamp(runeQueueLength, 1, RuneController.MAXQueueSize);
            switch (res)
            {
                case ResponseType.Accepted:
                    slots.Where((s, index) => index >= start && index <= end).ToList()
                        .ForEach(slot => slot.SetGlow(ResponseType.Accepted));
                    slots.Where((s, index) => !(index >= start && index <= end)).ToList()
                        .ForEach(slot => slot.SetGlow(ResponseType.Default));
                    break;
                case ResponseType.Selected:
                    slots.Where((s, index) => index >= start && index <= end).ToList()
                        .ForEach(slot => slot.SetGlow(ResponseType.Selected));
                    slots.Where((s, index) => !(index >= start && index <= end)).ToList()
                        .ForEach(slot => slot.SetGlow(ResponseType.Default));
                    break;
                case ResponseType.Error:
                    slots.Where((s, index) => index >= 1 && index <= slots.Length - 2).ToList()
                        .ForEach(slot => slot.SetGlow(ResponseType.Error));
                    break;
                default:
                    slots.ToList().ForEach(slot => slot.SetGlow(ResponseType.Default));
                    break;
            }
        }

        private void OnNewRune(Rune rune)
        {
            switch (rune.majorType)
            {
                case RuneType.General:
                {
                    if (rune.subType == (int)GeneralRune.Clear)
                    {
                        foreach (var image in imageQueue) image.Clear();
                        imageQueue.Clear();
                        RuneController.instance.runeEventSubject.OnNext(null);
                    }

                    break;
                }
                case RuneType.Low:
                case RuneType.Mid:
                case RuneType.High:
                {
                    PlayRuneSelectionSound(rune);
                    var index = Mathf.Clamp(imageQueue.Count + 2, 0, slots.Length - 1);
                    var image = Instantiate(runeImagePrefab, slots[index].transform);
                    image.CurrentRune = rune;
                    imageQueue.Add(image);
                    break;
                }
            }
        }

        private void PlayRuneSelectionSound(Rune rune)
        {
            switch (rune.majorType)
            {
                case RuneType.Low:
                    audioSource.PlayOneShot(AudioResourceController.instance.RandomRuneSelectionLowSound);
                    break;
                case RuneType.Mid:
                    switch ((MidAttackRune)rune.subType)
                    {
                        case MidAttackRune.Red:
                            audioSource.PlayOneShot(AudioResourceController.instance.RandomRuneSelectionMidFireSound);
                            break;
                        case MidAttackRune.Blue:
                            audioSource.PlayOneShot(AudioResourceController.instance.RandomRuneSelectionMidWaterSound);
                            break;
                        case MidAttackRune.Green:
                            audioSource.PlayOneShot(AudioResourceController.instance.RandomRuneSelectionMidLeafSound);
                            break;
                        case MidAttackRune.Yellow:
                            audioSource.PlayOneShot(AudioResourceController.instance.RandomRuneSelectionMidFlashSound);
                            break;
                    }

                    break;
            }
        }
    }
}