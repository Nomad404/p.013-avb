﻿using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace AVB
{
    public class RuneInfoSlot : MonoBehaviour
    {
        public RuneInfoSlotType slotType;

        private readonly ReactiveProperty<ResponseType> glowState =
            new(ResponseType.Default);

        public ResponseType GlowState
        {
            get => glowState.Value;
            set => glowState.SetValueAndForceNotify(value);
        }

        public Image glow;
        public Image background;

        private static readonly Color GlowDefault = new Color(1, 1, 1, 0);
        private static readonly Color GlowSelected = Color.yellow;
        private static readonly Color GlowAccepted = Color.green;
        private static readonly Color GlowError = Color.red;

        private readonly CompositeDisposable fadeCompDisp = new();

        private void Awake()
        {
            background.color = slotType switch
            {
                RuneInfoSlotType.Default => new Color(1, 1, 1, 1),
                _ => new Color(1, 1, 1, 0)
            };
        }

        private void Start()
        {
            glowState.Subscribe(SetGlow).AddTo(this);
        }

        private void OnDestroy()
        {
            fadeCompDisp.Dispose();
        }

        public void SetGlow(ResponseType newState)
        {
            fadeCompDisp.Clear();
            switch (newState)
            {
                case ResponseType.Selected:
                    Observable.FromCoroutine(_ => Seq_FadeGlowToColor(GlowSelected))
                        .Subscribe().AddTo(fadeCompDisp);
                    break;
                case ResponseType.Accepted:
                    glow.color = GlowAccepted;
                    var c1 = GlowAccepted;
                    c1.a = 0.0f;
                    Observable.FromCoroutine(_ => Seq_FadeGlowToColor(c1, delay: 0.5f))
                        .Subscribe().AddTo(fadeCompDisp);
                    break;
                case ResponseType.Error:
                    glow.color = GlowError;
                    var c2 = GlowError;
                    c2.a = 0.0f;
                    Observable.FromCoroutine(_ => Seq_FadeGlowToColor(c2, delay: 0.5f))
                        .Subscribe().AddTo(fadeCompDisp);
                    break;
                default:
                    Observable.FromCoroutine(_ => Seq_FadeGlowToColor(GlowDefault, 4))
                        .Subscribe().AddTo(fadeCompDisp);
                    break;
            }
        }

        private IEnumerator Seq_FadeGlowToColor(Color target, float speed = 3f, float delay = 0.0f)
        {
            yield return new WaitForSeconds(delay);
            while (Mathf.Abs(glow.color.GetMagnitude() - target.GetMagnitude()) > 0.000001f)
            {
                glow.color = Color.Lerp(glow.color, target, speed * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }

            glow.color = target;
        }
    }

    public enum RuneInfoSlotType
    {
        Default = 0,
        Empty = 1
    }
}