﻿namespace AVB
{
    public class BridgeEdge : BridgePart
    {
        protected override void Awake()
        {
            base.Awake();

            baseOriginalStartPosition = baseSprite.transform.localPosition;
            baseSprite.sortingOrder = SpriteLayers.BridgeSegDeathOverlay;
        }

        public void FlipBaseHorizontally()
        {
            var transform1 = baseSprite.transform;
            var scale = transform1.localScale;
            scale.x *= -1;
            transform1.localScale = scale;
        }
    }
}