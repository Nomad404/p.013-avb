﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UniRxExt = UniRx.ObservableExtensions;

namespace AVB
{
    public class RuneController : MonoBehaviour
    {
        public static RuneController instance;

        public const int MAXQueueSize = 3;

        public bool CanReceiveRunes { get; set; } = true;
        private Queue<Rune> _runeQueue;

        public List<Rune> CurrentRuneQueue => new(_runeQueue.ToArray());

        [HideInInspector] public ReactiveProperty<Rune> rxLatestRune = new();

        public readonly Subject<RuneEventArgs> runeEventSubject = new();
        public IObservable<RuneEventArgs> OnRuneEvent => runeEventSubject;

        public List<Rune> runeFilter = new();

        private Rune _lastLowRune = Rune.Empty;
        private int _runeComboStart;

        private const int DefaultRuneSetSize = 3;

        public ReactiveProperty<RuneSet> rxCurrentRuneSet = new(new RuneSet(DefaultRuneSetSize));

        public RuneSet CurrentRuneSet
        {
            get => rxCurrentRuneSet.Value;
            private set => rxCurrentRuneSet.Value = value;
        }

        private static Player Player => Player.instance;

        private void Awake()
        {
            if (!instance) instance = this;
            _runeQueue = new Queue<Rune>(MAXQueueSize);
        }

        private void Start()
        {
            UniRxExt.Subscribe(rxLatestRune.Skip(1), OnNewRune).AddTo(this);
        }

        public void EnqueueRune(Rune rune)
        {
            if (!CanReceiveRunes) return;
            if (rune == Rune.Clear)
            {
                _runeQueue.Clear();
                CurrentRuneSet = new RuneSet(DefaultRuneSetSize);
            }
            // When a rune is not allowed by a filter
            else if (runeFilter.Count != 0 && runeFilter.All(r => rune != r) &&
                     rune.majorType != RuneType.General)
            {
                PainterCanvasFader.instance.FadeoutPainting(Color.white);
                return;
            }
            else if (rune.majorType != RuneType.General)
            {
                if (_runeQueue.Count >= MAXQueueSize) _runeQueue.Dequeue();
                _runeQueue.Enqueue(rune);
            }

            rxLatestRune.SetValueAndForceNotify(rune);
        }

        public void ClearQueue()
        {
            EnqueueRune(Rune.Clear);
        }

        // public void ClearQueueUntilRune(Rune rune){
        //     while (runeQueue.Peek() != rune){
        //         runeQueue.Dequeue();
        //     }
        //     OnNewRuneEnqueued.Value = Rune.Empty;
        // }

        private void OnNewRune(Rune rune)
        {
            if (rune != Rune.Empty)
            {
                switch (rune.majorType)
                {
                    case RuneType.General:
                        return;
                    default:
                        OnTypeRune(rune);
                        return;
                }
            }

            _runeComboStart = 0;
        }

        public void ConfirmAttackRune()
        {
            runeEventSubject.OnNext(new RuneEventArgs(ResponseType.Accepted, _runeComboStart));
        }

        private void OnTypeRune(Rune rune)
        {
            var runesInQueue = CurrentRuneQueue;
            if (rune.majorType == RuneType.Low) _lastLowRune = rune;

            _runeComboStart = Mathf.Clamp(runesInQueue.LastIndexOf(_lastLowRune), 0, runesInQueue.Count - 1);

            var currentRunes = runesInQueue.GetRange(_runeComboStart, runesInQueue.Count - _runeComboStart);
            CurrentRuneSet = new RuneSet(DefaultRuneSetSize);
            CurrentRuneSet.UpdateRunes(currentRunes.ToArray());

            var hasMatchingTargets =
                EnemyController.instance.GetAllEnemiesOnRowByRuneSet(Player.CurrentBridgeIndex, CurrentRuneSet).Count >
                0;
            // if (_currentAttackTargets.Count != 0)
            // {
            //     // Set selection glow for attackable & non-attackable targets 
            //     EnemyController.AllLivingEnemies.ToList()
            //         .ForEach(enemy => enemy.SetSelectionGlow(_currentAttackTargets.Contains(enemy)));
            // }
            runeEventSubject.OnNext(hasMatchingTargets
                ? new RuneEventArgs(ResponseType.Selected, _runeComboStart)
                : null);
        }
    }

    public class RuneEventArgs : EventArgs
    {
        public ResponseType ResponseType { get; } = ResponseType.Default;
        public int RuneSetStartIndex { get; } = -1;

        public RuneEventArgs(ResponseType resType = ResponseType.Default, int setStartIndex = -1)
        {
            ResponseType = resType;
            RuneSetStartIndex = setStartIndex;
        }
    }
}