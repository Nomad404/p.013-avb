using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UniRx;
using UnityEngine;
using Object = UnityEngine.Object;
using UniRxExt = UniRx.ObservableExtensions;

namespace AVB.Tutorial
{
    public abstract class TutorialScript : MonoBehaviour
    {
        protected abstract int StepCount { get; }
        public List<CanvasGroup> steps;
        public bool overrideStepAlpha = true;

        protected static GameController GameController => GameController.instance;
        protected static EnemyController EnemyController => EnemyController.instance;
        protected static BaseGameController BaseGame => BaseGameController.instance;
        protected static BridgeController Bridge => BridgeController.instance;
        protected static Player Player => Player.instance;

        protected virtual void Awake()
        {
            if (overrideStepAlpha) steps.ForEach(step => step.alpha = 0f);
        }

        public virtual IEnumerator Seq_StartScript()
        {
            if (StepCount != steps.Count)
                throw new Exception($"steps have to be the size of {StepCount}");
            if (steps.Any(step => step == null))
                throw new Exception("steps has null elements");
            yield return null;
        }

        protected static IEnumerator FadeElement(CanvasGroup element, float alphaTarget, float timeSpan)
        {
            var start = element.alpha;
            var t = 0f;
            while (Math.Abs(element.alpha - alphaTarget) > 0.01f)
            {
                t += Time.fixedDeltaTime / timeSpan;
                element.alpha = Mathf.Lerp(start, alphaTarget, t);
                yield return new WaitForFixedUpdate();
            }

            element.alpha = alphaTarget;
        }
    }

    public class WaitForRuneInput : CustomYieldInstruction
    {
        private readonly Rune[] _runes;
        private Rune _latestRune;
        [CanBeNull] private readonly Func<bool> skipIf;

        public override bool keepWaiting => !_runes.Contains(_latestRune) || (skipIf?.Invoke() ?? false);

        public WaitForRuneInput(Rune[] runes, Func<bool> skipIf = null)
        {
            _runes = runes;
            this.skipIf = skipIf;
            UniRxExt.Subscribe(RuneController.instance.rxLatestRune.Skip(1), r => _latestRune = r);
        }

        public WaitForRuneInput(Rune rune, Func<bool> skipIf = null)
        {
            _runes = new[] { rune };
            this.skipIf = skipIf;
            UniRxExt.Subscribe(RuneController.instance.rxLatestRune.Skip(1), r => _latestRune = r);
        }
    }

    public class WaitForAttackSet : CustomYieldInstruction
    {
        private readonly AttackRuneSet _runeSet;
        private RuneSet _latestRuneSet;

        public override bool keepWaiting
        {
            get
            {
                if (_latestRuneSet == null) return true;
                var set = AttackRuneSet.FromRuneSet(_latestRuneSet);
                return set != _runeSet;
            }
        }

        public WaitForAttackSet(AttackRuneSet runeSet)
        {
            _runeSet = runeSet;
            UniRxExt.Subscribe(RuneController.instance.rxCurrentRuneSet.Skip(1), set => _latestRuneSet = set);
        }
    }

    public class WaitForDeathOfCharacter : CustomYieldInstruction
    {
        private readonly Character _target;

        public override bool keepWaiting => _target.IsAlive;

        public WaitForDeathOfCharacter(Character target)
        {
            _target = target;
        }
    }

    public static class TutorialExtensions
    {
        public static bool AnyAlive(this List<Enemy> enemies)
        {
            return enemies.Any(e => e != null) || enemies.Any(e => e.IsAlive);
        }
    }

    public class WaitForBridgeRowSwitch : CustomYieldInstruction
    {
        private static BridgeController Bridge => BridgeController.instance;

        private readonly Player _player;
        private readonly int _targetIndex;

        public override bool keepWaiting => _player.CurrentBridgeIndex != _targetIndex;

        public WaitForBridgeRowSwitch(int targetIndex)
        {
            if (targetIndex < 0 || targetIndex >= Bridge.VisibleSegmentCount)
            {
                throw new ArgumentException("Target index has to be in range of BridgeSegments");
            }

            _player = Object.FindObjectOfType<Player>();
            _targetIndex = targetIndex;
        }
    }
}