using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AVB.Tutorial
{
    public class Tutorial02 : TutorialScript
    {
        protected override int StepCount => 6;

        public override IEnumerator Seq_StartScript()
        {
            yield return base.Seq_StartScript();
            Player.CanAttack = false;
            yield return new WaitForSeconds(1f);

            // 1. Templates
            var enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Triangle))
            };
            enemies.ForEach(e => e.CanBeKilled = false);
            yield return FadeElement(steps[0], 1f, 0.5f);
            yield return new WaitForSeconds(5f);
            yield return FadeElement(steps[0], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 2. Draw triangles
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CharacterState = CharacterState.Idle;
            });
            yield return FadeElement(steps[1], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Triangle);
            yield return FadeElement(steps[1], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 3. Attack
            yield return FadeElement(steps[2], 1f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            yield return new WaitForRuneInput(Rune.Attack);
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            Player.CanAttack = false;
            yield return FadeElement(steps[2], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 4. Draw corner
            enemies = new List<Enemy>
            {
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Triangle), -6f),
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Square), -8f),
                EnemyController.SpawnEnemyAtRow(0, new AttackRuneSet(LowAttackRune.Triangle), -10f)
            };
            enemies.ForEach(e =>
            {
                e.CanMove = false;
                e.CanBeKilled = false;
                e.CharacterState = CharacterState.Idle;
            });

            yield return FadeElement(steps[3], 1f, 0.5f);
            yield return new WaitForRuneInput(Rune.Triangle);
            yield return FadeElement(steps[3], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 5. Attack multiple
            var triangleEnemies = enemies.Where(e => e.RuneSet.LowRuneType == LowAttackRune.Triangle).ToList();
            yield return FadeElement(steps[4], 1f, 0.5f);
            triangleEnemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            yield return new WaitForRuneInput(Rune.Attack);
            while (triangleEnemies.AnyAlive())
            {
                yield return null;
            }

            Player.CanAttack = false;
            yield return FadeElement(steps[4], 0f, 0.5f);

            yield return new WaitForSeconds(0.5f);

            // 6. Kill square enemy
            yield return FadeElement(steps[5], 1f, 0.5f);
            enemies.ForEach(e => e.CanBeKilled = true);
            Player.CanAttack = true;
            while (enemies.AnyAlive())
            {
                yield return null;
            }

            yield return FadeElement(steps[5], 0f, 0.5f);

            yield return BaseGame.Seq_FinishLevel();
        }
    }
}