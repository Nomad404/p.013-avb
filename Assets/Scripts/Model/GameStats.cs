﻿using System;
using UnityEngine;

namespace AVB
{
    public struct ScreenBoundarySet
    {
        public Vector2 topLeft, topRight, bottomLeft, bottomRight;

        public ScreenBoundarySet(Vector2 topLeft, Vector2 topRight, Vector2 bottomLeft, Vector2 bottomRight)
        {
            this.topLeft = topLeft;
            this.topRight = topRight;
            this.bottomLeft = bottomLeft;
            this.bottomRight = bottomRight;
        }

        public bool IsWithinBoundary(Vector2 pos)
        {
            return pos.x >= topLeft.x && pos.x <= topRight.x
                                      && pos.y <= topLeft.y && pos.y >= bottomLeft.y;
        }
    }

    [Serializable]
    public struct LevelBoundarySet
    {
        public BoxCollider2D top, bottom, left, right;

        public LevelBoundarySet(BoxCollider2D top, BoxCollider2D bottom, BoxCollider2D left, BoxCollider2D right)
        {
            this.top = top;
            this.bottom = bottom;
            this.left = left;
            this.right = right;
        }
    }
}