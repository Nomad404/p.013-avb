﻿using System;
using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using UniRxExt = UniRx.ObservableExtensions;

namespace AVB
{
    public class RuneInfoImage : MonoBehaviour
    {
        public Sprite[] lowRuneSprites = new Sprite[3];
        public Sprite[] midRuneSprites = new Sprite[4];

        private Image imageRenderer;
        private RectTransform rectTransform;

        [HideInInspector] public ReactiveProperty<Rune> rxRune = new(Rune.Empty);

        public Rune CurrentRune
        {
            get => rxRune.Value;
            set => rxRune.Value = value;
        }

        [HideInInspector] public ReactiveProperty<int> rxSlotIndex = new(0);

        public int CurrentSlotIndex
        {
            get => rxSlotIndex.Value;
            set
            {
                if (rxSlotIndex.Value != value) rxSlotIndex.Value = value;
            }
        }

        private readonly CompositeDisposable compDisp = new();

        public ReactiveProperty<Unit> onObjectStop = new();

        private const float InstantMoveDistanceThreshold = 0.01f;

        private void Awake()
        {
            imageRenderer = GetComponentInChildren<Image>();
            rectTransform = GetComponent<RectTransform>();

            GetComponentInChildren<Canvas>().sortingOrder = UILayers.RuneInfoRune;
        }

        private void Start()
        {
            UniRxExt.Subscribe(rxRune, r =>
            {
                imageRenderer.sprite = CurrentRune.majorType switch
                {
                    RuneType.Low => lowRuneSprites[CurrentRune.subType - 1],
                    RuneType.Mid => midRuneSprites[CurrentRune.subType - 1],
                    _ => imageRenderer.sprite
                };
            }).AddTo(this);

            UniRxExt.Subscribe(rxSlotIndex, OnSlotIndexChanged).AddTo(this);
        }

        private void OnDestroy()
        {
            compDisp.Dispose();
        }

        private void OnSlotIndexChanged(int newIndex)
        {
            transform.SetParent(RuneInfoContainer.instance.slots[newIndex].transform);
            if (newIndex == 0)
            {
                Clear();
            }
            else if (newIndex == RuneInfoContainer.instance.SlotCount - 1)
            {
                imageRenderer.color = new Color(1, 1, 1, 0);
                MoveToOverTime(new Vector3(0, 0, -10), 0.2f);
            }
            else
            {
                if (newIndex == RuneInfoContainer.instance.SlotCount - 2)
                {
                    MoveAndFadeOverTime(new Vector3(0, 0, 10), new Color(1, 1, 1, 1), 0.2f);
                }
                else
                {
                    MoveToOverTime(new Vector3(0, 0, -10), 0.2f);
                }
            }
        }

        public void Clear()
        {
            transform.SetParent(RuneInfoContainer.instance
                .slots[Mathf.Clamp(CurrentSlotIndex - 1, 0, RuneInfoContainer.instance.slots.Length - 1)].transform);
            UniRxExt.Subscribe(GetMoveAndFadeOverTimeObservable(new Vector3(0, 0, -10), new Color(1, 1, 1, 0), 0.2f),
                _ => { Destroy(gameObject); }).AddTo(compDisp);
        }

        public void MoveToOverTime(Vector3 destination, float timeFrame)
        {
            compDisp.Clear();
            UniRxExt.Subscribe(Observable.FromCoroutine(_ => Seq_MoveToPositionOverTime(destination, timeFrame)),
                    _ => { onObjectStop.Value = Unit.Default; })
                .AddTo(compDisp);
        }

        public void FadeOverTime(Color target, float timeFrame)
        {
            compDisp.Clear();
            UniRxExt.Subscribe(Observable.FromCoroutine(_ => Seq_FadeOverTime(target, timeFrame))).AddTo(compDisp);
        }

        public void MoveAndFadeOverTime(Vector3 destination, Color targetColor, float timeFrame)
        {
            compDisp.Clear();
            UniRxExt.Subscribe(GetMoveAndFadeOverTimeObservable(destination, targetColor, timeFrame)).AddTo(compDisp);
        }

        public IObservable<Unit> GetMoveAndFadeOverTimeObservable(Vector3 destination, Color targetColor,
            float timeFrame)
        {
            return Observable.WhenAll(
                Observable.FromCoroutine(_ => Seq_MoveToPositionOverTime(destination, timeFrame)),
                Observable.FromCoroutine(_ => Seq_FadeOverTime(targetColor, timeFrame))
            );
        }

        private IEnumerator Seq_MoveToPositionOverTime(Vector3 destination, float timeFrame)
        {
            var startPos = rectTransform.localPosition;
            var t = 0f;
            while (Vector3.Distance(rectTransform.localPosition, destination) > InstantMoveDistanceThreshold)
            {
                t += Time.deltaTime / timeFrame;
                rectTransform.localPosition = Vector3.Lerp(startPos, destination, t);
                yield return new WaitForEndOfFrame();
            }

            rectTransform.localPosition = destination;
        }

        private IEnumerator Seq_FadeOverTime(Color target, float timeFrame)
        {
            var startColor = imageRenderer.color;
            var t = 0f;
            while (Mathf.Abs(imageRenderer.color.a - target.a) > 0.001f)
            {
                t += Time.deltaTime / timeFrame;
                imageRenderer.color = Color.Lerp(startColor, target, t);
                yield return new WaitForEndOfFrame();
            }

            imageRenderer.color = target;
        }
    }
}