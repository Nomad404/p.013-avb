﻿namespace AVB
{
    public abstract class Character : MovingObject, IKillable
    {
        private CharacterState _currentCharacterState = CharacterState.Default;

        public CharacterState CharacterState
        {
            get => _currentCharacterState;
            set
            {
                var hasChanged = _currentCharacterState != value;
                _currentCharacterState = value;
                onNewCharacterState(hasChanged);
            }
        }

        public delegate void CharacterStateDelegate(bool stateChanged);

        public CharacterStateDelegate onNewCharacterState;

        public bool CanAttack { get; set; } = true;

        private float _hp;

        public float Health
        {
            get => _hp;
            set
            {
                _hp = value;
                if (_hp <= 0)
                {
                    OnHealthDepleted();
                }
            }
        }

        public bool IsAlive => Health > 0;

        protected abstract void OnHealthDepleted();
        public bool CanBeKilled { get; set; } = true;
        public abstract void Kill();
    }

    public enum CharacterState
    {
        Invalid = 0,
        Default = 1,
        Idle = 2,
        Moving = 3,
        Attacking = 4,
        Stunned = 5,
        Death = 6
    }
}