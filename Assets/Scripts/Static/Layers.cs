﻿namespace AVB
{
    public static class Layers
    {
        public const string Static = "Static";
        public const string Dynamic = "Dynamic";
        public const string Player = "Player";
        public const string Enemy = "Player";
        public const string EnemyDeadZOne = "EnemyDeadZone";
        public const string Barrier = "Barrier";
        public const string Projectiles = "Projectiles";
        public const string Bridge = "Bridge";
    }

    public static class SpriteLayers
    {
        public const int Default = -1;
        public const int Background = 0;

        public static int Waves => Background + 1;

        public static int RowOverlay => Waves + 1;

        public static int EnemyIndicator => RowOverlay + 1;

        public static int EnemyMIN => RowOverlay + 1;

        public static int EnemyMAX => BridgeSeg - 1;

        public static int BridgeSeg => EnemyIndicator + 200;

        public static int BridgeSegDeathOverlay => Player + 2;

        public static int ArrowTrail => ArrowDefault - 1;

        public static int ArrowDefault => Player - 1;

        public static int Player => BridgeSeg + 10;

        public static int FX => Player + 1;
    }

    public static class UILayers
    {
        public const int Invalid = -1;
        public const int Default = 0;

        public static int RuneInfoGlow => Default + 1;

        public static int RuneInfo => RuneInfoGlow + 1;

        public static int RuneInfoRune => RuneInfo + 1;

        public static int QuickInfoText => RuneInfoRune + 1;

        public static int MenuQuickButtons => MenuBackground - 1;

        public static int MenuBackground => QuickInfoText + 2;

        public static int MenuContent => MenuBackground + 1;

        public static int ForegroundOverlay => MenuContent + 1;
    }
}