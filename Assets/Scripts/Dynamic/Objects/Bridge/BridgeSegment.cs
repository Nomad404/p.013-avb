﻿using System.Collections;
using System.Linq;
using Spine;
using Spine.Unity;
using UnityEngine;

namespace AVB
{
    public class BridgeSegment : BridgePart, IDamageable
    {
        public SkeletonAnimation spineObj;
        public SkeletonAnimation deadLoopOverlayObj;
        public SpriteRenderer rowHighlightSprite;
        public GameObject enemyIndicator;

        private Vector3 _enemyIndicatorStartPos;

        private static LevelBoundaryController LevelBoundaries => LevelBoundaryController.instance;

        public int Index { get; set; } = -1;

        private const float MAXHealth = 100.0f;

        public float Health { get; private set; } = 100.0f;

        public bool CanBeKilled { get; set; } = true;

        private bool _isGlowing;

        public bool IsGlowing
        {
            get => _isGlowing;
            set
            {
                _isGlowing = value;
                if (_isGlowing) return;
                var mat = spineObj.GetComponent<MeshRenderer>().material;
                mat.SetFloat(FillPhase, 0f);
            }
        }

        public bool IsDestroyed =>
            spineObj.AnimationName is AnimStages.DeathTransition or AnimStages.DeathLoop;

        private const float RowHighlightMaxAlpha = 0.2f;
        private const float RowHighlightMinAlpha = 0.0f;
        private const float RowHighlightLerpSpeed = 3f;

        private IEnumerator _corHighlightFade;

        [Header("FX")] public SpriteRenderer hitFxPrefab;
        private static readonly int FillPhase = Shader.PropertyToID("_FillPhase");

        protected override void Awake()
        {
            base.Awake();
            Health = MAXHealth;

            baseOriginalStartPosition = spineObj.transform.localPosition;
            spineObj.GetComponent<MeshRenderer>().sortingOrder = SpriteLayers.BridgeSeg;
            rowHighlightSprite.sortingOrder = SpriteLayers.RowOverlay;
            var mesh = deadLoopOverlayObj.GetComponent<MeshRenderer>();
            mesh.sortingOrder = SpriteLayers.BridgeSegDeathOverlay;
            mesh.enabled = false;

            enemyIndicator.GetComponentsInChildren<SpriteRenderer>().ToList()
                .ForEach(sprite => sprite.sortingOrder = SpriteLayers.EnemyIndicator);
            _enemyIndicatorStartPos = enemyIndicator.transform.localPosition;
        }

        private void Start()
        {
            if (spineObj != null)
            {
                spineObj.AnimationState.Complete += delegate(TrackEntry trackEntry)
                {
                    if (trackEntry.Animation.name == AnimStages.DeathTransition)
                    {
                        SetAnimationState(AnimStages.DeathLoop);
                    }
                };
            }
        }

        private void Update()
        {
            if (GameTime.IsPaused) return;
            CheckTouch();
            if (IsGlowing)
            {
                var mat = spineObj.GetComponent<MeshRenderer>().material;
                mat.SetFloat(FillPhase, 0.25f * (1 + Mathf.Sin(Time.time * 2f)));
            }

            var indicatorVisible = LevelBoundaries.topEnemies.Any(e => e.SegmentIndex == Index);
            var indicatorSprites = enemyIndicator.GetComponentsInChildren<SpriteRenderer>().ToList();
            indicatorSprites.ForEach(sprite =>
            {
                var color = sprite.color;
                color.a = Mathf.Lerp(color.a, indicatorVisible ? 0.3f : 0f, 0.1f);
                sprite.color = color;
            });
            enemyIndicator.transform.localPosition =
                _enemyIndicatorStartPos + Vector3.up * Mathf.Sin(Time.time * 1.5f) * 0.5f;
        }

        private void CheckTouch()
        {
            if (Input.touchCount <= 0 || Input.GetTouch(0).phase != TouchPhase.Ended) return;
            if (Camera.main is null) return;
            var wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            var touchPos = new Vector2(wp.x, wp.y);
            if (boxCol == Physics2D.OverlapPoint(touchPos)) RequestPlayerMove();
        }

        private void OnMouseDown()
        {
            RequestPlayerMove();
        }

        private void RequestPlayerMove()
        {
            if (InputHandler.instance.playerBridgeMoveType == PlayerBridgeMoveType.Tap)
            {
                Player.OnBridgeSegmentTapped(Index);
            }
        }

        private IEnumerator Seq_Shake(float duration, float magnitude)
        {
            var spineTransform = spineObj.transform;
            var elapsed = 0f;
            while (elapsed < duration)
            {
                var newX = baseOriginalStartPosition.x + Random.Range(-1f, 1f) * magnitude;
                var newY = baseOriginalStartPosition.y + Random.Range(-1f, 1f) * magnitude;
                spineTransform.localPosition = new Vector3(spineTransform.localPosition.x, newY, 0);
                elapsed += Time.fixedDeltaTime;
                yield return null;
            }

            spineTransform.localPosition = baseOriginalStartPosition;
        }

        public void SwitchRowHighlight(bool turnOn, bool immediate)
        {
            var target = rowHighlightSprite.color;
            target.a = turnOn ? RowHighlightMaxAlpha : RowHighlightMinAlpha;
            if (immediate)
            {
                rowHighlightSprite.color = target;
            }
            else
            {
                if (_corHighlightFade != null) StopCoroutine(_corHighlightFade);
                _corHighlightFade = Seq_FadeRowHighlight(target);
                StartCoroutine(_corHighlightFade);
            }
        }

        private IEnumerator Seq_FadeRowHighlight(Color target)
        {
            while (Mathf.Abs(target.a - rowHighlightSprite.color.a) > 0.001f)
            {
                rowHighlightSprite.color = Color.Lerp(rowHighlightSprite.color, target,
                    RowHighlightLerpSpeed * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }

            rowHighlightSprite.color = target;
            yield return null;
        }

        public void TakeDamage(float amount)
        {
            Health -= amount;
            const float healthInterval = MAXHealth / 3;
            switch (Health)
            {
                case > healthInterval * 2:
                    SetAnimationState(AnimStages.Default);
                    break;
                case > healthInterval:
                    SetAnimationState(AnimStages.DamagedLow);
                    break;
                case > 0 and < healthInterval:
                    SetAnimationState(AnimStages.DamagedHigh);
                    break;
                default:
                    BridgeController.onBridgeDestroyed.Invoke(Index);
                    break;
            }

            var hitFX = Instantiate(hitFxPrefab, new Vector3(0, 3.8f, 0), Quaternion.identity, transform);
            hitFX.sortingOrder = SpriteLayers.BridgeSeg + 1;
            audioSource.PlayOneShot(AudioResourceController.instance.RandomBridgeDamageSound);
            StartCoroutine(Seq_Shake(0.3f, 0.2f));
        }

        public void Kill()
        {
            deadLoopOverlayObj.GetComponent<MeshRenderer>().enabled = true;
            SetAnimationState(AnimStages.DeathTransition);
            audioSource.PlayOneShot(AudioResourceController.instance.RandomBridgeDeathSound);
        }

        private void SetAnimationState(string animName, bool loop = false)
        {
            spineObj.AnimationState.SetAnimation(0, animName, loop);
        }

        public static class AnimStages
        {
            public const string Default = "default";
            public const string DamagedLow = "damaged1";
            public const string DamagedHigh = "damaged2";
            public const string DeathTransition = "deathtransition";
            public const string DeathLoop = "deadloop";
        }
    }
}